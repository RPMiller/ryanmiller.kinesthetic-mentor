﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Interfaces;

namespace KinestheticMentorTests
{
    [TestClass]
    public class MovmentTests : IPhaseDeletedReceiver
    {
        [TestMethod]
        public void MovmentMergeStartAndEndBetweenOnePhase()
        {
            MovementPhase testPhase = new MovementPhase();
            for (int i = 0; i < 9; i++)
            {
                testPhase.AddMovementFrame(new MovementFrame(new MySkeleton()));
            }
            Movement testMovement = new Movement();
            testMovement.AddMovementPhase(testPhase);
            testMovement.MergeFramesIntoPhase(3, 6, this);
            Assert.AreEqual<int>(3, testMovement.GetMovementPhases().Count, "There should be 3 phases but there are" + testMovement.GetMovementPhases().Count);
            int actualNumberOfFramesInPhaseOne = testMovement.GetMovementPhases()[0].GetMovementFrames().Count;
            Assert.AreEqual<int>(3, actualNumberOfFramesInPhaseOne, "There should be 3 frames but there are " + actualNumberOfFramesInPhaseOne);
        }

        [TestMethod]
        public void MovmentMergeStartsPartWayThroughThePhaseAndEndsInAnother()
        {
            MovementPhase testPhase = new MovementPhase();
            MovementPhase testPhase2 = new MovementPhase();
            for (int i = 0; i < 9; i++)
            {
                testPhase.AddMovementFrame(new MovementFrame(new MySkeleton()));
                testPhase2.AddMovementFrame(new MovementFrame(new MySkeleton()));
            }
            Movement testMovement = new Movement();
            testMovement.AddMovementPhase(testPhase);
            testMovement.AddMovementPhase(testPhase2);
            testMovement.MergeFramesIntoPhase(3, 12, this);
            Assert.AreEqual<int>(3, testMovement.GetMovementPhases().Count, "There should be 3 phases but there are" + testMovement.GetMovementPhases().Count);
            int actualNumberOfFramesInPhaseOne = testMovement.GetMovementPhases()[0].GetMovementFrames().Count;
            int actualNumberOfFramesInPhaseThree = testMovement.GetMovementPhases()[2].GetMovementFrames().Count;
            Assert.AreEqual<int>(3, actualNumberOfFramesInPhaseOne, "There should be 3 frames but there are " + actualNumberOfFramesInPhaseOne);
            Assert.AreEqual<int>(5, actualNumberOfFramesInPhaseThree);
        }

        [TestMethod]
        public void MovmentMergeStartsPartWayThroughThePhaseSpansAnotherPhaseAndEndsInAnother()
        {
            MovementPhase testPhase = new MovementPhase();
            MovementPhase testPhase2 = new MovementPhase();
            MovementPhase testPhase3 = new MovementPhase();
            for (int i = 0; i < 9; i++)
            {
                testPhase.AddMovementFrame(new MovementFrame(new MySkeleton()));
                testPhase2.AddMovementFrame(new MovementFrame(new MySkeleton()));
                testPhase3.AddMovementFrame(new MovementFrame(new MySkeleton()));
            }
            Movement testMovement = new Movement();
            testMovement.AddMovementPhase(testPhase);
            testMovement.AddMovementPhase(testPhase2);
            testMovement.AddMovementPhase(testPhase3);
            testMovement.MergeFramesIntoPhase(3, 20, this);
            Assert.AreEqual<int>(3, testMovement.GetMovementPhases().Count, "There should be 3 phases but there are" + testMovement.GetMovementPhases().Count);
            int actualNumberOfFramesInPhaseOne = testMovement.GetMovementPhases()[0].GetMovementFrames().Count;
            Assert.AreEqual<int>(3, actualNumberOfFramesInPhaseOne, "There should be 3 frames but there are " + actualNumberOfFramesInPhaseOne);
        }

        [TestMethod]
        public void DeleteLastPhase()
        {
            MovementPhase testPhase = new MovementPhase();
            MovementPhase testPhase2 = new MovementPhase();
            MovementPhase testPhase3 = new MovementPhase();
            for (int i = 0; i < 9; i++)
            {
                testPhase.AddMovementFrame(new MovementFrame(new MySkeleton()));
                testPhase2.AddMovementFrame(new MovementFrame(new MySkeleton()));
                testPhase3.AddMovementFrame(new MovementFrame(new MySkeleton()));
            }
            Movement testMovement = new Movement();
            testMovement.AddMovementPhase(testPhase);
            testMovement.AddMovementPhase(testPhase2);
            testMovement.AddMovementPhase(testPhase3);
            testMovement.RemoveFrames(15, 27, this);
            Assert.AreEqual<int>(2,testMovement.GetMovementPhases().Count);
        }

        public void UpdatePhaseDeletedReceiver(int phaseIndex)
        {
            
        }


        public void UpdatePhaseMovedReceiver(int phaseIndex)
        {

        }
    }
}
