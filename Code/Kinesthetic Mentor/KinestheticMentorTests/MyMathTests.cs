﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kinesthetic_Mentor.Utility;

namespace KinestheticMentorTests
{
    [TestClass]
    public class MyMathTests
    {
        [TestMethod]
        public void TestDistance()
        {
            double distance = MyMath.GetDistance(0, 0, 0, 1, 1, 1);
            Assert.AreEqual<double>(Math.Sqrt(3),distance);
        }

        [TestMethod]
        public void TestLawOfCosine()
        {
            double angle = MyMath.GetAngleLawOfCosinesForA(1, 1, 1);
            Assert.AreEqual<double>(Math.Round((double)60/180 * Math.PI,8),Math.Round(angle,8));
            double SquareRootOfThreeOverTwo = Math.Sqrt(3) / 2;
            Assert.AreNotEqual<double>(double.NaN, SquareRootOfThreeOverTwo);
            double angle2 = MyMath.GetAngleLawOfCosinesForA(.5, SquareRootOfThreeOverTwo , 1);
            Assert.AreEqual<double>(Math.Round((double)30 / 180 * Math.PI, 8), Math.Round(angle2, 8));
        }

        [TestMethod]
        public void TestDifferenceOfAngles()
        {
            double distanceLA = MyMath.GetDistance(0, 0, 0, 1, 1, 0);
            double distanceLB = MyMath.GetDistance(1, 1, 0, 2, 0, 0);
            double distanceLC = MyMath.GetDistance(2, 0, 0, 0, 0, 0);

            double distanceMA = MyMath.GetDistance(0, 0, 0, Math.Sqrt(3)/2, .5, 0);
            double distanceMB = MyMath.GetDistance(Math.Sqrt(3) / 2, .5, 0, Math.Sqrt(3) / 2, 0, 0);
            double distanceMC = MyMath.GetDistance(Math.Sqrt(3) / 2, 0, 0, 0, 0, 0);

            Assert.AreEqual(Math.Round(Math.Sqrt(2), 5), Math.Round(distanceLA, 5));
            Assert.AreEqual(Math.Round(Math.Sqrt(2), 5), Math.Round(distanceLB, 5));
            Assert.AreEqual(Math.Round(2.0, 5), Math.Round(distanceLC, 5));

            Assert.AreEqual(Math.Round(1.0, 5), Math.Round(distanceMA, 5));
            Assert.AreEqual(Math.Round(.5, 5), Math.Round(distanceMB, 5));
            Assert.AreEqual(Math.Round(Math.Sqrt(3) / 2, 5), Math.Round(distanceMC, 5));

            double angleLA = MyMath.GetAngleLawOfCosinesForA(distanceLA, distanceLB, distanceLC);
            double angleLB = MyMath.GetAngleLawOfCosinesForA(distanceLB, distanceLC, distanceLA);
            double angleLC = MyMath.GetAngleLawOfCosinesForA(distanceLC, distanceLA, distanceLB);

            double angleMA = MyMath.GetAngleLawOfCosinesForA(distanceMA, distanceMB, distanceMC);
            double angleMB = MyMath.GetAngleLawOfCosinesForA(distanceMB, distanceMC, distanceMA);
            double angleMC = MyMath.GetAngleLawOfCosinesForA(distanceMC, distanceMA, distanceMB);

            Assert.AreEqual(Math.Round(Math.PI / 4,5), Math.Round(angleLA, 5));
            Assert.AreEqual(Math.Round(Math.PI / 4, 5), Math.Round(angleLB, 5));
            Assert.AreEqual(Math.Round(Math.PI / 2, 5), Math.Round(angleLC, 5));

            Assert.AreEqual(Math.Round(Math.PI / 2, 5), Math.Round(angleMA, 5));
            Assert.AreEqual(Math.Round(Math.PI / 6, 5), Math.Round(angleMB, 5));
            Assert.AreEqual(Math.Round(Math.PI / 3, 5), Math.Round(angleMC, 5));
        }
    }
}
