﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MoNo;

namespace SkeletonViewerProject.Models
{
    public class ViewSetting : MoNo.Core.NotifyObject<ViewSetting>
    {
        public static readonly Property<Color> ColorProperty = DefineProperty(o => o.Color, Colors.Green);
        public static readonly Property<Color> BackgroundColorProperty = DefineProperty(o => o.BackgroundColor);
        public static readonly Property<bool> ShowAxisProperty = DefineProperty(o => o.ShowAxis);
        public static readonly Property<int> WheelZoomSignProperty = DefineProperty(o => o.WheelZoomSign, 1);
        public static readonly Property<Light> LightProperty = DefineProperty(o => o.Light, () => new Light());

        public Color Color
        {
            get { return base.GetValue(ColorProperty); }
            set { base.SetValue(ColorProperty, value); }
        }

        public Color BackgroundColor
        {
            get { return base.GetValue(BackgroundColorProperty); }
            set { base.SetValue(BackgroundColorProperty, value); }
        }

        public bool ShowAxis
        {
            get { return base.GetValue(ShowAxisProperty); }
            set { base.SetValue(ShowAxisProperty, value); }
        }

        public int WheelZoomSign
        {
            get { return base.GetValue(WheelZoomSignProperty); }
            set { base.SetValue(WheelZoomSignProperty, value); }
        }

        public Light Light
        {
            get { return base.GetValue(LightProperty); }
            private set { base.SetValue(LightProperty, value); }
        }

        [MoNo.Xml.Serializer(typeof(ViewSetting))]
        class Serializer : MoNo.Xml.NotifyObjectSerializer
        {
            protected override MoNo.Core.NotifyObject CreateTargetInstance() { return new ViewSetting(); }
        }
    }

}
