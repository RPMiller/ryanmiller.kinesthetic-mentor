﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoNo;

namespace SkeletonViewerProject.Models
{
    public class AppModel : MoNo.Core.NotifyObject<AppModel>
    {
        MoNo.Core.History _history = new MoNo.Core.History();
        int _savedAge;

        public AppModel()
        {
            MoNo.Core.History.ActiveHistory = _history;
            _savedAge = _history.CurrentAge;
            _history.AgeChanged += (sender, e) => base.RaisePropertyChanged(() => DocumentModified);
            this.ViewSetting.ShowAxis = true;
        }

        public static readonly Property<Document> DocumentProperty =
          DefineProperty(o => o.Document, () => new Document(), (self, e) =>
          {
              e.Old.Do(doc => doc.Release());
              e.New.Do(doc => doc.AddRef());
          });

        static readonly MoNo.Setting.SettingHolder<ViewSetting> _holder =
          new MoNo.Setting.SettingHolder<ViewSetting>(new ViewSetting(), MoNo.Setting.Api.LocalUserAppDataPath);

        public Document Document
        {
            get { return this.GetValue(DocumentProperty); }
            private set { this.SetValue(DocumentProperty, value); }
        }

        public ViewSetting ViewSetting
        {
            get { return _holder.Get(); }
        }

        public void Clear()
        {
            this.Document.Entries.Clear();
            _history.Clear();
            _savedAge = _history.CurrentAge;
        }

        public bool DocumentModified
        {
            get { return _history.CurrentAge != _savedAge; }
        }

        public void Save(string filename)
        {
            XmlUT.ExportObject(this.Document, filename);
            _savedAge = _history.CurrentAge;
        }

        public void Open(string filename)
        {
            this.Document = XmlUT.ImportObject(filename) as Document;
            _history.Clear();
            _savedAge = _history.CurrentAge;
        }

        public void Import(string filename)
        {
            var ext = System.IO.Path.GetExtension(filename).ToLower();
            if (ext == ".stl")
            {
                //var tris = MoNo.Spatial.Formats.STLFormat.ImportTriangles( filename ).ToTriangleSoup();
                var tris = MoNo.Geometries.STLFormat.readTriangle3dSoup(filename);
                this.Document.Entries.Add(new MoNo.Wpf.Entry { Object = tris });
            }
            else if (ext == ".obj")
            {
                foreach (var mesh in MoNo.Spatial.Formats.ObjFormat.Import(filename))
                {
                    this.Document.Entries.Add(new MoNo.Wpf.Entry { Object = mesh });
                }
            }
            else if (ext == ".asc" || ext == ".csv")
            {
                var cloud = MoNo.Spatial.Formats.AsciiFormat.Import(filename);
                this.Document.Entries.Add(new MoNo.Wpf.Entry { Object = cloud });
            }
        }

        public void Sample_PutPolygonMesh()
        {
            var mesh = MoNo.Core.Factory.NewInstance<MoNo.Spatial.IPolygonMesh>();
            int vertex1 = mesh.Points.Put(new Point3d(1, 0, 0));
            int vertex2 = mesh.Points.Put(new Point3d(0, 1, 0));
            int vertex3 = mesh.Points.Put(new Point3d(0, 0, 1));
            mesh.Facets.Put(vertex1, vertex2, vertex3);

            this.Document.Entries.Add(new MoNo.Wpf.Entry { Object = mesh });
        }
    }
}
