﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using MoNo;

namespace SkeletonViewerProject.Models
{
    public class Document : MoNo.Core.IMemorable
    {
        public readonly MoNo.Core.ObservableCollection<MoNo.Wpf.Entry> Entries;

        public Document(MoNo.Wpf.Entry[] entries)
        {
            this.Entries = new MoNo.Core.ObservableCollection<MoNo.Wpf.Entry>(entries);
        }

        public Document()
            : this(new MoNo.Wpf.Entry[0])
        { }

        #region IMemorable メンバー

        public void AddRef()
        {
            this.Entries.AddRef();
        }

        public void Release()
        {
            this.Entries.Release();
        }

        #endregion

        [MoNo.Xml.Serializer(typeof(Document))]
        class Serializer : MoNo.Xml.AbstractSerializer<Document>
        {
            public override IEnumerable<object> References
            {
                get { return Target.Entries; }
            }

            public override void Write(XmlWriter writer, MoNo.Xml.INameResolver names)
            {
                writer.WriteMonoArrayRef("Entries", names, Target.Entries.ToArray());
            }

            public override void Read(XmlReader reader, MoNo.Xml.IReferenceResolver refs)
            {
                Target = new Document(reader.ReadMonoArrayRef<MoNo.Wpf.Entry>("Entries", refs));
            }
        }
    }
}
