﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel.Composition;
using MoNo;
using MoNo.IO;

namespace SkeletonViewerProject.Models
{
    public class Light : MoNo.Core.NotifyObject<Light>
    {
        public static readonly Property<double>
          StrengthProperty = DefineProperty(o => o.Strength, 0.5);

        public static readonly Property<Direction3d>
          DirectionProperty = DefineProperty(o => o.Direction, GeomUT.V(1.0, 1.0, 1.0).Direction);

        public double Strength
        {
            get { return this.GetValue(StrengthProperty); }
            set { this.SetValue(StrengthProperty, value); }
        }

        public Direction3d Direction
        {
            get { return this.GetValue(DirectionProperty); }
            set { this.SetValue(DirectionProperty, value); }
        }

        [MoNo.Xml.Serializer(typeof(Light))]
        class Serializer : MoNo.Xml.NotifyObjectSerializer
        {
            protected override MoNo.Core.NotifyObject CreateTargetInstance() { return new Light(); }
        }
    }
}
