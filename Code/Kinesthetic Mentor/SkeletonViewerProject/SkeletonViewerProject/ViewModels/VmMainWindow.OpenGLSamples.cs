﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoNo;
using MoNo.OpenGL;

namespace SkeletonViewerProject.ViewModels
{
    partial class VmMainWindow
    {
        public IEnumerable<Common.CommandItem> OpenGLCommandItems
        {
            get
            {
                yield return new Common.CommandItem("OpenGL を P/Invoke で直接呼び出して直線を描画", this.Commands.ByAction(this.OpenGL_ByGLPInvokeScene));
                yield return new Common.CommandItem("描画デリゲートをシーングラフに登録して描画", this.Commands.ByAction(this.OpenGL_ByGLPInvokeDelegate));
                yield return new Common.CommandItem("MGL を利用して直線を描画", this.Commands.ByAction(this.OpenGL_ByMGL));
                yield return new Common.CommandItem("ISceneContext を利用して直線を描画", this.Commands.ByAction(this.OpenGL_BySceneContext));
                yield return new Common.CommandItem("頂点配列による折線描画（MGL）", this.Commands.ByAction(this.OpenGL_VertexArrayByMGL));
                yield return new Common.CommandItem("頂点配列による折線描画（GraphicsUT）", this.Commands.ByAction(this.OpenGL_VertexArrayByGraphicsUT));
                yield return new Common.CommandItem("テクスチャマッピング", this.Commands.ByAction(this.OpenGL_TextureMapping));
                yield return new Common.CommandItem("シーンをクリア", this.Commands.ByAction(this.OpenGL_ClearSceneGraph));
            }
        }

        #region OpenGL を P/Invoke で直接呼び出して直線を描画

        // MoNo.Graphics.IScene インターフェイスを実装したシーンクラスを定義します。
        class GLPInvokeScene : MoNo.Graphics.IScene
        {
            public void Draw(MoNo.Graphics.ISceneContext sc)
            {
                // MoNo.OpenGL.GL クラスを利用すると P/Invoke により直接 OpenGL の API を呼び出すことができます。
                GL.glDisable(GL.GL_LIGHTING); // ライティングを無効化
                GL.glColor3f(1, 1, 0);        // 色を設定
                GL.glBegin(GL.GL_LINES);
                GL.glVertex3d(0, 0, 0);
                GL.glVertex3d(1, 0, 0);
                GL.glEnd();
            }
        }

        void OpenGL_ByGLPInvokeScene()
        {
            // 作成したシーンオブジェクトをシーングラフの WorldScenes に登録します。
            this.View.SceneGraph.WorldScenes.Add(new GLPInvokeScene());
        }

        #endregion

        #region 描画デリゲートをシーングラフに登録して描画

        void OpenGL_ByGLPInvokeDelegate()
        {
            // シーンクラスを定義しなくても、シーングラフに描画デリゲートを登録することが出来ます。
            this.View.SceneGraph.WorldScenes.Add(sc =>
            {
                GL.glDisable(GL.GL_LIGHTING); // ライティングを無効化
                GL.glColor3f(1, 0.5f, 0);     // 色を設定
                GL.glBegin(GL.GL_LINES);
                GL.glVertex3d(0, 0, 0);
                GL.glVertex3d(1, 1, 0);
                GL.glEnd();
            });
        }

        #endregion

        #region MGL を利用して直線を描画

        void OpenGL_ByMGL()
        {
            // MoNo.OpenGL.MGL という OpenGL の薄いラッパーを利用すると、より便利に描画処理が記述できます。
            this.View.SceneGraph.WorldScenes.Add(sc =>
            {
                MGL.LightingEnabled = false;
                MGL.Color = System.Drawing.Color.Pink.ToColor4f();
                using (var gl = MGL.Begin(GLPrimType.Lines))
                {
                    gl.Vertex(Point3d.Zero);
                    gl.Vertex(new Point3d(1, 1, 1));
                }
            });
        }

        #endregion

        #region ISceneContext を利用して直線を描画

        void OpenGL_BySceneContext()
        {
            this.View.SceneGraph.WorldScenes.Add(sc =>
            {
                using (var scope = sc.Push())
                { // scope を利用すると Lighting 等の状態変更が using スコープ内に限定されます
                    scope.Lighting = false;
                    scope.Color = System.Drawing.Color.Plum;
                    sc.DrawLines(gl => gl.Vertices(Point3d.Zero, new Point3d(1, 2, 3)));
                }
            });
        }

        #endregion

        #region 頂点配列による折線描画（MGL）

        void OpenGL_VertexArrayByMGL()
        {
            // 頂点配列を作成
            var array = new[]{
        new Point3d( 0.0, 0.0, 0.0 ),
        new Point3d( 0.2, 0.0, 0.0 ),
        new Point3d( 0.2, 0.2, 0.0 ),
        new Point3d( 0.0, 0.2, 0.0 ),
        new Point3d( 0.0, 0.4, 0.0 ),
        new Point3d( 0.2, 0.4, 0.0 ),
        new Point3d( 0.2, 0.6, 0.0 ),
        new Point3d( 0.0, 0.6, 0.0 ),
      };

            // シーングラフに登録して描画
            this.View.SceneGraph.WorldScenes.Add(sc =>
            {
                using (var scope = sc.Push())
                {
                    scope.Lighting = false;
                    scope.Color = System.Drawing.Color.Salmon;
                    MGL.DrawArrays(GLPrimType.LineStrip, array); // 頂点配列の描画
                }
            });
        }

        #endregion

        #region 頂点配列による折線描画（GraphicsUT）

        void OpenGL_VertexArrayByGraphicsUT()
        {
            // 頂点配列によるシーンを生成
            var scene = GraphicsUT.CreateScene(GLPrimType.LineStrip, new[]{
        new Point3d( 0.0, 0.0, 0.5 ),
        new Point3d( 0.2, 0.0, 0.5 ),
        new Point3d( 0.2, 0.2, 0.5 ),
        new Point3d( 0.0, 0.2, 0.5 ),
        new Point3d( 0.0, 0.4, 0.5 ),
        new Point3d( 0.2, 0.4, 0.5 ),
        new Point3d( 0.2, 0.6, 0.5 ),
        new Point3d( 0.0, 0.6, 0.5 ),
      });

            // シーングラフに登録して描画
            this.View.SceneGraph.WorldScenes.Add(sc =>
            {
                using (var scope = sc.Push())
                {
                    scope.Lighting = false;
                    scope.Color = System.Drawing.Color.Purple;
                    scene.Draw(sc); // 頂点配列シーンの呼び出し
                }
            });
        }

        #endregion

        #region テクスチャマッピング

        void OpenGL_TextureMapping()
        {
            var path = this.GetPathByOpenFileDialog("画像ファイル (*.bmp;*.jpg;*.png)|*.bmp;*.jpg;*.png");
            if (path != null)
            {
                // 画像ファイルの読込
                var bitmap = new System.Drawing.Bitmap(System.Drawing.Image.FromFile(path));
                // ※ 注意！！ OpenGL 2.0 未満の場合はテクスチャのサイズは 2 のべき乗でなくてはなりません。
                //            詳しくは OpenGL の仕様を参照して下さい。

                // テクスチャオブジェクトを生成します。
                var texture = MGL.CreateTextureObject(bitmap);

                // シーングラフにテクスチャを描画するシーンを登録します。
                this.View.SceneGraph.WorldScenes.Add(sc =>
                {
                    using (var scope = sc.Push())
                    {
                        scope.Lighting = false;
                        scope.Color = System.Drawing.Color.White;
                        using (var binding = texture.Bind())
                        {  // テクスチャをバインドします。
                            sc.DrawQuads(gl =>
                            {
                                gl.TexCoord(0, 0); gl.Vertex(0, 0, 0);
                                gl.TexCoord(1, 0); gl.Vertex(1, 0, 0);
                                gl.TexCoord(1, 1); gl.Vertex(1, 1, 0);
                                gl.TexCoord(0, 1); gl.Vertex(0, 1, 0);
                            });
                        }
                    }
                });
            }
        }

        #endregion

        #region シーンをクリア

        void OpenGL_ClearSceneGraph()
        {
            this.View.SceneGraph.WorldScenes.Clear();
        }

        #endregion
    }
}
