﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoNo;

namespace SkeletonViewerProject.ViewModels
{
    partial class VmMainWindow
    {
        public IEnumerable<Common.CommandItem> EntityCommandItems
        {
            get
            {
                yield return new Common.CommandItem("折線エンティティを登録します", this.Commands.ByAction(this.Entity_PutPolyline));
                yield return new Common.CommandItem("三角形群エンティティを登録します", this.Commands.ByAction(this.Entity_PutTriangleSoup));
                yield return new Common.CommandItem("三角形群エンティティを登録します2", this.Commands.ByAction(this.Entity_PutTriangleSoup2));
                yield return new Common.CommandItem("メッシュエンティティを登録します", this.Commands.ByAction(this.Entity_PutPolygonMesh));
            }
        }

        void Entity_PutPolyline()
        {
            // MoNo.Spatial.IPolyline エンティティを生成
            var polyline = new[] {
        new Point3d( 0, 0, 0 ),
        new Point3d( 1, 0, 0 ),
        new Point3d( 1, 1, 0 ),
        new Point3d( 0, 1, 1 ) }.ToPolyline();

            // エントリを生成して Object プロパティに折線エンティティを設定
            var entry = new MoNo.Wpf.Entry { Object = polyline };

            // ドキュメントにエントリを登録
            _app.Document.Entries.Add(entry);

            // ★★★ _app.Document.Entries にエントリを登録すると、ビューにエンティティが描画されるメカニズムの説明 ★★★
            // VmMainWindow の Entries プロパティで _app.Document.Entries を返しています。
            // この Entries プロパティは、Views/MinaWindow.xaml で次のように Binding されています。
            //
            // <m:GLViewport ...>
            //   ...
            //   <m:SceneGraph EntriesSource="{Binding Path=Entries}" ... />
            //   ...
            // </m:GLViewport>
            //
            // このように、Binding を経由して最終的に SceneGraph に組み込まれることにより、エンティティがビューに描画されます。
        }

        void Entity_PutTriangleSoup()
        {
            // ITriangleSoup エンティティのインスタンスを生成します。
            var tris = MoNo.Core.Factory.NewInstance<MoNo.Spatial.ITriangleSoup>();

            // 三角形を一枚登録します。
            tris.Put(new Point3d(0.0, 0.0, 0.0), new Point3d(1.0, 0.0, 0.0), new Point3d(0.0, 1.0, 0.0));

            // エンティティをドキュメントに登録します。
            _app.Document.Entries.Add(new MoNo.Wpf.Entry { Object = tris });
        }

        void Entity_PutTriangleSoup2()
        {
            // 三角形の配列から ITriangleSoup を生成。
            var tris = new[] {
        new MoNo.Spatial.Triangle( new Point3d( 0.0, 0.1, 0.3 ), new Point3d( 0.2, 0.0, 0.5 ), new Point3d( 0.4, 0.3, 0.0 ) ),
        new MoNo.Spatial.Triangle( new Point3d( 0.8, 0.5, 0.4 ), new Point3d( 0.0, 0.2, 0.4 ), new Point3d( 0.2, 0.1, 0.6 ) ),
        new MoNo.Spatial.Triangle( new Point3d( 0.8, 0.6, 0.3 ), new Point3d( 0.3, 0.5, 0.4 ), new Point3d( 0.2, 0.2, 0.0 ) ),
        new MoNo.Spatial.Triangle( new Point3d( 0.5, 0.2, 0.5 ), new Point3d( 0.3, 0.6, 0.1 ), new Point3d( 0.0, 0.3, 0.7 ) )
      }.ToTriangleSoup();

            // エントリを生成
            var entry = new MoNo.Wpf.Entry { Object = tris };
            entry.Color = System.Windows.Media.Colors.Aqua; // エントリに色を設定出来ます

            // エンティティをドキュメントに登録します。
            _app.Document.Entries.Add(entry);
        }

        void Entity_PutPolygonMesh()
        {
            // IPolygonMesh のインスタンスを生成します。
            var mesh = MoNo.Core.Factory.NewInstance<MoNo.Spatial.IPolygonMesh>();

            // 頂点座標を設定します。
            int id1 = mesh.Points.Put(new Point3d(0.0, 0.0, 1.0));
            int id2 = mesh.Points.Put(new Point3d(1.0, 0.0, 1.0));
            int id3 = mesh.Points.Put(new Point3d(1.0, 1.0, 1.0));
            int id4 = mesh.Points.Put(new Point3d(0.0, 1.0, 1.0));

            // 面を設定します。
            mesh.Facets.Put(id1, id2, id3);
            mesh.Facets.Put(id1, id3, id4);

            // エンティティをドキュメントに登録します。
            _app.Document.Entries.Add(new MoNo.Wpf.Entry { Object = mesh });
        }
    }
}
