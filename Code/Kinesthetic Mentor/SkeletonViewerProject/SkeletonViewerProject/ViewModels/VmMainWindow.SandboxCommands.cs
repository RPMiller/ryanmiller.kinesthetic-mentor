﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Livet;
using Livet.Messaging;
using MoNo;

namespace SkeletonViewerProject.ViewModels
{
    partial class VmMainWindow
    {
        public IEnumerable<Common.CommandItem> SandboxCommands
        {
            get
            {
                yield return new Common.CommandItem("ここにコマンドを追加して下さい", base.Commands.ByAction(this.Sandbox));
                yield return new Common.CommandItem("サブメニューも作れます", this.SandboxSubCommands);
            }
        }

        IEnumerable<Common.CommandItem> SandboxSubCommands
        {
            get
            {
                yield return new Common.CommandItem("サブメニュー", base.Commands.ByAction(this.SandboxSubItem));
            }
        }

        void Sandbox()
        {
            this.Messenger.Raise(new InformationMessage("ここに自由にコマンドを追加して下さい", "Sandbox", "Information"));
        }

        void SandboxSubItem()
        {
            this.Messenger.Raise(new InformationMessage("サブメニューです", "Sandbox", "Information"));
        }
    }
}
