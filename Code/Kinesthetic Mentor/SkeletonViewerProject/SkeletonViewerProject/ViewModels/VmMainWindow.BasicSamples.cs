﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Livet.Messaging;
using MoNo;

namespace SkeletonViewerProject.ViewModels
{
    partial class VmMainWindow
    {
        #region サンプルコマンド： 単純なメッセージダイアログを表示

        // public なメソッドとしてコマンド関数を公開
        // → XAML側でCallMethodActionを使って呼び出し
        public void Sample_ShowMessage()
        {
            // ViewModel 側で直接メッセージボックスを表示するコードは書けないため、
            // Livet の Messenger 機能を利用して View 側にメッセージを送信
            // → XAML 側で Livet の InteractionMessageTrigger によりメッセージを受信
            this.Messenger.Raise(new InformationMessage("Hello World", "Hoge", "Information"));
        }

        #endregion

        #region サンプルコマンド： Commandによるメッセージダイアログの表示

        // ICommand 型のプロパティを公開します。
        // このプロパティは XAML 側で MenuItem 等の Command プロパティにバインドされます。
        // このコマンドはシンプルなアクションなので、base.Commands.ByAction() によりコマンドを生成します。
        // これによって生成されるコマンドオブジェクトは、実行終了時に
        // - ビルド処理（MoNo.RAILにおける状態更新処理）
        // - コミット処理（Undo/Redoのために一連の操作をコミットする処理）
        // - ビューの再描画
        // 等の処理を自動で行います。
        // 従って通常は CallMethodAction よりもこちらの方式の利用を推奨します。
        public ICommand Sample_ShowMessageCommand
        { get { return base.Commands.ByAction(this.Sample_ShowMessage); } }

        #endregion

        #region サンプル： CommandItem 列によるメニュー生成

        // XAML 側で MenuItem.ItemsSource にバインディングします。
        // また、XAMLで下記のようなスタイルを設定しておくことによりメニュー項目が生成されます。
        // <MenuItem.ItemContainerStyle>
        //   <Style TargetType="MenuItem">
        //     <Setter Property="Header" Value="{Binding Path=Caption}"/>
        //     <Setter Property="Command" Value="{Binding Path=Command}"/>
        //   </Style>
        // </MenuItem.ItemContainerStyle>

        public IEnumerable<Common.CommandItem> BasicCommandItems
        {
            get
            {
                yield return new Common.CommandItem("サンプル：メッセージダイアログの表示（CommandItemによる）", base.Commands.ByAction(this.Sample_ShowMessage));
            }
        }

        #endregion
    }
}
