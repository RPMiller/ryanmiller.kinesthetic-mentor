﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Forms;
using System.Windows.Media;

using Livet;
using Livet.EventListeners;

using MoNo;

namespace SkeletonViewerProject.ViewModels
{
    class VmViewSettingDialog : Livet.ViewModel
    {
        readonly Models.ViewSetting _model;

        public VmViewSettingDialog(Models.ViewSetting model)
        {
            _model = model;
            this.ColorList = new[] { Colors.Red, Colors.Blue, Colors.Green, Colors.Orange, Colors.Gray, Colors.LightGray, Colors.DarkGray };

            base.CompositeDisposable.Add(new PropertyChangedEventListener(_model) {
        { () => _model.Color,           ( sender, e ) => base.RaisePropertyChanged( () => Color ) },
        { () => _model.BackgroundColor, ( sender, e ) => { base.RaisePropertyChanged( () => BackgroundColor ); base.RaisePropertyChanged( () => BackgroundBrush ); } },
        { () => _model.ShowAxis,        ( sender, e ) => base.RaisePropertyChanged( () => ShowAxis ) },
        { () => _model.WheelZoomSign,   ( sender, e ) => base.RaisePropertyChanged( () => WheelZoomSignIndex ) }
      });

            base.CompositeDisposable.Add(new PropertyChangedEventListener(_model.Light) {
        { () => _model.Light.Strength,  ( sender, e ) => { base.RaisePropertyChanged( () => LightStrength ); base.RaisePropertyChanged( () => LightDiffuse ); } },
        { () => _model.Light.Direction, ( sender, e ) => base.RaisePropertyChanged( () => LightDirection ) }
      });

            this.LightDirectionScene = GraphicsUT.CreateScene(sc => this.DrawLightDirectionArrow(sc, this.LightDirection));
        }

        MoNo.Graphics.IView _view;
        public MoNo.Graphics.IView View
        {
            set
            {
                _view = value;
                if (_view != null)
                {
                    _view.Operation = null;
                    _view.SceneGraph.WorldScenes.Add(new WorldSphere());
                    _view.Fit();
                    _view.Events.MouseDown += (sender2, e2) =>
                    {
                        MoNo.Ctrl.OperationDriver.Default.Run(this.OnMouseDown(sender2, e2));
                    };
                }
            }
        }

        public Color Color
        {
            get { return _model.Color; }
            set { _model.Color = value; MoNo.Core.History.CommitActiveHistory(); }
        }

        public Color BackgroundColor
        {
            get { return _model.BackgroundColor; }
            set { _model.BackgroundColor = value; MoNo.Core.History.CommitActiveHistory(); }
        }

        public SolidColorBrush BackgroundBrush
        {
            get { return new SolidColorBrush(this.BackgroundColor); }
        }

        public Color[] ColorList { get; private set; }

        public bool ShowAxis
        {
            get { return _model.ShowAxis; }
            set { _model.ShowAxis = value; MoNo.Core.History.CommitActiveHistory(); }
        }

        public int WheelZoomSignIndex
        {
            get { return (_model.WheelZoomSign > 0) ? 0 : 1; }
            set { _model.WheelZoomSign = (value == 0) ? 1 : -1; MoNo.Core.History.CommitActiveHistory(); }
        }

        public double LightStrength
        {
            get { return _model.Light.Strength; }
            set { _model.Light.Strength = value; MoNo.Core.History.CommitActiveHistory(); }
        }

        public Direction3d LightDirection
        {
            get { return _model.Light.Direction; }
            set { _model.Light.Direction = value; }
        }

        public Color LightDiffuse
        {
            get
            {
                var x = (byte)(byte.MaxValue * this.LightStrength);
                return Color.FromRgb(x, x, x);
            }
        }

        public HmCod3d LightPosition
        {
            get { return this.LightDirection.ToVector(); }
        }

        public MoNo.Graphics.IScene LightDirectionScene { get; private set; }

        void DrawLightDirectionArrow(MoNo.Graphics.ISceneContext sc, Direction3d direction)
        {
            var d = direction.ToVector();
            var sys = new CodSys3d(d, new Point3d(d.X, d.Y, d.Z));
            var top = Point3d.Zero;
            var end = new Point3d(0, 0, 2);
            var px1 = new Point3d(0.3, 0, 0.8);
            var px2 = new Point3d(-0.3, 0, 0.8);
            var py1 = new Point3d(0, 0.3, 0.8);
            var py2 = new Point3d(0, -0.3, 0.8);
            top = sys.Globalize(top);
            end = sys.Globalize(end);
            px1 = sys.Globalize(px1);
            px2 = sys.Globalize(px2);
            py1 = sys.Globalize(py1);
            py2 = sys.Globalize(py2);
            sc.DrawLineStrip(gl => gl.Vertices(end, top, px1, px2, top, py1, py2, top));
        }

        IEnumerator<MoNo.Ctrl.IOperation> OnMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (_view != null && e.Button == MouseButtons.Left)
            {
                var pos = e.Location.ToPoint2i();
                var mouseUp = new MoNo.Ctrl.LButtonUp(_view);
                mouseUp.MouseMove += (sender2, e2) =>
                {
                    if (e2.Location.ToPoint2i() != pos)
                    {
                        var vec = _view.Camera.ScreenToWorld(e2.Location.ToPoint2i() - pos);
                        var axis = Vector3d.Ez * vec;
                        var theta = Math.Atan2(vec.Length, 0.5 * _view.Camera.ViewingDepth);
                        var rot = new Rotation3d(axis.Normalize(), theta);
                        this.LightDirection = rot.Rotate(this.LightDirection);
                        _view.Invalidate();
                        pos = e2.Location.ToPoint2i();
                    }
                };
                yield return mouseUp;
                MoNo.Core.History.CommitActiveHistory();
            }
        }

        class WorldSphere : MoNo.Graphics.IScene, IBoundary3d
        {
            void MoNo.Graphics.IScene.Draw(MoNo.Graphics.ISceneContext con) { }
            public Box3d BoundingBox { get { return BoundingSphere.BoundingBox; } }
            public Sphere3d BoundingSphere { get { return new Sphere3d(Point3d.Zero, 2.5); } }
        }
    }
}
