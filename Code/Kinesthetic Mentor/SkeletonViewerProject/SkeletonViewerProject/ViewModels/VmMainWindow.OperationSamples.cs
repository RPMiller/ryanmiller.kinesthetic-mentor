﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoNo;

namespace SkeletonViewerProject.ViewModels
{
    partial class VmMainWindow
    {
        public IEnumerable<Common.CommandItem> OperationCommandItems
        {
            get
            {
                yield return new Common.CommandItem("クリックを1回検出します", this.Commands.ByOperation(this.Operation_Click));
                yield return new Common.CommandItem("クリックを2回検出します", this.Commands.ByOperations(this.Operation_TwoClicks));
                yield return new Common.CommandItem("２点クリックで直線を作図します", this.Commands.ByOperations(this.Operation_PutLine));
                yield return new Common.CommandItem("オブジェクトをピックします", this.Commands.ByOperation(this.Operation_PickEntry));
                yield return new Common.CommandItem("HBallハンドルをドラッグします", this.Commands.ByOperation(this.Operation_HBallDrag));
            }
        }

        #region クリックを1回検出します

        MoNo.Ctrl.IOperation Operation_Click()
        {
            // クリック検出用オブジェクトを生成します
            var click = new MoNo.Ctrl.LButtonClick(this.View);

            // クリックされた時のイベントハンドラを記述します
            click.Succeeded += (sender, e) =>
            {
                this.Messenger.Raise(new Livet.Messaging.InformationMessage("クリックされました", "Message", "Information"));
            };

            // クリック待ち状態でのマウスカーソルを設定できます。
            click.Cursor = System.Windows.Forms.Cursors.Hand;

            return click;
        }

        #endregion

        #region クリックを2回検出します

        // クリック検出用オブジェクトを複数 yield return することで、複数のイベント検出が出来ます。
        IEnumerator<MoNo.Ctrl.IOperation> Operation_TwoClicks(MoNo.Wpf.OperationContext con)
        {
            var click1 = new MoNo.Ctrl.LButtonClick(this.View);
            var click2 = new MoNo.Ctrl.LButtonClick(this.View);

            yield return click1;  // 1回目のクリックを検出
            this.Messenger.Raise(new Livet.Messaging.InformationMessage("1回目のクリック", "Message", "Information"));

            yield return click2;  // 2回目のクリックを検出
            this.Messenger.Raise(new Livet.Messaging.InformationMessage("2回目のクリック", "Message", "Information"));
        }

        #endregion

        #region ２点クリックで直線を作図します

        // 端点を2点クリックすることで直線を作図するオペレーションです。
        IEnumerator<MoNo.Ctrl.IOperation> Operation_PutLine(MoNo.Wpf.OperationContext con)
        {
            // 一点目のクリックを検出します。
            var click1 = new MoNo.Ctrl.LButtonClick(this.View);
            yield return click1;

            // 直線の両端点の座標を作成します。
            var p1 = this.View.Camera.ScreenToWorld(click1.EventArgs.Location);
            var p2 = p1;  // p2 はこの時点では p1 と同じ座標を設定しておきます

            // 二点目のクリックを検出します。
            var click2 = new MoNo.Ctrl.LButtonClick(this.View);
            click2.WorldScenes.Add(sc =>
            { // 二点目のクリック待ちの間に、直線のプレビュー表示を描画します。
                using (var scope = sc.Push())
                {
                    scope.Lighting = false;
                    scope.Color = System.Drawing.Color.Turquoise;
                    sc.DrawLines(gl => gl.Vertices(p1, p2));
                }
            });
            click2.MouseMove += (sender, e) =>
            {  // MouseMove イベントで二点目の座標 p2 を設定します。
                p2 = this.View.Camera.ScreenToWorld(e.Location);
                this.View.Invalidate(); // 再描画
            };
            yield return click2;

            // ２点 p1, p2 から IPolyline オブジェクトを生成します。
            var polyline = new[] { p1, p2 }.ToPolyline();

            // ドキュメントにエンティティを登録します。
            _app.Document.Entries.Add(new MoNo.Wpf.Entry { Object = polyline });
        }

        #endregion

        #region オブジェクトをピックします

        MoNo.Ctrl.IOperation Operation_PickEntry()
        {
            var click = new MoNo.Ctrl.LButtonClick(this.View);
            click.Cursor = System.Windows.Forms.Cursors.Hand;
            click.Succeeded += (sender, e) =>
            {
                // PickAt() メソッドによりスクリーン座標値からオブジェクト（シーン）を取得できます。
                var pick = this.View.PickAt<MoNo.Wpf.Entry>(click.EventArgs.Location.ToPoint2i());
                if (pick != null)
                {
                    this.Messenger.Raise(new Livet.Messaging.InformationMessage(
                      string.Format("HitObject = {0}", pick.HitObject.Object),
                      string.Format("サンプル： エントリのピック"),
                      "Information"));
                }
            };
            return click;
        }

        #endregion

        #region HBallハンドルをドラッグします

        MoNo.Ctrl.IOperation Operation_HBallDrag()
        {
            // ボール状のハンドル（3Dビューに表示される、マウス操作可能なオブジェクト）を作成します。
            var ball = new MoNo.Ctrl.HBall(new Point3d(0.5, 0.5, 0.5));

            // ボールハンドルの MouseDown イベントで、ボールをドラッグで移動するコードを記述します。
            ball.MouseDown += (sender, e) =>
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left && ball.IsHighlighted)
                {
                    var ballpos = ball.Position;
                    var mouseUp = new MoNo.Ctrl.LButtonUp(this.View);
                    mouseUp.MouseMove += (sender2, e2) =>
                    { // マウスを離すまでの間、マウスドラッグによるボールの移動を計算します
                        var move = this.View.Camera.ScreenToWorld(e2.Location.ToPoint2i() - e.Location.ToPoint2i());
                        ball.Position = ballpos + move;
                        this.View.Invalidate(); // 再描画
                    };
                    e.Interrupt(mouseUp); // ドラッグ操作のためのイベントハンドリングを「割り込み」させます
                }
            };

            this.View.Invalidate();
            return new MoNo.Ctrl.HandleOperation(this.View, ball);  // HandleOperation に HBall ハンドルを設定して返します
        }

        #endregion
    }
}
