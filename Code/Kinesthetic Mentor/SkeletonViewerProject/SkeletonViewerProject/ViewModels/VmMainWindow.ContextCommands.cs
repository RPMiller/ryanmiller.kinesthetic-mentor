﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Livet;
using Livet.Messaging;
using MoNo;
using MoNo.Geometries;

namespace SkeletonViewerProject.ViewModels
{
    partial class VmMainWindow
    {
        MoNo.Wpf.Entry _selectedEntry;

        public MoNo.Wpf.Entry SelectedEntry
        {
            get { return _selectedEntry; }
            set { _selectedEntry = value; base.RaisePropertyChanged(() => SelectedEntry, () => ContextCommands); }  // ContextCommands の変更通知が必要
        }

        public IEnumerable<Common.CommandItem> ContextCommands
        {
            get
            {
                object target = this.SelectedEntry != null ? this.SelectedEntry.Object : null;
                if (target is Soup<Triangle3d>)
                {
                    yield return new Common.CommandItem("面数表示", base.Commands.ByAction<Soup<Triangle3d>>(this.Triangle3dSoup_ShowTriangleCount), target);
                }
            }
        }

        void Triangle3dSoup_ShowTriangleCount(Soup<Triangle3d> tris)
        {
            this.Messenger.Raise(new InformationMessage(string.Format("triangle count = {0}", tris.Items.Length), "Soup<Triangle3d>", "Information"));
        }
    }
}
