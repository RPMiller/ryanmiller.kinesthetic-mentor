﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Input;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using MoNo;
using SkeletonViewerProject.Models;

namespace SkeletonViewerProject.ViewModels
{
    public partial class VmMainWindow : Common.ViewModel
    {
        /* コマンド、プロパティの定義にはそれぞれ 
         * 
         *  lvcom   : ViewModelCommand
         *  lvcomn  : ViewModelCommand(CanExecute無)
         *  llcom   : ListenerCommand(パラメータ有のコマンド)
         *  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
         *  lprop   : 変更通知プロパティ
         *  
         * を使用してください。
         * 
         * Modelが十分にリッチであるならコマンドにこだわる必要はありません。
         * View側のコードビハインドを使用しないMVVMパターンの実装を行う場合でも、ViewModelにメソッドを定義し、
         * LivetCallMethodActionなどから直接メソッドを呼び出してください。
         * 
         * ViewModelのコマンドを呼び出せるLivetのすべてのビヘイビア・トリガー・アクションは
         * 同様に直接ViewModelのメソッドを呼び出し可能です。
         */

        /* ViewModelからViewを操作したい場合は、View側のコードビハインド無で処理を行いたい場合は
         * Messengerプロパティからメッセージ(各種InteractionMessage)を発信する事を検討してください。
         */

        /* Modelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedEventListenerや
         * CollectionChangedEventListenerを使うと便利です。各種ListenerはViewModelに定義されている
         * CompositeDisposableプロパティ(LivetCompositeDisposable型)に格納しておく事でイベント解放を容易に行えます。
         * 
         * ReactiveExtensionsなどを併用する場合は、ReactiveExtensionsのCompositeDisposableを
         * ViewModelのCompositeDisposableプロパティに格納しておくのを推奨します。
         * 
         * LivetのWindowテンプレートではViewのウィンドウが閉じる際にDataContextDisposeActionが動作するようになっており、
         * ViewModelのDisposeが呼ばれCompositeDisposableプロパティに格納されたすべてのIDisposable型のインスタンスが解放されます。
         * 
         * ViewModelを使いまわしたい時などは、ViewからDataContextDisposeActionを取り除くか、発動のタイミングをずらす事で対応可能です。
         */

        /* UIDispatcherを操作する場合は、DispatcherHelperのメソッドを操作してください。
         * UIDispatcher自体はApp.xaml.csでインスタンスを確保してあります。
         * 
         * LivetのViewModelではプロパティ変更通知(RaisePropertyChanged)やDispatcherCollectionを使ったコレクション変更通知は
         * 自動的にUIDispatcher上での通知に変換されます。変更通知に際してUIDispatcherを操作する必要はありません。
         */


        // アプリケーション全体の状態を管理するモデルオブジェクト
        readonly Models.AppModel _app = new Models.AppModel();

        // ビュー設定ダイアログのビューモデル
        readonly VmViewSettingDialog _vmViewSettingDialog;

        public VmMainWindow()
        {
            _vmViewSettingDialog = new VmViewSettingDialog(_app.ViewSetting);
        }

        // 初期化関数
        public void Initialize()
        {
            /*
             *  VmMainWindow は _app の変更を監視し、変更があった場合は適切に変更通知のイベントを発行しなくてはなりません。  
             *  この変更通知を伝播させる設定をここで行なっています。
             */

            // _app の Document プロパティの変更を捕捉して、自身の Entries プロパティの変更イベントを発行させます。
            base.CompositeDisposable.Add(new PropertyChangedEventListener(_app) {
        { () => _app.Document, ( sender, e ) => base.RaisePropertyChanged( () => Entries ) },
      });

            // _app.ViewSetting の変更を捕捉して、関連する自身のプロパティの変更イベントを発行させます。
            base.CompositeDisposable.Add(new PropertyChangedEventListener(_app.ViewSetting) {
        { () => _app.ViewSetting.Color,           ( sender, e ) => base.RaisePropertyChanged( () => Color ) },
        { () => _app.ViewSetting.BackgroundColor, ( sender, e ) => base.RaisePropertyChanged( () => BackgroundColor1, () => BackgroundColor2 ) },
        { () => _app.ViewSetting.ShowAxis,        ( sender, e ) => base.RaisePropertyChanged( () => ShowAxis ) },
        { () => _app.ViewSetting.WheelZoomSign,   ( sender, e ) =>
          {
            if ( _view != null ) {
              ((MoNo.Graphics.StandardViewOperation)_view.Operation).WheelZoomSign = _app.ViewSetting.WheelZoomSign;
            }
          } }
      });

            // _app.ViewSetting.Light の変更を捕捉して、関連する自身のプロパティの変更イベントを発行させます。
            base.CompositeDisposable.Add(new PropertyChangedEventListener(_app.ViewSetting.Light) {
        { () => _app.ViewSetting.Light.Strength,  ( sender, e ) => base.RaisePropertyChanged( () => LightDiffuse ) },
        { () => _app.ViewSetting.Light.Direction, ( sender, e ) => base.RaisePropertyChanged( () => LightPosition ) },
      });
        }

        // View 側 (View/MainWindow.xaml）に記述された GLViewport 要素で次のように View プロパティとバインディングされます。 
        // <m:GLViewport View="{Binding Path=View, Mode=OneWayToSource}">
        // Mode=OneWayToSource でバインドされている点に注意して下さい。これは View から ViewModel に向かってバインドされています。
        MoNo.Graphics.IView _view;
        public MoNo.Graphics.IView View
        {
            get { return _view; }
            set { _view = value; base.RaisePropertyChanged(() => View); }
        }

        public ObservableCollection<MoNo.Wpf.Entry> Entries
        {
            get { return _app.Document.Entries; }
        }

        public Color Color
        {
            get { return _app.ViewSetting.Color; }
        }

        public Color BackgroundColor1
        {
            get { return _app.ViewSetting.BackgroundColor; }
        }

        public Color BackgroundColor2
        {
            get
            {
                var c = Color.Subtract(_app.ViewSetting.BackgroundColor, Colors.LightGray);
                return Color.FromRgb(c.R, c.G, c.B);
            }
        }

        public bool ShowAxis
        {
            get { return _app.ViewSetting.ShowAxis; }
        }

        public Color LightDiffuse
        {
            get
            {
                var x = (byte)(byte.MaxValue * _app.ViewSetting.Light.Strength);
                return Color.FromRgb(x, x, x);
            }
        }

        public HmCod3d LightPosition
        {
            get { return _app.ViewSetting.Light.Direction.ToVector(); }
        }

        public bool IsPerspective
        {
            get { return (_view != null) ? _view.Camera.Perspective : false; }
            set
            {
                if (_view != null)
                {
                    _view.Camera.Perspective = value;
                    this.RaisePropertyChanged(() => IsPerspective);
                }
            }
        }

        public MoNo.Graphics.PolygonStyles PolygonStyle
        {
            get { return this.ShowEdges ? MoNo.Graphics.PolygonStyles.FaceEdge : MoNo.Graphics.PolygonStyles.Face; }
        }

        bool _showEdges;
        public bool ShowEdges
        {
            get { return _showEdges; }
            set { _showEdges = value; base.RaisePropertyChanged(() => ShowEdges, () => PolygonStyle); }
        }

        public void Clear()
        {
            _app.Clear();
        }

        public ICommand OpenCommand { get { return base.Commands.ByAction(this.Open); } }
        void Open()
        {
            var path = this.GetPathByOpenFileDialog("MoNo Object XML (*.mono)|*.mono");
            if (path != null)
            {
                using (var progress = CoreUT.StartProgress("Open"))
                {
                    progress.Step(0.6);
                    _app.Open(path);

                    progress.Step(0.4);
                    CoreUT.Build();
                    _view.RunFitAnimation();
                }
            }
        }

        public ICommand ImportCommand { get { return base.Commands.ByAction(this.Import); } }
        void Import()
        {
            var filter = "3D data (*.stl;*.obj;*.asc;*.csv)|*.stl;*.obj;*.asc;*.csv";
            var filename = this.GetPathByOpenFileDialog(filter);
            if (filename != null)
            {
                using (var progress = CoreUT.StartProgress("Import"))
                {
                    progress.Step(0.6);
                    _app.Import(filename);

                    progress.Step(0.4);
                    CoreUT.Build();
                    _view.RunFitAnimation();
                }
            }
        }

        public ICommand SaveCommand { get { return base.Commands.ByAction(this.Save, () => _app.DocumentModified); } }
        void Save()
        {
            var path = this.GetPathBySaveFileDialog("MoNo Object XML (*.mono)|*.mono");
            if (path != null)
            {
                using (var progress = CoreUT.StartProgress("Save"))
                {
                    progress.Step(1.0);
                    _app.Save(path);
                }
            }
        }

        public void ViewSetting()
        {
            this.Messenger.Raise(new TransitionMessage(_vmViewSettingDialog, "Transition"));
        }

        string GetPathByOpenFileDialog(string filter)
        {
            var msg = base.Messenger.GetResponse(
              new OpeningFileSelectionMessage("OpenFile") { Filter = filter });
            return msg.Response != null && msg.Response.Length > 0 ? msg.Response[0] : null;
        }

        string GetPathBySaveFileDialog(string filter)
        {
            var msg = base.Messenger.GetResponse(
              new SavingFileSelectionMessage("SaveFile") { Filter = filter });
            return msg.Response != null && msg.Response.Length > 0 ? msg.Response[0] : null;
        }
    }
}
