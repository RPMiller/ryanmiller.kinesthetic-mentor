﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

using Livet;

namespace SkeletonViewerProject
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            DispatcherHelper.UIDispatcher = Dispatcher;
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            MoNo.Core.Progress.IntervalMilliseconds = 200;

            // プラグインDLLの設定
            MoNo.Framework.LoadPluginAssemblies(
              typeof(App).Assembly,
              typeof(Models.AppModel).Assembly,
              typeof(MoNo.Wpf.Api).Assembly,
              typeof(MoNo.Geometries.Conversion).Assembly,  // MoNo.Geometries.Plugin.dll
              typeof(MoNo.SpatialUT).Assembly);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            MoNo.Setting.ApplicationDataRegistry.Instance.Save();
            base.OnExit(e);
        }

        //集約エラーハンドラ
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //TODO:ロギング処理など
            MessageBox.Show(
                "不明なエラーが発生しました。アプリケーションを終了します。",
                "エラー",
                MessageBoxButton.OK,
                MessageBoxImage.Error);

            Environment.Exit(1);
        }
    }
}
