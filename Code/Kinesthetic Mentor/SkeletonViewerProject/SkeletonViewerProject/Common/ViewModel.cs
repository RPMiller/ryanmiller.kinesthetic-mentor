﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SkeletonViewerProject.Common
{
    public class ViewModel : Livet.ViewModel, MoNo.Wpf.IContext, MoNo.Core.IDependent
    {
        protected readonly MoNo.Wpf.CommandRegistry Commands = new MoNo.Wpf.CommandRegistry();

        public MoNo.Wpf.ViewContext ViewContext
        {
            get
            {
                if (this.Commands.ViewContext == null)
                {
                    this.Commands.ViewContext = new MoNo.Wpf.ViewContext();
                }
                return (MoNo.Wpf.ViewContext)this.Commands.ViewContext;
            }
        }

        protected void RaisePropertyChanged(params Expression<Func<object>>[] propExpressions)
        {
            foreach (var expression in propExpressions)
            {
                base.RaisePropertyChanged(
                  MoNo.Core.NotifyPropertyChanged.GetPropertyNameFromExpression(expression));
            }
        }

        MoNo.Core.Breath _breath;

        public virtual IEnumerable<MoNo.Core.IBreath> Sources
        {
            get { yield break; }
        }

        public int BreathCount
        {
            get { return _breath.BreathCount; }
        }

        public void Touch()
        {
            _breath.Touch();
        }
    }
}
