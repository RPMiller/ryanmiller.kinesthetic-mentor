﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace SkeletonViewerProject.Common
{
    public class CommandItem
    {
        public string Caption { get; private set; }
        public ICommand Command { get; private set; }
        public object Parameter { get; private set; }
        public IEnumerable<CommandItem> Items { get; private set; }

        public CommandItem(string caption, ICommand command, object parameter, IEnumerable<CommandItem> items)
        { this.Caption = caption; this.Command = command; this.Parameter = parameter; this.Items = items; }

        public CommandItem(string caption, ICommand command, object parameter) : this(caption, command, parameter, null) { }
        public CommandItem(string caption, ICommand command) : this(caption, command, null, null) { }
        public CommandItem(string caption, IEnumerable<CommandItem> items) : this(caption, null, null, items) { }
    }
}
