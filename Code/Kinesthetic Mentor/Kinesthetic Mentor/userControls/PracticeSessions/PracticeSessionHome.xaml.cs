﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;
using Kinesthetic_Mentor.Models.PracticeSessions;

namespace Kinesthetic_Mentor.userControls.PracticeSessions
{
    /// <summary>
    /// Interaction logic for PracticeSessionHome.xaml
    /// </summary>
    public partial class PracticeSessionHome : UserControl, IPracticeSessionSelectedReceiver, IPracticeSessionFinishedReceiver
    {
        public PracticeSessionHome()
        {
            InitializeComponent();
        }

        public void SetUpNavigation()
        {
            navigation.Width = this.Width;
            navigation.Height = this.Height;
            navigation.editorButton.Click += editorButton_Click;
            navigation.practiceButton.Click += practiceButton_Click;
        }

        void practiceButton_Click(object sender, RoutedEventArgs e)
        {
            PracticeSessionSelector selector = new PracticeSessionSelector();
            selector.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            selector.selectorLabel.Content = "Choose a practice session to try.";
            selector.SetUp(this);
            navigation.NavigationProperty = selector;
        }

        void editorButton_Click(object sender, RoutedEventArgs e)
        {
            PracticeSessionRecorder recorder = new PracticeSessionRecorder();
            recorder.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            navigation.NavigationProperty = recorder;

        }

        public void UpdatePracticeSessionSelectedReceiver(string fileLocation)
        {
            FlatFileIO io = new FlatFileIO();
            PracticeSession session = io.ReadPracticeSession(fileLocation);
            PracticeSessionPlayer sessionPlayer = new PracticeSessionPlayer(session,this);
            DockPanel.SetDock(sessionPlayer, Dock.Bottom);
            navigation.NavigationProperty = sessionPlayer;
        }

        public void UpdateIPracticeSessionFinishedReceiver()
        {
            navigation.backImage.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }
    }
}
