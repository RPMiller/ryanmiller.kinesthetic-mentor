﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.PracticeSessions
{
    /// <summary>
    /// Interaction logic for PracticeSessionNavigation.xaml
    /// </summary>
    public partial class PracticeSessionNavigation : UserControl
    {
        private UIElement navEle = null;
        public UIElement NavigationProperty
        {
            get { return navEle; }
            set
            {
                if (navEle != null)
                {
                    practiceScreen.Children.Remove(navEle);
                }
                if (value != null)
                {
                    practiceScreen.Children.Add(value);
                }
                navEle = value;
            }
        }

        public PracticeSessionNavigation()
        {
            InitializeComponent();
        }

        private void Navigation_Click(object sender, RoutedEventArgs e)
        {
            backImage.Visibility = System.Windows.Visibility.Visible;
            navigationGrid.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Image_MouseClick(object sender, RoutedEventArgs e)
        {
            NavigationProperty = null;
            backImage.Visibility = System.Windows.Visibility.Collapsed;
            navigationGrid.Visibility = System.Windows.Visibility.Visible;
        }
    }
}
