﻿using Kinesthetic_Mentor.Models.PracticeSessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models.Learner;

namespace Kinesthetic_Mentor.userControls.PracticeSessions
{
    /// <summary>
    /// Interaction logic for PracticeSessionPlayer.xaml
    /// </summary>
    public partial class PracticeSessionPlayer : UserControl, ILearnerMovementFinishedReceiver
    {
        private PracticeSession session;
        private PracticeMovement currentPracticeMovement = null;
        private int numberOfRepsCorrect = 0;
        private int numberOfMovmentsCorrect = -1;
        private IEnumerator<PracticeMovement> iterator;
        private IPracticeSessionFinishedReceiver receiver;

        public PracticeSessionPlayer(PracticeSession session,IPracticeSessionFinishedReceiver receiver)
        {
            InitializeComponent();
            this.receiver = receiver;
            this.session = session;
            iterator = session.GetPracticeMovements().GetEnumerator();
            iterator.MoveNext();
            messageLabel.Content = "Description: " + session.Description;
            MoveToNextMovement();
        }

        public void UpdateLearnerMovementFinishedReceiver(Models.Movement learner, Models.Movement mentor)
        {
            LearnerMovement learnerMovement = new LearnerMovement(learner, mentor);
            if (learnerMovement.IsCorrect())
            {
                numberOfRepsCorrect++;
                repsCompleted.Content = numberOfRepsCorrect + " / " + currentPracticeMovement.NumberOfReps;
            }

            UpdateFramesCompleted(learnerMovement);

            if (numberOfRepsCorrect == currentPracticeMovement.NumberOfReps)
            {
                if (iterator.MoveNext())
                {
                    MoveToNextMovement();
                }
                else
                {
                    numberOfMovmentsCorrect++;
                    movementsCompleted.Content = numberOfMovmentsCorrect + " / " + session.GetNumberOfMovements();
                    MessageBox.Show("Congratualtions you finished");
                    receiver.UpdateIPracticeSessionFinishedReceiver();
                }
            }
            else
            {
                learnerPlayer.Replay();
            }
        }

        private void UpdateFramesCompleted(LearnerMovement learnerMovement)
        {
            double numberFramesCompleted = 0;
            foreach (LearnerPhase phase in learnerMovement.GetPhases())
            {
                phase.IsPhaseSuccessful();
                numberFramesCompleted += phase.NumberOfFramesCorrect;
            }
            framesCompleted.Content = numberFramesCompleted;
        }

        private void MoveToNextMovement()
        {
            numberOfRepsCorrect = 0;
            currentPracticeMovement = iterator.Current;
            learnerPlayer.SetUp(currentPracticeMovement.GetMovement(), this,2);
            repsCompleted.Content = "0 / " + currentPracticeMovement.NumberOfReps;
            currentMovementNameLabel.Content = currentPracticeMovement.GetMovement().Name;
            numberOfMovmentsCorrect++;
            movementsCompleted.Content = numberOfMovmentsCorrect + " / " + session.GetNumberOfMovements();
            framesCompleted.Content = 0;
            framesNeeded.Content = " / " + currentPracticeMovement.GetMovement().PercentOfFramesCorrectNeeded / (100) * currentPracticeMovement.GetMovement().GetTotalNumberOfFrames();
        }

        public void updateMovementFinishedReceiver()
        {

        }
    }
}
