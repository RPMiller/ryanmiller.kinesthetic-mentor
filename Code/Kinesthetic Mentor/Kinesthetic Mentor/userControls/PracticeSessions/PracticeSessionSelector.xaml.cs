﻿using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.PracticeSessions
{
    /// <summary>
    /// Interaction logic for PracticeSessionSelector.xaml
    /// </summary>
    public partial class PracticeSessionSelector : UserControl
    {
        private IPracticeSessionSelectedReceiver receiver;

        public PracticeSessionSelector()
        {
            InitializeComponent();
        }

        public void SetUp(IPracticeSessionSelectedReceiver receiver)
        {
            this.Visibility = System.Windows.Visibility.Visible;
            mainPanel.Children.Clear();
            this.receiver = receiver;
            String fileFilter = "*" + Kinesthetic_Mentor.Utility.FlatFileIO.GetPracticeSessionFileExtension();
            DirectoryInfo directoryInfo = new DirectoryInfo(FlatFileIO.GetFolderLocation());
            FileInfo[] files = directoryInfo.GetFiles(fileFilter);
            foreach (FileInfo file in files)
            {
                CreateFileButton(file, fileFilter);
            }
        }

        private void CreateFileButton(FileInfo file, String fileFilter)
        {
            String fileName = file.Name;
            int nameLength = fileName.Length - (fileFilter.Length - 1);
            Button button = new Button();
            button.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
            button.Content = fileName.Substring(0, nameLength);
            button.Click += FileButton_Click;
            mainPanel.Children.Add(button);
        }

        void FileButton_Click(object sender, RoutedEventArgs e)
        {
            String filePath = FlatFileIO.GetFolderLocation() + ((Button)sender).Content.ToString();
            receiver.UpdatePracticeSessionSelectedReceiver(filePath);
        }

        private void ExitButtonClick(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
