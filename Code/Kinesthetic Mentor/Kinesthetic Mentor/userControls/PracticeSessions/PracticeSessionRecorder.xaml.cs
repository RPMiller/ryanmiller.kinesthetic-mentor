﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;
using Kinesthetic_Mentor.Models.PracticeSessions;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;
using System.Windows.Media.Animation;

namespace Kinesthetic_Mentor.userControls.PracticeSessions
{
    /// <summary>
    /// Interaction logic for PracticeSessionRecorder.xaml
    /// </summary>
    public partial class PracticeSessionRecorder : UserControl, IPracticeSessionSelectedReceiver, IMovementSelectedReceiver, IEntirePracticeSessionWasDeletedReceiver, ICountDownFinishedReceiver
    {
        private delegate void SessionLoadDelegate(String fileLocation);
        private SessionLoadDelegate sessionLoadDelegate;
        private PracticeSessionEditor editor;
        private MovementRecorder recorder = new MovementRecorder();

        public PracticeSessionRecorder()
        {
            InitializeComponent();
            editor = new PracticeSessionEditor(this);
        }

        private void Record_Click(object sender, RoutedEventArgs e)
        {
            const String STOP_TOOL_TIP = "Stops the recording and sends it to the editor that is in front";
            recorderButton.ToolTip = STOP_TOOL_TIP;
            recorderButton.Click -= Record_Click;
            recorderButton.Click += Stop_Click;
            recorderButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/stop.png", UriKind.Relative)) };
            recorderButton.IsEnabled = false;
            skeletonViewer.StartCountDown(3);
            skeletonViewer.RegisterCountDownFinishedReceiver(this);
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            const String RECORD_TOOL_TIP = "Starts recording your movements after the countdown finishes";
            recorderButton.ToolTip = RECORD_TOOL_TIP;
            recorderButton.Click += Record_Click;
            recorderButton.Click -= Stop_Click;
            recorderButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/record.png", UriKind.Relative)) };
            MovementPhase movementPhase = recorder.StopRecording();
            if (movementPhase.GetMovementFrames().Count > 0)
            {
                Movement movement = new Movement();
                movement.AddMovementPhase(movementPhase);
                editor.AddMovement(movement);
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            SetPracticeSessionSelector("Delete Session");
            sessionLoadDelegate = DeleteSession;
            
        }

        private void LoadMovement_Click(object sender, RoutedEventArgs e)
        {
            MovementSelector selector = new MovementSelector();
            selector.SetUp(this);
            scrollViewer.Content = selector;
            scrollViewer.Visibility = System.Windows.Visibility.Visible;
        }

        private void LoadSession_Click(object sender, RoutedEventArgs e)
        {
            SetPracticeSessionSelector("Load Session");
            sessionLoadDelegate = LoadSession;
        }

        private void SetPracticeSessionSelector(String text)
        {
            PracticeSessionSelector selector = new PracticeSessionSelector();
            selector.SetUp(this);
            selector.selectorLabel.Content = text;
            scrollViewer.Content = selector;
            scrollViewer.Visibility = System.Windows.Visibility.Visible;
        }

        private void LoadSession(String fileLocation)
        {
            FlatFileIO io = new FlatFileIO();
            PracticeSession session = io.ReadPracticeSession(fileLocation);
            editor.SetPracticeSession(session);
            scrollViewer.Visibility = System.Windows.Visibility.Hidden;
            LoadEditor();
        }

        private void DeleteSession(String fileLoaction)
        {
            FlatFileIO io = new FlatFileIO();
            io.DeletePracticeSession(fileLoaction);
            scrollViewer.Visibility = System.Windows.Visibility.Hidden;
        }

        public void UpdatePracticeSessionSelectedReceiver(String fileLocation)
        {
            sessionLoadDelegate.Invoke(fileLocation);
        }

        public void UpdateMovementSelectedReceiver(string fileLocation)
        {
            FlatFileIO io = new FlatFileIO();
            Movement movement = io.ReadMovement(fileLocation);
            editor.AddMovement(movement);
            LoadEditor();
            scrollViewer.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void LoadEditor()
        {
            if (!editor.IsLoaded)
            {
                screen.Children.Add(editor);
                ShrinkRecorder();
            }
        }

        public void UpdateEntirePracticeSessionWasDeletedReceiver()
        {
            screen.Children.Remove(editor);
            GrowRecorder();
            editor = null;
        }

        public void updateCountDownFinishedReceiver()
        {
            recorderButton.IsEnabled = true;
            recorder.StartRecording();
        }

        private void ShrinkRecorder()
        {
            skeletonViewer.BeginAnimation(StackPanel.HeightProperty, new DoubleAnimation(330, new Duration(new TimeSpan(0, 0, 1))));
            skeletonViewer.BeginAnimation(StackPanel.WidthProperty, new DoubleAnimation(310, new Duration(new TimeSpan(0, 0, 1))));
        }

        private void GrowRecorder()
        {
            skeletonViewer.BeginAnimation(StackPanel.HeightProperty, new DoubleAnimation(500, new Duration(new TimeSpan(0, 0, 1))));
            skeletonViewer.BeginAnimation(StackPanel.WidthProperty, new DoubleAnimation(500, new Duration(new TimeSpan(0, 0, 1))));
        }
    }
}
