﻿using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Models.PracticeSessions;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;
using Kinesthetic_Mentor.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.PracticeSessions
{
    /// <summary>
    /// Interaction logic for PracticeSessionEditor.xaml
    /// </summary>
    public partial class PracticeSessionEditor : UserControl, ISaveReceiver, IPracticeMovementOrderChangedReceiver
    {
        internal PracticeSession session;
        private PracticeSessionMovementEditor currentMovementEditor;
        private PracticeSessionDataEditor dataEditor;
        private PracticeSessionMovementSelectorEditor movementSelectorEditor = new PracticeSessionMovementSelectorEditor();
        private CarouselViewer carouselViewer;
        private IEntirePracticeSessionWasDeletedReceiver deleteReceiver;
        private PracticeMovementSelector practiceMovementSelector;

        public PracticeSessionEditor(IEntirePracticeSessionWasDeletedReceiver deleteReceiver)
        {
            InitializeComponent();
            dataEditor = new PracticeSessionDataEditor(this);
            practiceMovementSelector = new PracticeMovementSelector(this);
            this.deleteReceiver = deleteReceiver;
            movementSelectorEditor.movementPanel.Children.Add(practiceMovementSelector);
        }

        public void SetPracticeSession(PracticeSession session)
        {
            this.session = session;
            practiceMovementSelector.Children.Clear();
            Initialize();
            dataEditor.descriptionTextBox.Text = session.Description;
            dataEditor.nameTextBox.Text = session.Name;
            if (carouselViewer == null)
            {
                List<UIElement> elements = new List<UIElement>(){dataEditor, movementSelectorEditor};
                carouselViewer = new CarouselViewer(elements);

                screen.Children.Add(carouselViewer);
                carouselViewer.Width = 480;
                carouselViewer.Height = 660;
            }
        }

        private void Initialize()
        {
            foreach (PracticeMovement movement in session.GetPracticeMovements())
            {
                AddPracticeSessionMovementEditor(movement);
            }
            practiceMovementSelector.ClickEditorButton(0);
        }

        private void AddPracticeSessionMovementEditor(PracticeMovement movement)
        {
            EditorButtonDeleterThingyStackPanel thingy = new EditorButtonDeleterThingyStackPanel(movement, this);
            practiceMovementSelector.AddEditorButtonThingy(thingy);
            movementSelectorEditor.movementGrid.Children.Add(thingy.GetPracticeSessionMovementEditor());
        }

        public void AddMovement(Movement movement)
        {
            PracticeMovement practiceMovement = new PracticeMovement(FlatFileIO.GetFolderLocation() + movement.Name);
            if (session == null)
            {
                session = new PracticeSession();
                session.AddPracticeMovement(practiceMovement);
                SetPracticeSession(session);
            }
            else
            {
                session.AddPracticeMovement(practiceMovement);
                AddPracticeSessionMovementEditor(practiceMovement);
            }
        }

        public void SwapMovmentEditorFocusTo(PracticeSessionMovementEditor movementEditor)
        {
            if (currentMovementEditor != null)
            {
                currentMovementEditor.Visibility = System.Windows.Visibility.Hidden;
            }
            movementEditor.Visibility = System.Windows.Visibility.Visible;
            currentMovementEditor = movementEditor;
        }

        public void UpdateSaveReceiver()
        {
            bool shouldSave = true;
            if (FlatFileIO.FileExists(dataEditor.nameTextBox.Text + FlatFileIO.GetPracticeSessionFileExtension()))
            {
                shouldSave = MessageBox.Show("A file with the name " + dataEditor.nameTextBox.Text + " already exists. Are you sure you want to save?", "Save", MessageBoxButton.YesNo) == MessageBoxResult.Yes;
            }
            if (shouldSave)
            {
                session.Name = dataEditor.nameTextBox.Text;
                session.Description = dataEditor.descriptionTextBox.Text;
                FlatFileIO io = new FlatFileIO();
                io.SavePracticeSession(session, FlatFileIO.GetFolderLocation() + session.Name);
            }
        }

        internal void RemoveMovementFromSession(EditorButtonDeleterThingyStackPanel thingy, PracticeMovement practiceMovement)
        {
            session.GetPracticeMovements().Remove(practiceMovement);
            practiceMovementSelector.Children.Remove(thingy);
        }

        internal void SwapToFirstMovement()
        {
            if (practiceMovementSelector.Children.Count > 0)
            {
                practiceMovementSelector.ClickEditorButton(0);
            }
            else
            {
                deleteReceiver.UpdateEntirePracticeSessionWasDeletedReceiver();
            }
        }

        internal class EditorButton : Button, INameChangedReceiver
        {
            private PracticeSessionMovementEditor movementEditor;
            private PracticeSessionEditor editor;

            internal EditorButton(PracticeSessionMovementEditor movementEditor, PracticeSessionEditor editor)
            {
                this.movementEditor = movementEditor;
                this.editor = editor;
                Click += EditorButton_Click;
                FontSize = 20;
                Height = 40;
                movementEditor.SetNameChangedReceiver(this);
                movementEditor.Visibility = System.Windows.Visibility.Hidden;
            }

            public void EditorButton_Click(object sender, RoutedEventArgs e)
            {
                editor.SwapMovmentEditorFocusTo(movementEditor);
            }

            public void UpdateNameChangedReceiver(string Name)
            {
                this.Content = Name;
            }

            public PracticeSessionMovementEditor GetPracticeSessionMovementEditor()
            {
                return movementEditor;
            }

            public void DeleteMe(EditorButtonDeleterThingyStackPanel thingy)
            {
                editor.RemoveMovementFromSession(thingy, movementEditor.GetPracticeSessionMovement());
                editor.SwapToFirstMovement();
            }
        }

        public void UpdatePracticeMovementOrderChangedReceiver(int oldPosition, int newPosition)
        {
            PracticeMovement temp = session.GetPracticeMovements()[oldPosition];
            session.GetPracticeMovements()[oldPosition] = session.GetPracticeMovements()[newPosition];
            session.GetPracticeMovements()[newPosition] = temp;
        }

        internal class EditorButtonDeleterThingyStackPanel : StackPanel, IEntireMovemementWasDeletedReceiver
        {
            private Button deleteButton = new Button(){Content = new Image(){Height= 40, Source = new BitmapImage(new Uri("../../Images/Delete.png",UriKind.Relative))}};
            private EditorButton editorButton;

            internal EditorButtonDeleterThingyStackPanel(PracticeMovement movement, PracticeSessionEditor editor)
            {
                editorButton = new EditorButton(new PracticeSessionMovementEditor(movement, this), editor);
                editorButton.Content = movement.GetMovement().Name;
                Orientation = System.Windows.Controls.Orientation.Horizontal;
                Children.Add(editorButton);
                Children.Add(deleteButton);
                deleteButton.Click += deleteButton_Click;
             }

            void deleteButton_Click(object sender, RoutedEventArgs e)
            {
                editorButton.DeleteMe(this);
            }

            public void ClickEditorButton()
            {
                editorButton.EditorButton_Click(null,null);
            }

            public void UpdateEntireMovemementWasDeletedReceiver(Editor editorWithNoPhases)
            {
                deleteButton_Click(null, null);
            }

            public PracticeSessionMovementEditor GetPracticeSessionMovementEditor()
            {
                return editorButton.GetPracticeSessionMovementEditor();
            }
        }

        private class PracticeMovementSelector : StackPanel
        {
            private UIElement selectedElement;
            IPracticeMovementOrderChangedReceiver receiver;

            internal PracticeMovementSelector(IPracticeMovementOrderChangedReceiver receiver)
            {
                this.receiver = receiver;
                EventManager.RegisterClassHandler(typeof(Window), Mouse.MouseMoveEvent, new MouseEventHandler(Mouse_Move), true);
                EventManager.RegisterClassHandler(typeof(Window), Mouse.MouseUpEvent, new MouseButtonEventHandler(Mouse_Up), true);
            }

            public void AddEditorButtonThingy(EditorButtonDeleterThingyStackPanel thingy)
            {
                this.Children.Add(thingy);
                thingy.PreviewMouseDown += slider_MouseDown;
            }

            void slider_MouseDown(object sender, MouseButtonEventArgs e)
            {
                selectedElement = sender as UIElement;
            }

            void Mouse_Up(object sender, MouseButtonEventArgs e)
            {
                selectedElement = null;
            }

            private void Mouse_Move(object sender, MouseEventArgs e)
            {
                if (selectedElement != null)
                {
                    bool isSearching = true;
                    bool isAbove = true;
                    int oldPosition = this.Children.IndexOf(selectedElement);
                    for (int i = 0; i < this.Children.Count && isSearching; i++)
                    {
                        UIElement nextElement = this.Children[i];
                        if (selectedElement != nextElement)
                        {
                            if (isAbove)
                            {
                                isSearching = e.GetPosition(this).Y > nextElement.DesiredSize.Height * i;
                                SwapIfSearchComplete(isSearching, oldPosition,  i);
                            }
                            else
                            {
                                isSearching = e.GetPosition(this).Y < nextElement.DesiredSize.Height * i;
                                SwapIfSearchComplete(isSearching, oldPosition, i);
                            }
                        }
                        else
                        {
                            isAbove = false;
                        }
                    }
                }

                
            }

            private void SwapIfSearchComplete(bool isSearching, int oldPosition, int newPosition)
            {
                if (!isSearching)
                {
                    receiver.UpdatePracticeMovementOrderChangedReceiver(oldPosition, newPosition);
                    this.Children.Remove(selectedElement);
                    this.Children.Insert(newPosition, selectedElement);
                }
            }

            public void ClickEditorButton(int index)
            {
                ((EditorButtonDeleterThingyStackPanel)this.Children[index]).ClickEditorButton();
            }
        }
    }
}
