﻿using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Models.PracticeSessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;

namespace Kinesthetic_Mentor.userControls.PracticeSessions
{
    /// <summary>
    /// Interaction logic for PracticeSessionMovementEditor.xaml
    /// </summary>
    public partial class PracticeSessionMovementEditor : UserControl, ISaveReceiver
    {
        private PracticeMovement movement;
        private bool hasBeenEdited = false;
        private INameChangedReceiver nameChangedReceiver;
        private Editor editor;

        public PracticeSessionMovementEditor(PracticeMovement movement,IEntireMovemementWasDeletedReceiver entireMovementWasDeletedReceiver)
        {
            InitializeComponent();
            this.movement = movement;
            editor = new Editor(entireMovementWasDeletedReceiver);
            editor.Height = 580;
            screen.Children.Add(editor);
            StackPanel.SetZIndex(editor, 1);
            editor.SetMovement(movement.GetMovement(),this);
            numberOfRepsTextBox.Text = movement.NumberOfReps.ToString();
        }

        public PracticeMovement GetPracticeSessionMovement()
        {
            return movement;
        }

        public void SetNameChangedReceiver(INameChangedReceiver nameChangedReceiver)
        {
            this.nameChangedReceiver = nameChangedReceiver;
        }

        public bool HasBeenEdited()
        {
            return hasBeenEdited;
        }

        private void RepsText_Changed(object sender, TextChangedEventArgs e)
        {
            int reps = 0;
            if(!String.IsNullOrEmpty(numberOfRepsTextBox.Text) && int.TryParse(numberOfRepsTextBox.Text,out reps))
            {
                movement.NumberOfReps = reps;
            }
        }

        public void UpdateSaveReceiver()
        {
            if (movement.NumberOfReps < 1)
            {
                movement.NumberOfReps = 1;
            }
            movement.CopyMovementFileLocation = FlatFileIO.GetFolderLocation() + editor.movementEditor.GetMovement().Name;
            nameChangedReceiver.UpdateNameChangedReceiver(editor.movementEditor.GetMovement().Name);
        }
    }
}
