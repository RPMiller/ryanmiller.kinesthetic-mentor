﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Kinesthetic_Mentor.Interfaces;

namespace Kinesthetic_Mentor.userControls
{
    /// <summary>
    /// Interaction logic for CountDown.xaml
    /// </summary>
    public partial class CountDown : UserControl
    {
        private IList<ICountDownFinishedReceiver> receivers = new List<ICountDownFinishedReceiver>();
        private DispatcherTimer timer = new DispatcherTimer();
        private long nanoSecondsLeft;
        private const int HUNDRED_NANOSECONDS_IN_MILISECOND = 10000;
        private const int HUNDRED_NANOSECONDS_IN_SECOND = 10000000;
        private const int TIMER_TICK_IN_HUNDRED_NANOSECONDS = HUNDRED_NANOSECONDS_IN_MILISECOND * 100;

        public CountDown()
        {
            InitializeComponent();
            timer.Interval = new TimeSpan(TIMER_TICK_IN_HUNDRED_NANOSECONDS);
            timer.Tick += CountDown_Tick;
        }

        private void CountDown_Loaded(object sender, RoutedEventArgs e)
        {
            countDownLabel.Foreground = new SolidColorBrush(Colors.White);
        }

        void CountDown_Tick(object sender, EventArgs e)
        {
            if (nanoSecondsLeft > 0)
            {
                const int MAX_FONT_SIZE = 5000;
                int secondsLeft = (int)nanoSecondsLeft / HUNDRED_NANOSECONDS_IN_SECOND;
                long milisecondsLeftInSecond = (nanoSecondsLeft % HUNDRED_NANOSECONDS_IN_SECOND)/HUNDRED_NANOSECONDS_IN_MILISECOND;
                int fontSize = (int)(((double)milisecondsLeftInSecond / (double)HUNDRED_NANOSECONDS_IN_MILISECOND) * MAX_FONT_SIZE) + 1;
                if (countDownLabel.Content == null || !countDownLabel.Content.Equals(secondsLeft))
                {
                    countDownLabel.Content = secondsLeft;
                }
                countDownLabel.FontSize = fontSize;
                nanoSecondsLeft -= TIMER_TICK_IN_HUNDRED_NANOSECONDS;
            }
            else
            {
                timer.Stop();
                UpdateCountDownFinishedReceivers();
                countDownLabel.Content = null;
            }
        }

        public void StartCountDown(int timeInSeconds)
        {
            nanoSecondsLeft = timeInSeconds * HUNDRED_NANOSECONDS_IN_SECOND;
            timer.Start();
        }

        public void RegisterCountDownFinishedReceiver(ICountDownFinishedReceiver receiver)
        {
            receivers.Add(receiver);
        }

        public void UnregisterCountDownFinishedReceiver(ICountDownFinishedReceiver receiver)
        {
            receivers.Remove(receiver);
        }

        public void UnregisterAllCountDownFinishedReceivers()
        {
            receivers.Clear();
        }

        private void UpdateCountDownFinishedReceivers()
        {
            foreach (ICountDownFinishedReceiver receiver in receivers)
            {
                receiver.updateCountDownFinishedReceiver();
            }
        }
    }
}
