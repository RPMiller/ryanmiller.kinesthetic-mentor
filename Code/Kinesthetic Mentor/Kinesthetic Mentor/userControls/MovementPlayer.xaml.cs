﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Interfaces;

namespace Kinesthetic_Mentor.userControls
{
    /// <summary>
    /// Interaction logic for MovementPlayer.xaml
    /// </summary>
    public partial class MovementPlayer : UserControl, IMovementFinishedReceiver, ICountDownFinishedReceiver
    {
        private MovementPlayerManager manager;
        private IMovementFinishedReceiver receiver;

        public MovementPlayer()
        {
            InitializeComponent();
        }

        public void SetUp(Movement movement,IMovementFinishedReceiver receiver,int countDown = 3)
        {
            this.receiver = receiver;
            manager = new MovementPlayerManager(movement);
            manager.registerSkeletonReceiver(skeletonViewer);
            skeletonViewer.RegisterCountDownFinishedReceiver(this);
            skeletonViewer.StartCountDown(countDown);
        }

        private void MovementPlayer_Loaded(object sender, RoutedEventArgs e)
        {
            skeletonViewer.Unregister();
        }

        public void updateMovementFinishedReceiver()
        {
            receiver.updateMovementFinishedReceiver();
            manager.unregisterAllSkeletonReceivers();
        }

        public void updateCountDownFinishedReceiver()
        {
            manager.PlayMovement();
        }

        public MovementPlayerManager GetMovementPlayerManager()
        {
            return manager;
        }
    }
}
