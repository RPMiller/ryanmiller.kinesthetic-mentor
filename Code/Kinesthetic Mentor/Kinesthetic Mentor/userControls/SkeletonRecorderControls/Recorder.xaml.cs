﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;
using Kinesthetic_Mentor.Models;
using System.Windows.Media.Animation;
using System.Threading;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    /// <summary>
    /// Interaction logic for Recorder.xaml
    /// </summary>
    public partial class Recorder : UserControl, ICountDownFinishedReceiver, IMovementSelectedReceiver, IEntireMovemementWasDeletedReceiver
    {
        private MovementRecorder recorder = new MovementRecorder();
        private Editor editor = null;
        private delegate void MovementSelectorDelegate(String fileLocation);
        private MovementSelectorDelegate movementSelectorDelegate;
        private IEntireMovemementWasDeletedReceiver entireMovementWasDeletedReceiver;

        public Recorder(IEntireMovemementWasDeletedReceiver entireMovementWasDeletedReceiver = null)
        {
            InitializeComponent();
            this.entireMovementWasDeletedReceiver = entireMovementWasDeletedReceiver;
        }

        private void Record_Click(object sender, RoutedEventArgs e)
        {
            const String STOP_TOOL_TIP = "Stops the recording and sends it to the editor that is in front";
            recorderButton.ToolTip = STOP_TOOL_TIP;
            recorderButton.Click -= Record_Click;
            recorderButton.Click += Stop_Click;
            recorderButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/stop.png",UriKind.Relative)) };
            recorderButton.IsEnabled = false;
            skeletonViewer.StartCountDown(3);
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            const String RECORD_TOOL_TIP = "Starts recording your movements after the countdown finishes";
            recorderButton.ToolTip = RECORD_TOOL_TIP;
            recorderButton.Click += Record_Click;
            recorderButton.Click -= Stop_Click;
            recorderButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/record.png", UriKind.Relative)) };
            MovementPhase movementPhase = recorder.StopRecording();
            if (movementPhase.GetMovementFrames().Count > 0)
            {
                if (editor == null)
                {
                    Movement movement = new Movement();
                    movement.AddMovementPhase(movementPhase);
                    SetEditorMovement(movement);
                }
                else
                {
                    editor.AddMovementPhase(movementPhase);
                }
            }
        }

        private void SetEditorMovement(Movement movement)
        {
            if (editor == null)
            {
                editor = new Editor(this);
                Grid.SetColumn(editor, 2);
                screen.Children.Add(editor);
                ShrinkRecorder();
            }
            editor.SetMovement(movement);
        }

        public void updateCountDownFinishedReceiver()
        {
            recorderButton.IsEnabled = true;
            recorder.StartRecording();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            movementSelector.SetUp(this);
            movementSelectorDelegate = new MovementSelectorDelegate(LoadMovement);
            movementSelector.Visibility = System.Windows.Visibility.Visible;
            movementSelector.selectorLabel.Content = "Load";
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            movementSelector.SetUp(this);
            movementSelectorDelegate = new MovementSelectorDelegate(DeleteMovement);
            movementSelector.Visibility = System.Windows.Visibility.Visible;
            movementSelector.selectorLabel.Content = "Delete";
        }

        public void UpdateMovementSelectedReceiver(String fileLocation)
        {
            movementSelector.Visibility = System.Windows.Visibility.Collapsed;
            movementSelectorDelegate(fileLocation);
        }

        private void LoadMovement(String fileLocation)
        {
            int startIndex = fileLocation.LastIndexOf('/') + 1;
            String fileName = fileLocation.Substring(startIndex, fileLocation.Count() - startIndex).ToString();

            FlatFileIO io = new FlatFileIO();
            Movement loaded = io.ReadMovement(fileLocation);

            if (editor == null)
            {
                LoadMovementDisplayData(fileName, loaded);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Upon loading" + fileName + " do you wish to add your current phases to the end of the loaded movement? Click cancel if you do not wish to load " + fileName, "Save", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    Movement combinedMovement = new Movement(editor.movementEditor.GetMovement().GetMovementPhases().Concat(loaded.GetMovementPhases()).ToList());
                    SetEditorMovement(combinedMovement);
                }
                else if (result == MessageBoxResult.No)
                {
                    LoadMovementDisplayData(fileName, loaded);
                }
            }
        }

        private void LoadMovementDisplayData(String fileName, Movement loaded)
        {
            SetEditorMovement(loaded);
            editor.movementEditor.fileNameTextBox.Text = fileName;
            editor.movementEditor.LoadedMovementName = fileName;
            editor.movementEditor.videoLocationLabel.Content = loaded.IntroVideoLocation;
        }

        private void DeleteMovement(String fileLocation)
        {
            FlatFileIO io = new FlatFileIO();
            io.DeleteMovement(fileLocation);
        }

        private void Recorder_Loaded(object sender, RoutedEventArgs e)
        {
            skeletonViewer.RegisterCountDownFinishedReceiver(this);
        }

        public void UpdateEntireMovemementWasDeletedReceiver(Editor editorWithNoPhases)
        {
            if (entireMovementWasDeletedReceiver != null)
            {
                entireMovementWasDeletedReceiver.UpdateEntireMovemementWasDeletedReceiver(editor);
            }
            screen.Children.Remove(editor);
            GrowRecorder();
            editor = null;
        }

        private void ShrinkRecorder()
        {
            recorderPanel.BeginAnimation(StackPanel.HeightProperty, new DoubleAnimation(450, new Duration(new TimeSpan(0, 0, 1))));
            recorderPanel.BeginAnimation(StackPanel.WidthProperty, new DoubleAnimation(350, new Duration(new TimeSpan(0, 0, 1))));
            skeletonViewer.BeginAnimation(StackPanel.HeightProperty, new DoubleAnimation(350, new Duration(new TimeSpan(0, 0, 1))));
            skeletonViewer.BeginAnimation(StackPanel.WidthProperty, new DoubleAnimation(350, new Duration(new TimeSpan(0, 0, 1))));
            recorderPanel.BeginAnimation(StackPanel.MarginProperty, new ThicknessAnimation(new Thickness(0, 0, 40, 0), new Duration(new TimeSpan(0, 0, 1))));
            editor.BeginAnimation(StackPanel.MarginProperty, new ThicknessAnimation(new Thickness(40, 0, 0, 0), new Duration(new TimeSpan(0, 0, 1))));
        }

        private void GrowRecorder()
        {
            recorderPanel.BeginAnimation(StackPanel.HeightProperty, new DoubleAnimation(600, new Duration(new TimeSpan(0, 0, 1))));
            recorderPanel.BeginAnimation(StackPanel.WidthProperty, new DoubleAnimation(500, new Duration(new TimeSpan(0, 0, 1))));
            skeletonViewer.BeginAnimation(StackPanel.HeightProperty, new DoubleAnimation(500, new Duration(new TimeSpan(0, 0, 1))));
            skeletonViewer.BeginAnimation(StackPanel.WidthProperty, new DoubleAnimation(500, new Duration(new TimeSpan(0, 0, 1))));
            recorderPanel.BeginAnimation(StackPanel.MarginProperty, new ThicknessAnimation(new Thickness(0, 0, 0, 0), new Duration(new TimeSpan(0, 0, 1))));
        }
    }
}
