﻿using Kinesthetic_Mentor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    public abstract class EditorSliderConfig : ISliderConfig, IEditorStateButtonClickedReceiver
    {
        protected EditorState currentEditorState = null;
        protected int startModifierFrame = -1;
        protected int endModifierFrame = 0;
        protected int selectedStartFrame = -1;
        protected int selectedEndFrame = 0;
        protected Rectangle selectedRectangle = null;
        protected Rectangle selectedArea = null;
        protected double initialSelectPosition = -1;
        protected MovementSlider slider;
        protected IPhaseSelectedReceiver phaseSelecedReceiver;
        protected IMovementModifiedReceiver movementModifiedReceiver;
        protected bool isSliding = false;
        protected bool isAltSelectStart = true;
        protected List<EditorState> editorStates = new List<EditorState>();
        protected StackPanel editorPanel;
        protected bool isEditMode = false;


        public EditorSliderConfig()
        {
            EventManager.RegisterClassHandler(typeof(Window), Keyboard.PreviewKeyUpEvent, new KeyEventHandler(Key_Up), false);
            editorStates.Add(new EditorState(new EditorState.MouseDown(MouseDownShift),
                new EditorState.MouseUp(MouseUpShift),"Key:Escape then Shift When toggled you can drag phases to rearrange them",
                new Image(){Source = new BitmapImage(new Uri("../../Images/drag.png",UriKind.Relative))},
                this,new List<Key>(){Key.LeftShift, Key.RightShift}));
            editorStates.Add(new EditorState(new EditorState.MouseDown(MouseDownAlt),
                new EditorState.MouseUp(MouseUpAlt), "Key:Escape then Alt When toggled you can select a set of frames by dragging your mouse over them while pressing the left mouse button. \nRegardless of toggled Press enter to turn the selected area into a phase. \nRegardless of toggled Press delete to delete the selected area.",
                new Image() { Source = new BitmapImage(new Uri("../../Images/Select.png", UriKind.Relative)) },
                this, new List<Key>() { Key.System }));
            editorStates.Add(new EditorState(new EditorState.MouseDown(LeftDown),
                new EditorState.MouseUp(DummyMouseUp), "Key:Escape then N When toggled normal frame seeking is avaliable.",
                new Image() { Source = new BitmapImage(new Uri("../../Images/play.png", UriKind.Relative)) },
                this, new List<Key>() { Key.N }));
            HighlightState(editorStates[editorStates.Count - 1]);
        }

        protected void DummyMouseUp(double what) { }

        private StackPanel GenerateEditorButtonsPanel()
        {
            StackPanel panel = new StackPanel() { Orientation = Orientation.Horizontal };

            foreach(EditorState state in editorStates)
            {
                panel.Children.Add(state.EditorStateButton);
            }
            return panel;
        }

        public StackPanel GetEditorButtonsPanel()
        {
            if (editorPanel == null)
            {
                editorPanel = GenerateEditorButtonsPanel();
            }
            return editorPanel;
        }

        private Button GenerateNormalButton()
        {
            Button button = new Button();
            button.Height = 40;
            button.Background = new SolidColorBrush(Colors.Green);
            return button;
        }

        private void Key_Up(object sender, KeyEventArgs e)
        {
            if (isEditMode)
            {
                for (int i = 0; i < editorStates.Count; i++)
                {
                    if (editorStates[i].DoesStateActivateFromKey(e.Key))
                    {
                        currentEditorState = editorStates[i];
                        HighlightState(currentEditorState);
                    }
                }
                isEditMode = e.Key == Key.Escape;
            }
            else
            {
                isEditMode = e.Key == Key.Escape;
            }
        }

        protected void HighlightState(EditorState state)
        {
            Brush baseColor = (Brush)Application.Current.Resources["BackgroundBrush"];
            foreach (EditorState currentState in editorStates)
            {
                if (currentState.Equals(state))
                {
                    currentState.EditorStateButton.Background = new SolidColorBrush(Colors.Green);
                }
                else
                {
                    currentState.EditorStateButton.Background = baseColor;
                }
            }
        }

        protected void MouseDownAlt(MouseEventArgs e)
        {
            double cursorPosition = slider.GetCurrentPosistion(e);
            if (isAltSelectStart)
            {
                AltDownPaintRectangle(cursorPosition);
            }
            else
            {
                if (initialSelectPosition > cursorPosition)
                {
                    DrawAltRect(cursorPosition, initialSelectPosition);
                }
                else
                {
                    DrawAltRect(initialSelectPosition, cursorPosition);
                }

            }
        }

        protected void MouseUpAlt(double cursorPosition)
        {
            selectedStartFrame = startModifierFrame;
            selectedEndFrame = endModifierFrame;
            if (selectedStartFrame > selectedEndFrame)
            {
                int temp = selectedStartFrame;
                selectedStartFrame = selectedEndFrame;
                selectedEndFrame = temp;
            }
            isAltSelectStart = true;
        }

        protected void Bar_Mouse_Move(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && isSliding)
            {
                if (currentEditorState != null)
                {
                    currentEditorState.mouseDown(e);
                    endModifierFrame = slider.GetFrameFromMouse(e);
                }
                else
                {
                    LeftDown(e);
                }
            }
        }

        protected void DrawAltRect(double start, double stop)
        {
            Canvas.SetLeft(selectedArea, start);
            Canvas.SetRight(selectedArea, stop);
            selectedArea.Width = stop - start;
        }

        protected void Mouse_Up(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                isSliding = false;
                if (currentEditorState != null)
                {
                    currentEditorState.mouseUp(slider.GetCurrentPosistion(e));
                }
                startModifierFrame = -1;
            }
        }

        protected void AltDownPaintRectangle(double cursorPosition)
        {
            slider.mainCanvas.Children.Remove(selectedArea);
            selectedArea = new Rectangle();
            Canvas.SetTop(selectedArea, -2);
            Canvas.SetZIndex(selectedArea, 3);
            selectedArea.Height = 14;
            initialSelectPosition = cursorPosition;
            selectedArea.Fill = new SolidColorBrush(Colors.SeaGreen);
            selectedArea.Opacity = .75;
            Canvas.SetLeft(selectedArea, cursorPosition);
            slider.mainCanvas.Children.Add(selectedArea);
            isAltSelectStart = false;
        }

        protected abstract void KeyPressCleanUp();
        protected abstract void MouseDownShift(MouseEventArgs e);
        protected abstract void LeftDown(MouseEventArgs e);
        protected abstract void DeleteKeyUp();
        protected abstract void MouseUpShift(double cursorPosition);

        public abstract void SetUpSlider(MovementSlider slider);
        public abstract void Mouse_LeftDown(object sender, MouseButtonEventArgs e);
        public abstract void UpdateSkeletonReceiver();


        public bool GetIsSliding()
        {
            return isSliding;
        }

        public void UpdateEditorStateButtonClickedReceiver(EditorState state)
        {
            currentEditorState = state;
            HighlightState(currentEditorState);
        }
    }
}
