﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    /// <summary>
    /// Interaction logic for Editor.xaml
    /// </summary>
    public partial class Editor : UserControl, IPhaseSelectedReceiver, ISaveReceiver,IElementFocusChangedReceiver, IMovementModifiedReceiver
    {
        private IPhaseAddedReceiver currentReceiver;
        private IList<PhaseEditor> phaseEditors = new List<PhaseEditor>();
        private SolidColorBrush borderBrush = new SolidColorBrush(Colors.Green);
        private PhaseEditor currentPhaseEditor;
        private CarouselViewer carosuelViewer;
        private StackPanel phaseEditorPanel = new StackPanel();
        public MovementEditor movementEditor { get; private set; }
        private ISaveReceiver saveReceiver;
        private IEntireMovemementWasDeletedReceiver entireMovementWasDeletedReceiver;

        public Editor(IEntireMovemementWasDeletedReceiver entireMovementWasDeletedReceiver)
        {
            InitializeComponent();
            movementEditor = new MovementEditor();
            movementEditor.BorderBrush = borderBrush;
            currentReceiver = movementEditor;
            movementEditor.SetPhaseSelectedReceiver(this,this,this);
            movementEditor.BorderThickness = new Thickness(5);
            this.entireMovementWasDeletedReceiver = entireMovementWasDeletedReceiver;
        }

        public void AddMovementPhase(MovementPhase movementPhase)
        {
            currentReceiver.UpdatePhaseAddedReceiver(movementPhase);
            if (currentReceiver == movementEditor)
            {
                CreatePhaseEditor(movementPhase);
            }
        }

        public void UpdatePhaseSelectedReceiver(int phaseIndex)
        {
            phaseEditorPanel.Children.Remove(currentPhaseEditor);
            currentPhaseEditor = phaseEditors[phaseIndex];
            phaseEditorPanel.Children.Add(currentPhaseEditor);
            currentPhaseEditor.IsEnabled = true;
            carosuelViewer.RotateToElement(phaseEditorPanel);
        }

        public void SetMovement(Movement movement, ISaveReceiver saveReceiver = null)
        {
            if (movement.GetTotalNumberOfFrames() == 0)
            {
                entireMovementWasDeletedReceiver.UpdateEntireMovemementWasDeletedReceiver(this);
            }
            else
            {
                this.saveReceiver = saveReceiver;
                phaseEditors.Clear();
                movementEditor.SetMovement(movement);
                foreach (MovementPhase phase in movement.GetMovementPhases())
                {
                    CreatePhaseEditor(phase);
                }
                phaseEditorPanel.Children.Remove(currentPhaseEditor);
                currentPhaseEditor = phaseEditors[0];
                phaseEditorPanel.Children.Add(currentPhaseEditor);
                currentPhaseEditor.IsEnabled = true;
                if (carosuelViewer == null)
                {
                    List<UIElement> elements = new List<UIElement>() { movementEditor, phaseEditorPanel };
                    carosuelViewer = new CarouselViewer(elements, this);
                    mainPanel.Children.Add(carosuelViewer);
                    carosuelViewer.SetSizeBasedOnElements();
                }
                SwapFocus(movementEditor, currentPhaseEditor);
                carosuelViewer.RotateToElement(movementEditor);
            }
        }

        private void CreatePhaseEditor(MovementPhase phase)
        {
            PhaseEditor editor = new PhaseEditor();
            editor.SetMovementPhase(phase);
            phaseEditors.Add(editor);
            editor.BorderBrush = borderBrush;
            editor.IsEnabled = false;
        }

        private void SwapFocus(UserControl elementToHaveFocus, UserControl elementToLoseFocus)
        {
            elementToHaveFocus.BorderThickness = new Thickness(5);
            elementToLoseFocus.BorderThickness = new Thickness(0);
            currentReceiver = elementToHaveFocus as IPhaseAddedReceiver;
        }

        public void UpdateSaveReceiver()
        {
            if (!String.IsNullOrEmpty(movementEditor.fileNameTextBox.Text))
            {
                foreach (PhaseEditor editor in phaseEditors)
                {
                    editor.Save();
                }
                movementEditor.Save();
                if (movementEditor.HasNameChanged() && saveReceiver != null)
                {
                    saveReceiver.UpdateSaveReceiver();
                }
            }
            else
            {
                MessageBox.Show("There was no name given to the movement. Please fill out the text box for file name.");
            }
        }

        public void UpdateElementFocusChangedReceiver(UIElement elementInFocus)
        {
            if (elementInFocus == movementEditor)
            {
                SwapFocus(movementEditor, currentPhaseEditor);
            }
            else
            {
                SwapFocus(currentPhaseEditor, movementEditor);
            }
        }

        public void UpdateMovementModifiedReceiver()
        {
            SetMovement(movementEditor.GetMovement(),saveReceiver);
        }
    }
}
