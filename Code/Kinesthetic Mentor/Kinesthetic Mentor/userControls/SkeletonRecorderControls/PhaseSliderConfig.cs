﻿using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    public class PhaseSliderConfig : EditorSliderConfig
    {
        private int totalPhaseFrames;
        private CommonMistake mistake;
        private ISkeletonReceiver receiver;
        private CommonMistakeEditor mistakeEditor;

        public PhaseSliderConfig(int totalPhaseFrames, CommonMistake mistake, IEntireCommonMistakeWasDeletedReceiver mistakeDeletedReceiver)
        {
            if (totalPhaseFrames > mistake.StartIndex)
            {
                this.totalPhaseFrames = totalPhaseFrames;
                this.mistake = mistake;
                mistakeEditor = new CommonMistakeEditor(mistake);
                mistakeEditor.IsEnabled = false;
                if (mistake.EndIndex > totalPhaseFrames)
                {
                    TrimMistakeToTotalPhaseFrameLength();
                }
            }
            else
            {
                mistakeDeletedReceiver.UpdateEntireCommonMistakeWasDeletedReceiver();
            }
            ChangeAltToolTip();
        }

        private void ChangeAltToolTip()
        {
            foreach (EditorState state in editorStates)
            {
                if (state.DoesStateActivateFromKey(Key.System))
                {
                    state.EditorStateButton.ToolTip = "Key:Alt When toggled you can select a set of frames by dragging your mouse over them while pressing the left mouse button. \nRegardless of toggled Press delete to delete the selected area.";
                }
            }
        }

        public CommonMistake GetCommonMistake()
        {
            return mistakeEditor.GetCommonMistake();
        }

        public CommonMistakeEditor GetMistakeEditor()
        {
            return mistakeEditor;
        }

        public void SetISkeletonReceiver(ISkeletonReceiver receiver)
        {
            this.receiver = receiver;
        }

        public override void SetUpSlider(MovementSlider slider)
        {
            EventManager.RegisterClassHandler(typeof(Window), Mouse.MouseMoveEvent, new MouseEventHandler(Bar_Mouse_Move), true);
            this.slider = slider;
            selectedRectangle = new Rectangle();
            Canvas.SetLeft(selectedRectangle, ((double)mistake.StartIndex / totalPhaseFrames) * slider.sliderBar.Width);
            Canvas.SetZIndex(selectedRectangle, 1);
            Canvas.SetZIndex(slider.seekerCircle, 2);
            selectedRectangle.Height = 10;
            selectedRectangle.Width = (((double)mistake.EndIndex - mistake.StartIndex) / totalPhaseFrames) * slider.sliderBar.Width;
            selectedRectangle.Fill = new SolidColorBrush(Colors.Blue);
            slider.mainCanvas.Children.Add(selectedRectangle);
            slider.TotalFrames = totalPhaseFrames;
            EventManager.RegisterClassHandler(typeof(Window), Mouse.MouseUpEvent, new MouseButtonEventHandler(Mouse_Up), true);
        }

        private void TrimMistakeToTotalPhaseFrameLength()
        {
            MessageBox.Show("The common mistake recorded was larger than the lengths of the phase, therefore the end of the phase was cut off");
            mistake.MistakePhase.RemoveFrames(totalPhaseFrames + 1, mistake.EndIndex);
            mistake.EndIndex = totalPhaseFrames;
        }

        public override void Mouse_LeftDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            isSliding = true;
            if (startModifierFrame == -1 && ModifierKeyPressed())
            {
                startModifierFrame = slider.GetFrameFromMouse(e);
            }
            else
            {
                LeftDown(e);
            }
        }

        private bool ModifierKeyPressed()
        {
            return (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift) ||
                Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt));
        }

        protected override void MouseUpShift(double cursorPosition)
        {
            if (startModifierFrame >= 0)
            {
                if (Canvas.GetLeft(selectedRectangle) + selectedRectangle.Width > slider.mainCanvas.Width)
                {
                    Canvas.SetLeft(selectedRectangle, slider.mainCanvas.Width - selectedRectangle.Width);
                    mistake.StartIndex = slider.GetFrameFromMouse(new MouseEventArgs(Mouse.PrimaryDevice, 0));
                    mistake.EndIndex = totalPhaseFrames;
                }
                else
                {
                    mistake.StartIndex = (int)Canvas.GetLeft(selectedRectangle);
                    mistake.EndIndex = mistake.StartIndex + mistake.MistakePhase.GetMovementFrames().Count;
                }
            }
        }

        protected override void MouseDownShift(MouseEventArgs e)
        {
            double cursorPosition = slider.GetCurrentPosistion(e);
            if (cursorPosition > slider.mainCanvas.Width - selectedRectangle.Width)
            {
                cursorPosition = slider.mainCanvas.Width - selectedRectangle.Width;
            }
            Canvas.SetLeft(selectedRectangle, cursorPosition);
        }

        public bool KeyPressed(Key key)
        {
            bool shouldHandle = false;
            if (selectedEndFrame != -1 || endModifierFrame != -1)
            {
                if (key.Equals(Key.Delete) && selectedStartFrame < mistake.EndIndex && selectedStartFrame >= mistake.StartIndex && selectedEndFrame >0)
                {
                    shouldHandle = true;
                    Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new Action(DeleteKeyUp));
                }
            }
            return shouldHandle;
        }

        protected override void DeleteKeyUp()
        {
            mistake.MistakePhase.RemoveFrames(selectedStartFrame - mistake.StartIndex, selectedEndFrame - mistake.StartIndex);
            ResetCommonMistake(selectedStartFrame);
            KeyPressCleanUp();
        }

        private void ResetCommonMistake(int startFrame)
        {
            int numberOfFrames = mistake.MistakePhase.GetMovementFrames().Count();
            int framesOver = selectedStartFrame + numberOfFrames - totalPhaseFrames;
            if (selectedStartFrame <= mistake.StartIndex)
            {
                mistake.StartIndex = selectedEndFrame;
            }
            mistake.EndIndex = mistake.StartIndex + numberOfFrames;
            selectedRectangle.Width = ((double)numberOfFrames / totalPhaseFrames) * slider.sliderBar.Width;
            Canvas.SetLeft(selectedRectangle,((double)mistake.StartIndex / totalPhaseFrames) * slider.sliderBar.Width);
        }

        protected override void KeyPressCleanUp()
        {
            slider.mainCanvas.Children.Remove(selectedArea);
            slider.SeekFrame(0);
            slider.UpdateFrameSeekedReceiverToCurrentFrame();
            selectedStartFrame = -1;
            selectedEndFrame = -1;
        }

        protected override void LeftDown(MouseEventArgs e)
        {
            if (startModifierFrame == -1)
            {
                startModifierFrame = slider.GetFrameFromMouse(e);
            }
            int currentFrame = slider.GetFrameFromMouse(e);
            slider.SeekFrame(currentFrame);
            UpdateSkeletonReceiver();
        }

        public override void UpdateSkeletonReceiver()
        {
            int currentFrame = slider.GetCurrentFrame();
            if (currentFrame >= mistake.StartIndex && currentFrame < mistake.EndIndex)
            {
                if (receiver != null)
                {
                    receiver.UpdateSkeleton(mistake.MistakePhase.GetMovementFrames()[currentFrame - mistake.StartIndex].GetSkeleton());
                }
            }
        }
    }
}
