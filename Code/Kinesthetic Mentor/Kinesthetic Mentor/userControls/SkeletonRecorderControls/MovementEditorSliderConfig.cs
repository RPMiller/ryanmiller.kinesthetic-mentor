﻿using Kinesthetic_Mentor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    public class MovementEditorSliderConfig : EditorSliderConfig
    {
        public MovementEditorSliderConfig(IPhaseSelectedReceiver receiver, IMovementModifiedReceiver movementModifiedReceiver)
        {
            this.phaseSelecedReceiver = receiver;
            this.movementModifiedReceiver = movementModifiedReceiver;
            editorStates.Add(new EditorState(new EditorState.MouseDown(ControlDown),
                new EditorState.MouseUp(DummyMouseUp), "Key:Escape then Ctrl When toggled if you click on a phase the phase editor will change to reflect the phase you clicked on",
                new Image() { Source = new BitmapImage(new Uri("../../Images/Click.png", UriKind.Relative)) },
                this, new List<Key>() { Key.LeftCtrl, Key.RightCtrl },true));
        }

        public override void SetUpSlider(MovementSlider slider)
        {
            this.slider = slider;
            EventManager.RegisterClassHandler(typeof(Window), Mouse.MouseMoveEvent, new MouseEventHandler(Bar_Mouse_Move), true);
            EventManager.RegisterClassHandler(typeof(Window), Keyboard.KeyUpEvent, new KeyEventHandler(Key_Up), true);
            EventManager.RegisterClassHandler(typeof(Window), Mouse.MouseUpEvent, new MouseButtonEventHandler(Mouse_Up), true);
        }

        private void Key_Up(object sender, KeyEventArgs e)
        {
            if (selectedStartFrame != -1)
            {
                if (e.Key.Equals(Key.Delete))
                {
                    Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new Action(DeleteKeyUp));
                }
                else if (e.Key.Equals(Key.Enter))
                {
                    Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new Action(EnterKeyUp));
                }
            }
        }

        private void EnterKeyUp()
        {
            slider.GetMovement().MergeFramesIntoPhase(selectedStartFrame, selectedEndFrame, slider);
            if (selectedEndFrame != selectedStartFrame)
            {
                movementModifiedReceiver.UpdateMovementModifiedReceiver();
            }
            KeyPressCleanUp();
        }

        protected override void KeyPressCleanUp()
        {
            slider.mainCanvas.Children.Remove(selectedArea);
            selectedArea = null;
            slider.ResetMovement();
            slider.SeekFrame(0);
            slider.UpdateFrameSeekedReceiverToCurrentFrame();
            selectedStartFrame = -1;
        }

        protected override void MouseDownShift(MouseEventArgs e)
        {
            double cursorPosition = slider.GetCurrentPosistion(e);
            if (selectedRectangle == null)
            {
                FindSelectedRectangle(cursorPosition);
            }
            else
            {
                if (cursorPosition > slider.mainCanvas.Width - selectedRectangle.Width)
                {
                    cursorPosition = slider.mainCanvas.Width - selectedRectangle.Width;
                }
                Canvas.SetLeft(selectedRectangle, cursorPosition);
            }
        }

        private void FindSelectedRectangle(double cursorPosition)
        {
            bool rectangleFound = false;
            for (int i = 2; i < slider.mainCanvas.Children.Count; i++)
            {
                Rectangle rectangle = (Rectangle)slider.mainCanvas.Children[i];
                double leftBound = Canvas.GetLeft(rectangle);
                double rightBound = leftBound + rectangle.Width;
                rectangleFound = cursorPosition >= leftBound && cursorPosition <= rightBound;
                if (rectangleFound)
                {
                    selectedRectangle = rectangle;
                    Canvas.SetZIndex(selectedRectangle, 2);
                }
            }
        }

        protected void ControlDown(MouseEventArgs e)
        {
            int currentFrame = slider.GetFrameFromMouse(e);
            int phaseIndex = slider.GetMovement().GetPhaseIndexAtFrameIndex(currentFrame);
            phaseSelecedReceiver.UpdatePhaseSelectedReceiver(phaseIndex);
        }

        protected override void LeftDown(MouseEventArgs e)
        {
            if (startModifierFrame == -1)
            {
                startModifierFrame = slider.GetFrameFromMouse(e);
            }
            else
            {
                int currentFrame = slider.GetFrameFromMouse(e);
                slider.SeekFrame(currentFrame);
                slider.UpdateFrameSeekedReceiver(currentFrame);
            }
        }

        public override void Mouse_LeftDown(object sender, MouseButtonEventArgs e)
        {
            if (currentEditorState != null && currentEditorState.IsLeftDown)
            {
                currentEditorState.mouseDown(e);
            }
            isSliding = true;
            LeftDown(e);
        }

        private bool ModifierKeyPressed()
        {
            return (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift) ||
                Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt) ||
                Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl));
        }

        protected override void MouseUpShift(double cursorPosition)
        {
            if (startModifierFrame >= 0)
            {
                for (int i = 2; i < slider.mainCanvas.Children.Count; i++)
                {
                    Rectangle rectangle = (Rectangle)slider.mainCanvas.Children[i];
                    double leftBound = Canvas.GetLeft(rectangle);
                    double rightBound = leftBound + rectangle.Width;
                    if (cursorPosition >= leftBound && cursorPosition <= rightBound && !rectangle.Equals(selectedRectangle))
                    {
                        int firstIndex = slider.GetMovement().GetPhaseIndexAtFrameIndex(startModifierFrame);
                        int secondIndex = slider.GetMovement().GetPhaseIndexAtFrameIndex(endModifierFrame);
                        slider.MoveFirstInFrontOfSecond(firstIndex, secondIndex);
                    }
                }
                slider.ResetMovement();
                selectedRectangle = null;
            }
        }

        protected override void DeleteKeyUp()
        {
            slider.GetMovement().RemoveFrames(selectedStartFrame, selectedEndFrame, slider);
            if (selectedEndFrame != selectedStartFrame && movementModifiedReceiver != null)
            {
                movementModifiedReceiver.UpdateMovementModifiedReceiver();
            }
            KeyPressCleanUp();
        }

        public override void UpdateSkeletonReceiver()
        {
            slider.UpdateFrameSeekedReceiverToCurrentFrame();
        }
    }
}
