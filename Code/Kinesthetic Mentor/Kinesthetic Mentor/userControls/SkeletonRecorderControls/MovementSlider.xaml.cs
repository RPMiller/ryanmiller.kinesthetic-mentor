﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Models;
using System.Collections;
using Kinesthetic_Mentor.Interfaces;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    /// <summary>
    /// Interaction logic for MovementSlider.xaml
    /// </summary>
    public partial class MovementSlider : UserControl , IPhaseDeletedReceiver
    {
        public int TotalFrames { get; set; }
        private int currentFrame = 0;
        private Movement movement = new Movement();
        private IList<Color> usedColors = new List<Color>();
        private ISliderReceiver sliderReceiver;
        private ISliderConfig config;
        private Random randomGen = new Random(0);

        public MovementSlider()
        {
            InitializeComponent();
            TotalFrames = 1;
        }

        public void SetUp(ISliderReceiver receiver,ISliderConfig config)
        {
            this.sliderReceiver = receiver;
            usedColors.Add(Colors.Red);
            usedColors.Add(Colors.Green);
            usedColors.Add(Colors.Blue);
            usedColors.Add(Colors.Gold);
            usedColors.Add(Colors.Silver);
            usedColors.Add(Colors.Black);
            usedColors.Add(Colors.Gray);
            usedColors.Add(Colors.Orange);
            config.SetUpSlider(this);
            this.config = config;
            mainCanvas.MouseLeftButtonDown += config.Mouse_LeftDown;
            SeekFrame(0);
        }

        public ISliderConfig GetConfig()
        {
            return config;
        }

        public int GetCurrentFrame()
        {
            return currentFrame;
        }

        public void UnregisterSliderReceiver()
        {
            sliderReceiver = null;
        }

        public void RegisterSliderReceiver(ISliderReceiver receiver)
        {
            sliderReceiver = receiver;
        }

        public int GetFrameFromMouse(MouseEventArgs e)
        {
            int currentFrame = (int)((TotalFrames - 1) * (e.GetPosition(mainCanvas).X / sliderBar.Width));
            if (currentFrame < 0)
            {
                currentFrame = 0;
            }
            else if (currentFrame > TotalFrames - 1)
            {
                currentFrame = TotalFrames - 1;
            }
            return currentFrame;
        }

        public double GetCurrentPosistion(MouseEventArgs e)
        {
            double position = e.GetPosition(mainCanvas).X;
            if (position < 0)
            {
                position = 0;
            }
            else if (position > mainCanvas.Width)
            {
                position = mainCanvas.Width;
            }
            return position;
        }

        public void SetMovement(Movement movement)
        {
            TotalFrames = movement.GetTotalNumberOfFrames();
            int phaseStart = 0; 
            this.movement = movement;
            GenerateNewColors();
            ElementsChanged();
            for (int i = 0; i < movement.GetMovementPhases().Count;i++ )
            {
                MovementPhase phase = movement.GetMovementPhases()[i];
                DrawPhaseRectangle(phaseStart, phase.GetMovementFrames().Count, usedColors[i]);
                phaseStart += phase.GetMovementFrames().Count;
            }
        }

        private void GenerateNewColors()
        {
            for(int i = usedColors.Count; i < movement.GetMovementPhases().Count; i++)
            {
                Color randomColor = Color.FromRgb((byte)randomGen.Next(150, 255), (byte)randomGen.Next(85, 190), (byte)randomGen.Next(150, 255));
                usedColors.Add(randomColor);
            }
        }

        private void ElementsChanged()
        {
            TotalFrames = movement.GetTotalNumberOfFrames();
            mainCanvas.Children.Clear();
            mainCanvas.Children.Add(seekerCircle);
            Canvas.SetZIndex(seekerCircle, 3);
            mainCanvas.Children.Add(sliderBar);
            Canvas.SetZIndex(sliderBar, 0);
        }

        private void DrawPhaseRectangle(int startIndex,int phaseLength,Color color)
        {
            Rectangle rectangle = new Rectangle();
            rectangle.Width = (mainCanvas.Width / TotalFrames) * phaseLength;
            rectangle.Height = 10;
            rectangle.Fill = new SolidColorBrush(color);
            Canvas.SetLeft(rectangle, (mainCanvas.Width / TotalFrames) * startIndex);
            Canvas.SetZIndex(rectangle, 1);
            mainCanvas.Children.Add(rectangle);
        }

        public bool IsAtEnd()
        {
            return currentFrame >= TotalFrames - 1 && (config != null && !config.GetIsSliding());
        }

        public void SeekNextFrame()
        {
            SeekFrame(currentFrame + 1);
        }

        public void SeekFrame(int currentFrame)
        {
            Canvas.SetLeft(seekerCircle, (((double)currentFrame / (double)TotalFrames) * mainCanvas.Width) - (seekerCircle.Width / 2) - 5);
            this.currentFrame = currentFrame;
        }

        public bool GetIsSliding()
        {
            return config.GetIsSliding();
        }

        public Movement GetMovement()
        {
            return movement;
        }

        public void UpdateFrameSeekedReceiverToCurrentFrame()
        {
            if (sliderReceiver != null)
            {
                sliderReceiver.UpdateFrameSeeked(currentFrame);
            }
        }

        public void UpdateFrameSeekedReceiver(int frame)
        {
            if (sliderReceiver != null)
            {
                sliderReceiver.UpdateFrameSeeked(frame);
            }
        }

        public void ResetMovement()
        {
            SetMovement(movement);
        }

        public int GetTotalFrames()
        {
            return TotalFrames;
        }

        public void MoveFirstInFrontOfSecond(int firstIndex, int secondIndex)
        {
            MovementPhase tempPhase = movement.GetMovementPhases()[firstIndex];
            movement.GetMovementPhases().Remove(tempPhase);
            movement.GetMovementPhases().Insert(secondIndex,tempPhase);
            SwapColors(firstIndex, secondIndex);
        }

        private void SwapColors(int firstIndex, int secondIndex)
        {
            Color tempColor = usedColors[firstIndex];
            usedColors.Remove(tempColor);
            usedColors.Insert(secondIndex, tempColor);
        }

        public void UpdatePhaseDeletedReceiver(int phaseIndex)
        {
            int nextColorIndex = usedColors.Count - 1;
            if (nextColorIndex < usedColors.Count)
            {
                GenerateSingleNewColor();
            }
            SwapColors(phaseIndex, nextColorIndex);
        }

        public void UpdatePhaseMovedReceiver(int phaseIndex)
        {
            int nextColorIndex = phaseIndex + 1;
            if (nextColorIndex >= usedColors.Count)
            {
                GenerateSingleNewColor();
            }
            SwapColors(phaseIndex, nextColorIndex);
        }

        private void GenerateSingleNewColor()
        {
            Color randomColor = Color.FromRgb((byte)randomGen.Next(150, 255), (byte)randomGen.Next(85, 190), (byte)randomGen.Next(150, 255));
            usedColors.Add(randomColor);
        }
    }
}