﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;
using System.IO;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    /// <summary>
    /// Interaction logic for MovementEditor.xaml
    /// </summary>
    public partial class MovementEditor : UserControl, IPhaseAddedReceiver, ISkeletonReceiver, ISliderReceiver,IPlayerActionReceiver
    {
        private MovementPlayerManager manager;
        private IPhaseSelectedReceiver phaseSelectedReceiver;
        private ISaveReceiver saveReceiver;
        public String LoadedMovementName { get; set; }
        private bool hasFinished = false;
        private bool hasNameChanged = false;

        public MovementEditor()
        {
            InitializeComponent();
            playerPanel.SetPlayerReceiver(this);
            skeletonViewer.SetColorToMentor();
        }

        public void SetPhaseSelectedReceiver(IPhaseSelectedReceiver phaseSelectedReceiver,ISaveReceiver saveReceiver,IMovementModifiedReceiver modifiedReceiver)
        {
            this.phaseSelectedReceiver = phaseSelectedReceiver;
            EditorSliderConfig config = new MovementEditorSliderConfig(phaseSelectedReceiver, modifiedReceiver);
            playerPanel.slider.SetUp(this,config);
            stateButtonPanelGrid.Children.Add(config.GetEditorButtonsPanel());
            this.saveReceiver = saveReceiver;
        }

        public bool HasNameChanged()
        {
            return hasNameChanged;
        }

        public void SetMovement(Movement movement)
        {
            if (manager != null)
            {
                manager.unregisterAllSkeletonReceivers();
            }
            playerPanel.slider.SetMovement(movement);
            fileNameTextBox.Text = movement.Name;
            videoLocationLabel.Content = movement.IntroVideoLocation;
            playerPanel.slider.SeekFrame(0);
            manager = new MovementPlayerManager(movement);
            manager.registerSkeletonReceiver(this);
            percentNeededTextBox.Text = movement.PercentOfFramesCorrectNeeded.ToString();
        }

        public Movement GetMovement()
        {
            return manager.GetMovement();
        }

        public void UpdateSkeleton(MySkeleton skeleton)
        {
            if (playerPanel.slider.IsAtEnd())
            {
                playerPanel.ChangeToReplay();
                hasFinished = true;
            }
            playerPanel.slider.SeekNextFrame();
            skeletonViewer.UpdateSkeleton(skeleton);
        }

        private void Editor_Initialized(object sender, EventArgs e)
        {
            skeletonViewer.Unregister();
        }

        public void Save()
        {
            String fileName = FlatFileIO.GetFolderLocation() + fileNameTextBox.Text;

            if (!String.IsNullOrEmpty(fileName))
            {
                bool shouldSave = true;
                if (FlatFileIO.FileExists(fileNameTextBox.Text + FlatFileIO.GetMovementFileExtension()))
                {
                    shouldSave = MessageBox.Show("A file with the name " + fileNameTextBox.Text + " already exists. Are you sure you want to save?", "Save", MessageBoxButton.YesNo) == MessageBoxResult.Yes;
                }
                if (shouldSave)
                {
                    FlatFileIO io = new FlatFileIO();
                    if (videoLocationLabel.Content != null)
                    {
                        manager.GetMovement().IntroVideoLocation = videoLocationLabel.Content.ToString();
                    }
                    hasNameChanged = manager.GetMovement().Name != fileNameTextBox.Text;
                    double percentNeeded = manager.GetMovement().PercentOfFramesCorrectNeeded;
                    Double.TryParse(percentNeededTextBox.Text, out percentNeeded);
                    if(percentNeeded > 100 || percentNeeded <=0)
                    {
                        percentNeeded = 50;
                    }
                    manager.GetMovement().PercentOfFramesCorrectNeeded = percentNeeded;
                    manager.GetMovement().Name = fileNameTextBox.Text;
                    io.SaveMovement(manager.GetMovement(), fileName);
                    LoadedMovementName = fileNameTextBox.Text;
                    MessageBox.Show("Movement saved");
                }
            }
        }

        public void UpdateFrameSeeked(int index)
        {
            manager.SeekFrame(index);
            if (hasFinished)
            {
                playerPanel.Replay();
                hasFinished = false;
                manager.PlayMovement();
            }
        }

        public void UpdateMovementEdited(Movement movement)
        {
            SetMovement(movement);
        }

        private void BrowseForIntroVideo_Click(object sender, RoutedEventArgs e)
        {
            String fileFilter = "MOV Files (*.MOV)|*.MOV|ASF Files (*.ASF)|*.ASF|AVI Files (*.AVI)|*.AVI|DVR-MS Files (*.DVR-MS)|*.DVR-MS|WMV Files (*.WMV)|*.WMV" +
            "|IFO Files (*.IFO)|*.IFO|M1V Files (*.M1V)|*.M1V|MPEG Files (*.MPEG)|*.MPEG|MPG Files (*.MPG)|*.MPG|VOB Files (*.VOB)|*.VOB|WM Files (*.WM)|*.WM";
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog() { Filter = fileFilter };
            Nullable<bool> result = fileDialog.ShowDialog();
            if (result.GetValueOrDefault(false))
            {
                videoLocationLabel.Content = fileDialog.FileName;
            }
        }

        public void UpdatePhaseSelectedReceiver(int phaseIndex)
        {
            throw new NotImplementedException();
        }

        public void UpdatePhaseAddedReceiver(Models.MovementPhase phase)
        {
            Movement movement = manager.GetMovement();
            movement.AddMovementPhase(phase);
            SetMovement(movement);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            saveReceiver.UpdateSaveReceiver();
        }

        public void PlayReceived()
        {
            manager.PlayMovement();
        }

        public void PauseReceived()
        {
            manager.PauseMovement();
        }

        public void StopReceived()
        {
            manager.Restart();
        }

        public void ReplayReceived()
        {
            hasFinished = false;
            manager.Replay();
        }

        private void fileNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (manager != null)
            {
                manager.GetMovement().Name = ((TextBox)sender).Text;
            }
        }
    }
}
