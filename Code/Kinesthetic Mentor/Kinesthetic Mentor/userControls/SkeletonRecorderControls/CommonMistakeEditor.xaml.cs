﻿using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    /// <summary>
    /// Interaction logic for CommonMistakeEditor.xaml
    /// </summary>
    public partial class CommonMistakeEditor : UserControl
    {
        private CommonMistake mistake;

        public CommonMistakeEditor(CommonMistake mistake)
        {
            InitializeComponent();
            this.mistake = mistake;
            messageTextBlock.Text = mistake.Message;
            videoLocationLabel.Content = mistake.MistakeVideoLocation;
            phaseNameTextBox.Text = mistake.MistakePhase.Name;
        }

        public CommonMistake GetCommonMistake()
        {
            return mistake;
        }

        public bool HasChanged()
        {
            return !(mistake.Message == messageTextBlock.Text && mistake.MistakePhase.Name == phaseNameTextBox.Text && mistake.MistakeVideoLocation == videoLocationLabel.Content as String) && (!String.IsNullOrEmpty(phaseNameTextBox.Text) && !String.IsNullOrEmpty(messageTextBlock.Text));
        }

        public void Save()
        {
            mistake.Message = messageTextBlock.Text;
            mistake.MistakeVideoLocation = videoLocationLabel.Content as String;
            mistake.MistakePhase.Name = phaseNameTextBox.Text;
        }

        private void BrowseForIntroVideo_Click(object sender, RoutedEventArgs e)
        {
            String fileFilter = "MOV Files (*.MOV)|*.MOV|ASF Files (*.ASF)|*.ASF|AVI Files (*.AVI)|*.AVI|DVR-MS Files (*.DVR-MS)|*.DVR-MS|WMV Files (*.WMV)|*.WMV" +
            "|IFO Files (*.IFO)|*.IFO|M1V Files (*.M1V)|*.M1V|MPEG Files (*.MPEG)|*.MPEG|MPG Files (*.MPG)|*.MPG|VOB Files (*.VOB)|*.VOB|WM Files (*.WM)|*.WM";
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog() { Filter = fileFilter };
            Nullable<bool> result = fileDialog.ShowDialog();
            if (result.GetValueOrDefault(false))
            {
                videoLocationLabel.Content = fileDialog.FileName;
                mistake.MistakeVideoLocation = fileDialog.FileName;
            }
        }

        private void phaseNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            mistake.MistakePhase.Name = phaseNameTextBox.Text;
        }

        private void Message_TextChanged(object sender, TextChangedEventArgs e)
        {
            mistake.Message = messageTextBlock.Text;
        }
    }
}