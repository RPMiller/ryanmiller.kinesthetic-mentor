﻿using Kinesthetic_Mentor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    /// <summary>
    /// Interaction logic for CommonMistakeSlider.xaml
    /// </summary>
    public partial class CommonMistakeSlider : UserControl, IEntireCommonMistakeWasDeletedReceiver
    {
        private IDeleteMeReceiver receiver;
        public bool ShouldAdd { get; private set; }

        public CommonMistakeSlider(IDeleteMeReceiver receiver)
        {
            InitializeComponent();
            this.receiver = receiver;
            ShouldAdd = true;
        }

        private void DeleteME_Click(object sender, RoutedEventArgs e)
        {
            receiver.UpdateDeleteMeReceiver(this);
        }

        public void UpdateEntireCommonMistakeWasDeletedReceiver()
        {
            ShouldAdd = false;
        }
    }
}
