﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Kinesthetic_Mentor.Interfaces;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    public class EditorState
    {
        public delegate void MouseUp(double cursorPosition);
        public delegate void MouseDown(MouseEventArgs e);
        public MouseDown mouseDown { get; private set; }
        public MouseUp mouseUp { get; private set; }
        public Button EditorStateButton { get; set; }
        private IEditorStateButtonClickedReceiver receiver;
        private List<Key> keys;
        public bool IsLeftDown { get; private set; }

        public EditorState(MouseDown md, MouseUp mu, String toolTip, Image image, IEditorStateButtonClickedReceiver receiver,List<Key> keys, bool isLeftDown = false)
        {
            mouseDown = md;
            mouseUp = mu;
            EditorStateButton = new Button { ToolTip = toolTip };
            this.receiver = receiver;
            EditorStateButton.Click += EditorStateButton_Click;
            EditorStateButton.Content = image;
            EditorStateButton.Height = 40;
            this.keys = keys;
            this.IsLeftDown = isLeftDown;
        }

        public bool DoesStateActivateFromKey(Key key)
        {
            bool isFound = false;

            foreach (Key k in keys)
            {
                if (k.Equals(key))
                {
                    isFound = true;
                }
            }

            return isFound;
        }

        void EditorStateButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            receiver.UpdateEditorStateButtonClickedReceiver(this);
        }
    }
}
