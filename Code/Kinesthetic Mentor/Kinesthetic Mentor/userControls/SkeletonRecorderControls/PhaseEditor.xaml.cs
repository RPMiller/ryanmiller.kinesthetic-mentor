﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    /// <summary>
    /// Interaction logic for PhaseEditor.xaml
    /// </summary>
    public partial class PhaseEditor : UserControl, ISliderReceiver, IPhaseAddedReceiver, ISkeletonReceiver, IDeleteMeReceiver, IMovementFinishedReceiver
    {
        private MovementPlayerManager manager;
        private MovementPhase phase;
        private bool hasChanged = false;
        private MovementSlider sliderInFocus;
        private MovementSlider mainSlider;
        private CommonMistakeEditor commonMistakeEditor;
        private bool hasFinished = false;

        public PhaseEditor()
        {
            InitializeComponent();
            skeletonViewer.SetColorToMentor();
        }

        private void Editor_Initialized(object sender, EventArgs e)
        {
            skeletonViewer.Unregister();
            EventManager.RegisterClassHandler(typeof(Window), Keyboard.KeyUpEvent, new KeyEventHandler(Key_Up), true);
        }

        private void Key_Up(object sender, KeyEventArgs e)
        {
            if (!sliderInFocus.Equals(mainSlider))
            {
                PhaseSliderConfig config = sliderInFocus.GetConfig() as PhaseSliderConfig;
                config.KeyPressed(e.Key);
            }
        }

        public void SetMovementPhase(MovementPhase phase)
        {
            this.phase = phase;
            phaseNameTextBox.Text = phase.Name;
            Movement singlePhaseMovment = LoadMainSlider(phase);
            manager = new MovementPlayerManager(singlePhaseMovment);
            manager.registerSkeletonReceiver(this);
            manager.SetSlider(slider);
            manager.SeekFrame(0);
            slider.SeekFrame(0);
            sliderInFocus = slider;
            mainSlider = slider;
            SliderChangeFocus_MouseUp(slider, null);
            slider.MouseDown += SliderChangeFocus_MouseUp;
            LoadMistakes();
            manager.registerMovementFinishedReceiver(this);
        }

        private Movement LoadMainSlider(MovementPhase phase)
        {
            slider.SetUp(this, new NormalSliderConfig());
            commonMistakePanel.Children.Clear();
            Movement singlePhaseMovment = new Movement();
            singlePhaseMovment.AddMovementPhase(phase);
            slider.SetMovement(singlePhaseMovment);
            skeletonViewer.Unregister();
            return singlePhaseMovment;
        }

        private void CreateCommonMistakeSlider(CommonMistake mistake)
        {
            CommonMistakeSlider mistakeSlider = new CommonMistakeSlider(this);
            EditorSliderConfig config = new PhaseSliderConfig(this.phase.GetMovementFrames().Count, mistake, mistakeSlider);
            if (mistakeSlider.ShouldAdd)
            {
                mistakeSlider.slider.SetUp(null, config);
                mistakeSlider.slider.MouseDown += SliderChangeFocus_MouseUp;
                commonMistakePanel.Children.Add(mistakeSlider);
                if (controlPanel.Children.Count == 0)
                {
                    SliderChangeFocus_MouseUp(mistakeSlider.slider, null);
                }
            }
        }

        private void LoadMistakes()
        {
            foreach (CommonMistake currentMistake in phase.GetCommonMistakes())
            {
                CreateCommonMistakeSlider(currentMistake);
            }
        }

        void SliderChangeFocus_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MovementSlider focusedSlider = sender as MovementSlider;
            controlPanel.Children.Clear();

            if (!focusedSlider.Equals(mainSlider))
            {
                PhaseSliderConfig focusConfig = focusedSlider.GetConfig() as PhaseSliderConfig;
                focusConfig.SetISkeletonReceiver(this);
                LoadCommonMistakeEditor(focusConfig.GetMistakeEditor());
                controlPanel.Children.Add(focusConfig.GetEditorButtonsPanel());
            }
            else
            {
                mainPanel.Children.Remove(commonMistakeEditor);
            }
            sliderInFocus.UnregisterSliderReceiver();
            SwapFocusOfClickedOnAndOldSlider(focusedSlider);
        }

        private void SwapFocusOfClickedOnAndOldSlider(MovementSlider focusedSlider)
        {
            sliderInFocus.UnregisterSliderReceiver();
            focusedSlider.RegisterSliderReceiver(this);
            sliderInFocus.BorderBrush = new SolidColorBrush(Colors.Silver);
            focusedSlider.BorderBrush = new SolidColorBrush(Colors.GreenYellow);
            focusedSlider.BorderThickness = new Thickness(3);
            sliderInFocus.UnregisterSliderReceiver();
            focusedSlider.RegisterSliderReceiver(this);
            sliderInFocus = focusedSlider;
            sliderInFocus.SeekFrame(0);
            manager.unregisterAllMovementFinishedReceivers();
            manager.unregisterAllSkeletonReceivers();
            manager.SetSlider(sliderInFocus);
            manager.registerMovementFinishedReceiver(this);
            manager.registerSkeletonReceiver(this);
        }

        private void LoadCommonMistakeEditor(CommonMistakeEditor mistakeEditor)
        {
            mainPanel.Children.Remove(commonMistakeEditor);
            commonMistakeEditor = mistakeEditor;
            mainPanel.Children.Add(commonMistakeEditor);
            commonMistakeEditor.IsEnabled = true;
        }

        public void UpdateFrameSeeked(int index)
        {
            manager.SeekFrame(index);
            if (hasFinished)
            {
                Replay();
                hasFinished = false;
                manager.PlayMovement();
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            playButton.Click += Play_Click;
            playButton.Click -= Pause_Click;
            playButton.Click -= Replay_Click;
            playButton.ToolTip = "Pauses the gold highlighted movement at the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/play.png", UriKind.Relative)) };
            manager.Restart();
            sliderInFocus.SeekFrame(0);
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            playButton.Click -= Play_Click;
            playButton.Click += Pause_Click;
            playButton.ToolTip = "Pauses the gold highlighted movement at the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/pause.png", UriKind.Relative)) };
            manager.PlayMovement();
        }

        private void Pause_Click(object sender, RoutedEventArgs e)
        {
            playButton.Click += Play_Click;
            playButton.Click -= Pause_Click;
            playButton.ToolTip = "Plays the gold highlighted movement from the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/play.png", UriKind.Relative)) };
            manager.PauseMovement();
        }

        private void Replay_Click(object sender, RoutedEventArgs e)
        {
            Replay();
            sliderInFocus.SeekFrame(0);
            manager.Replay();
        }

        private void Replay()
        {
            playButton.Click += Pause_Click;
            playButton.Click -= Replay_Click;
            hasFinished = false;
            playButton.ToolTip = "Pauses the gold highlighted movement at the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/pause.png", UriKind.Relative)) };
        }

        public void UpdateSkeleton(MySkeleton skeleton)
        {
            skeletonViewer.UpdateSkeleton(skeleton);
        }

        public void UpdateMovementEdited(Movement movement)
        {
            throw new NotImplementedException();
        }

        public bool GetHasChanged()
        {
            return hasChanged;
        }

        public void UpdatePhaseAddedReceiver(MovementPhase phase)
        {
            CommonMistake mistake = new CommonMistake("", phase, 0, phase.GetMovementFrames().Count);
            CreateCommonMistakeSlider(mistake);
            hasChanged = true;
        }

        public void Save()
        {
            phase.Name = phaseNameTextBox.Text;
            phase.GetCommonMistakes().Clear();
            foreach (CommonMistakeSlider slider in commonMistakePanel.Children)
            {
                ((PhaseSliderConfig)slider.slider.GetConfig()).GetMistakeEditor().Save();
                phase.AddCommonMistake(((PhaseSliderConfig)slider.slider.GetConfig()).GetCommonMistake());
            }
        }

        public void UpdateDeleteMeReceiver(UIElement element)
        {
            commonMistakePanel.Children.Remove(element);
            if (element == sliderInFocus)
            {
                SliderChangeFocus_MouseUp(mainSlider, null);
            }
        }

        private void phaseNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            phase.Name = phaseNameTextBox.Text;
        }

        public void updateMovementFinishedReceiver()
        {
            playButton.Click -= Play_Click;
            playButton.Click += Replay_Click;
            manager.PauseMovement();
            hasFinished = true;
            playButton.ToolTip = "Replays the gold highlighted movement from the beginning";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/replay.png", UriKind.Relative)) };
        }
    }
}
