﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    class MovementRecorder : ISkeletonReceiver
    {
        private MovementPhase recording;

        public void StartRecording()
        {
            recording = new MovementPhase();

            KinectSensorManager.registerSkeletonReceiver(this);
        }

        public MovementPhase StopRecording()
        {
            KinectSensorManager.unregisterSkeletonReceiver(this);
            return recording;
        }

        public void UpdateSkeleton(MySkeleton skeleton)
        {
            recording.AddMovementFrame(new MovementFrame(skeleton));
        }
    }
}
