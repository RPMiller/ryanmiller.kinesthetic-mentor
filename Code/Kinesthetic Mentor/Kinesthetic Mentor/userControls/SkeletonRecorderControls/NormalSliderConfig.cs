﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.Interfaces;
using System.Windows.Input;
using System.Windows;

namespace Kinesthetic_Mentor.userControls.SkeletonRecorderControls
{
    public class NormalSliderConfig : ISliderConfig
    {
        protected MovementSlider slider;
        protected bool isSliding = false;

        public void SetUpSlider(MovementSlider slider)
        {
            EventManager.RegisterClassHandler(typeof(Window), Mouse.MouseMoveEvent, new MouseEventHandler(Bar_Mouse_Move), true);
            this.slider = slider;
        }

        public void Mouse_LeftDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            isSliding = true;
            LeftDown(e);
        }

        private void Bar_Mouse_Move(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && isSliding)
            {
                LeftDown(e);
            }
            else
            {
                isSliding = false;
            }
        }

        private void LeftDown(MouseEventArgs e)
        {
            int currentFrame = slider.GetFrameFromMouse(e);
            slider.SeekFrame(currentFrame);
            UpdateSkeletonReceiver();
        }

        public void UpdateSkeletonReceiver()
        {
            slider.UpdateFrameSeekedReceiver(slider.GetCurrentFrame());
        }


        public bool GetIsSliding()
        {
            return isSliding;
        }
    }
}
