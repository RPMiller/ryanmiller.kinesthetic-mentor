﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.SkeletonLearner
{
    /// <summary>
    /// Interaction logic for CorrectnessBar.xaml
    /// </summary>
    public partial class CorrectnessBar : UserControl
    {
        private double data;
        public double Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
                double correctness = .2 - value;
                if (correctness < 0)
                {
                    correctness = 0;
                }
                progressBar.Value = correctness * 500;
                progressBar.Foreground = correctness >= .1 ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Red);
            }
        }

        public CorrectnessBar()
        {
            InitializeComponent();
        }
    }
}
