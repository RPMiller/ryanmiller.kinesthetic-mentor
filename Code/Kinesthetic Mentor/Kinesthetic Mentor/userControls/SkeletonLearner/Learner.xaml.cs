﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Utility;

namespace Kinesthetic_Mentor.userControls.SkeletonLearner
{
    /// <summary>
    /// Interaction logic for Learner.xaml
    /// </summary>
    public partial class Learner : UserControl, IMovementFinishedReceiver, IMovementSelectedReceiver, ILearnerMovementFinishedReceiver, IPhaseSelectedReceiver, ITryAgainReceiver
    {
        private LearnerMovementPlayer player;
        private LearnerPlayer learnerPlayer = new LearnerPlayer();
        private Movement loadedMovement;
        private LearnerNavigation mainContent = new LearnerNavigation();

        public Learner()
        {
            InitializeComponent();
            player = new LearnerMovementPlayer(this);
        }

        public void SetUpNavigation()
        {
            mainContent.Width = this.Width;
            mainContent.Height = this.Height;
        }

        void learnPhase_Click(object sender, RoutedEventArgs e)
        {
            PhaseSelector phaseSelector = new PhaseSelector();
            phaseSelector.SetUp(loadedMovement, this);
            phaseSelector.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            mainContent.NavigationProperty = phaseSelector;
        }

        void learnMovement_Click(object sender, RoutedEventArgs e)
        {
            learnerPlayer = new LearnerPlayer();
            learnerPlayer.SetUp(loadedMovement, this);
            mainContent.NavigationProperty = learnerPlayer;
        }

        void playIntroVideo_Click(object sender, RoutedEventArgs e)
        {
            mainContent.NavigationProperty = new VideoViewer(loadedMovement.IntroVideoLocation) { HorizontalAlignment = System.Windows.HorizontalAlignment.Center };
        }

        void selectNewMovement_Click(object sender, RoutedEventArgs e)
        {
            contentPanel.Children.Add(mainGrid);
            contentPanel.Children.Remove(mainContent);
        }

        public void updateMovementFinishedReceiver()
        {
            
        }

        public void UpdateMovementSelectedReceiver(String fileLocaction)
        {
            swapVisibility(mainContent, mainGrid);
            IMovementDAL fileIO = new FlatFileIO();
            loadedMovement = fileIO.ReadMovement(fileLocaction);
            if (loadedMovement.IntroVideoLocation == null)
            {
                mainContent.playIntroVideo.IsEnabled = false;
                mainContent.playIntroVideo.Background = new SolidColorBrush(Colors.Wheat);
            }
        }

        private void swapVisibility(UIElement elementToBeVisible, UIElement elementToBeHidden)
        {
            contentPanel.Children.Add(elementToBeVisible);
            contentPanel.Children.Remove(elementToBeHidden);
        }

        private void Learner_Loaded(object sender, RoutedEventArgs e)
        {
            movementSelector.SetUp(this);
            mainContent.playIntroVideo.Click += playIntroVideo_Click;
            mainContent.learnMovement.Click += learnMovement_Click;
            mainContent.learnPhase.Click += learnPhase_Click;
            mainContent.selectNewMovement.Click += selectNewMovement_Click;
        }

        public void UpdateLearnerMovementFinishedReceiver(Movement learner, Movement mentor)
        {
            contentPanel.Children.Remove(mainGrid);
            if (mainContent.backImage.Visibility == System.Windows.Visibility.Visible)
            {
                player.SetUp(learner, mentor);
                mainContent.NavigationProperty = player;
            }
        }

        public void UpdatePhaseSelectedReceiver(int phaseIndex)
        {
            Movement singlePhaseMovement = new Movement();
            singlePhaseMovement.AddMovementPhase(loadedMovement.GetMovementPhases()[phaseIndex]);
            singlePhaseMovement.PercentOfFramesCorrectNeeded = loadedMovement.PercentOfFramesCorrectNeeded;
            learnerPlayer = new LearnerPlayer();
            learnerPlayer.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            learnerPlayer.SetUp(singlePhaseMovement, this);
            mainContent.NavigationProperty = learnerPlayer;
        }

        public void UpdateTryAgainReceiver(Movement tryAgain)
        {
            learnerPlayer = new LearnerPlayer();
            learnerPlayer.SetUp(tryAgain, this);
            mainContent.NavigationProperty = learnerPlayer;
        }
    }
}
