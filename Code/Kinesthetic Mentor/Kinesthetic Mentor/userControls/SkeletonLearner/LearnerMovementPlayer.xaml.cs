﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models.Learner;
using System.Windows.Threading;

namespace Kinesthetic_Mentor.userControls.SkeletonLearner
{
    /// <summary>
    /// Interaction logic for LearnerMovementPlayer.xaml
    /// </summary>
    public partial class LearnerMovementPlayer : UserControl, IMovementFinishedReceiver, ISkeletonReceiver, ISliderReceiver, IPlayerActionReceiver
    {
        private LearnerMovement learnerMovement;
        private bool isSeek = false;
        private int currentPhaseIndex = 0;
        private Movement mentorMovement;
        private bool hasFinished = false;
        private ITryAgainReceiver tryAgainReceiver;

        public LearnerMovementPlayer(ITryAgainReceiver tryAgainReceiver)
        {
            InitializeComponent();
            playerPanel.SetPlayerReceiver(this);
            this.tryAgainReceiver = tryAgainReceiver;
            mentorPlayer.skeletonViewer.SetColorToMentor();
        }

        public void SetUp(Movement learner, Movement mentor)
        {
            learnerPlayer.SetUp(learner, this);
            mentorPlayer.SetUp(mentor, this);
            screen.Children.Remove(comparisonViewer);
            learnerMovement = new LearnerMovement(learner, mentor);
            playerPanel.slider.SetUp(this, new Kinesthetic_Mentor.userControls.SkeletonRecorderControls.NormalSliderConfig());
            playerPanel.slider.SetMovement(mentor);
            this.mentorMovement = mentor;
            currentPhaseIndex = 0;
            LoadMistakeDisplayer(0);
            comparisonViewer = new ComparisonDataViewer();
            screen.Children.Add(comparisonViewer);
            UpdateComparisonData(0);
            LoadFramesNeededData(0);
            mentorPlayer.GetMovementPlayerManager().registerSkeletonReceiver(this);
            playerPanel.Replay();
        }

        private void LoadFramesNeededData(int phaseIndex)
        {
            framesNeeededLabel.Content = learnerMovement.GetPhases()[phaseIndex].NumberOfFramesCorrect + " / " + (mentorMovement.PercentOfFramesCorrectNeeded / 100 * mentorMovement.GetMovementPhases()[phaseIndex].GetMovementFrames().Count);
        }

        private void LoadMistakeDisplayer(int index)
        {
            IList<CommonMistake> mistakesThatOccured = new List<CommonMistake>();
            LearnerPhase phase = learnerMovement.GetPhases()[index];
            for (int i = 0; i < phase.GetCommonMistakes().Count; i++)
            {
                if(phase.IsCommonMistakePhaseSuccessful(i))
                {
                    mistakesThatOccured.Add(mentorMovement.GetMovementPhases()[index].GetCommonMistakes()[i]);
                }
            }
            commonMistakeDisplayer.LoadMistakeData(mistakesThatOccured);
        }

        public void UpdateComparisonViewer(int index)
        {
            bool isSearching = true;
            int phaseStart = 0;
            IList<LearnerPhase> phases = learnerMovement.GetPhases();
            for (int phaseIndex = 0; phaseIndex < phases.Count && isSearching; phaseIndex++)
            {
                LearnerPhase currentPhase = phases[phaseIndex];
                int phaseLength = currentPhase.GetLearnerFrames().Count;
                if (index < phaseStart + phaseLength && index >= phaseStart)
                {
                    int frameIndex = index - phaseStart;
                    if(frameIndex < currentPhase.GetLearnerFrames().Count)
                    {
                        ComparisionData data = currentPhase.GetLearnerFrames()[frameIndex].GetComparison();
                        comparisonViewer.ShowComparisonData(data, currentPhase.Name, phaseIndex,currentPhase.IsPhaseSuccessful());
                        //comparisonViewer.ShowDataText(data, currentPhase.Name, phaseIndex, currentPhase.IsPhaseSuccessful());
                    }
                    LoadFramesNeededData(phaseIndex);
                    isSearching = false;
                }
                else
                {
                    phaseStart += phaseLength;
                }
            }
        }

        public void updateMovementFinishedReceiver()
        {
            
        }

        public void UpdateSkeleton(MySkeleton skeleton)
        {
            if (playerPanel.slider.IsAtEnd())
            {
                hasFinished = true;
                playerPanel.ChangeToReplay();
            }
            if (isSeek)
            {
                isSeek = false;
            }
            playerPanel.slider.SeekNextFrame();
            if (learnerMovement.GetPhaseIndexAtFrameIndex(playerPanel.slider.GetCurrentFrame()) != currentPhaseIndex)
            {
                currentPhaseIndex = playerPanel.slider.GetMovement().GetPhaseIndexAtFrameIndex(playerPanel.slider.GetCurrentFrame());
                LoadMistakeDisplayer(currentPhaseIndex);
            }
            UpdateComparisonViewer(playerPanel.slider.GetCurrentFrame());
        }

        public void UpdateFrameSeeked(int index)
        {
            learnerPlayer.GetMovementPlayerManager().SeekFrame(index);
            mentorPlayer.GetMovementPlayerManager().SeekFrame(index);
            if (hasFinished)
            {
                hasFinished = false;
                playerPanel.Replay();
                mentorPlayer.GetMovementPlayerManager().PlayMovement();
            }
            playerPanel.slider.SeekNextFrame();
            Dispatcher.Invoke(DispatcherPriority.Background, new Action<int>(UpdateComparisonData),index);
        }

        private void UpdateComparisonData(int index)
        {
            ComparisionData data = learnerMovement.GetFrameAtIndex(index).GetComparison();
            int phaseIndex = learnerMovement.GetPhaseIndexAtFrameIndex(index);
            LearnerPhase phase = learnerMovement.GetPhases()[phaseIndex];
            comparisonViewer.ShowComparisonData(data, phase.Name, phaseIndex, phase.IsPhaseSuccessful());
            //comparisonViewer.ShowDataText(data, phase.Name,phaseIndex, phase.IsPhaseSuccessful());
        }

        public void UpdateMovementEdited(Movement movement)
        {
            throw new NotImplementedException();
        }

        private void Learner_Loaded(object sender, RoutedEventArgs e)
        {
            KinectSensorManager.unregisterAllSkeletonReceivers();
        }

        public void PlayReceived()
        {
            learnerPlayer.GetMovementPlayerManager().PlayMovement();
            mentorPlayer.GetMovementPlayerManager().PlayMovement();
        }

        public void PauseReceived()
        {
            learnerPlayer.GetMovementPlayerManager().PauseMovement();
            mentorPlayer.GetMovementPlayerManager().PauseMovement();
        }

        public void StopReceived()
        {
            learnerPlayer.GetMovementPlayerManager().Restart();
            mentorPlayer.GetMovementPlayerManager().Restart();
        }

        public void ReplayReceived()
        {
            learnerPlayer.GetMovementPlayerManager().Replay();
            mentorPlayer.GetMovementPlayerManager().Replay();
        }

        private void TryAgain_Click(object sender, RoutedEventArgs e)
        {
            tryAgainReceiver.UpdateTryAgainReceiver(mentorPlayer.GetMovementPlayerManager().GetMovement());
        }
    }
}
