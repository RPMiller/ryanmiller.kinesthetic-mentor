﻿using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.SkeletonLearner
{
    /// <summary>
    /// Interaction logic for CommonMistakeDisplayer.xaml
    /// </summary>
    public partial class CommonMistakeDisplayer : UserControl
    {
        public CommonMistakeDisplayer()
        {
            InitializeComponent();
        }

        public void LoadMistakeData(IList<CommonMistake> mistakes)
        {
            mainPanel.Children.Clear();
            foreach (CommonMistake mistake in mistakes)
            {
                mainPanel.Children.Add(new SingleCommonMistakeDisplayer(mistake));
            }
        }
    }
}
