﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Models.Learner;
using Microsoft.Kinect;

namespace Kinesthetic_Mentor.userControls.SkeletonLearner
{
    /// <summary>
    /// Interaction logic for ComparisonDataViewer.xaml
    /// </summary>
    public partial class ComparisonDataViewer : UserControl
    {
        public ComparisonDataViewer()
        {
            InitializeComponent();
            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                LoadDataText(jointType.ToString(), 0, (int)jointType + 1);
                LoadCorrectnessBar(1, (int)jointType + 1);
            }
        }

        private void LoadCorrectnessBar(int col,int row)
        {
            CorrectnessBar bar = new CorrectnessBar();
            Grid.SetColumn(bar,col);
            Grid.SetRow(bar,row);
            mainGrid.Children.Add(bar);
        }

        public void ShowComparisonData(ComparisionData data, String PhaseName, int phaseIndex, bool isSuccess)
        {
            LoadPhaseData(PhaseName, phaseIndex, isSuccess);
            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                LoadDataBar(data[jointType].Data, 1, (int)jointType + 1);
            }
        }

        private void LoadPhaseData(String PhaseName, int phaseIndex, bool isSuccess)
        {
            phaseLabel.Content = PhaseName + " (Phase " + (phaseIndex + 1) + ")";
            if (isSuccess)
            {
                phaseLabel.Content += ": Successful";
                phaseLabel.Foreground = new SolidColorBrush(Colors.Green);
            }
            else
            {
                phaseLabel.Content += ": Unsuccessful";
                phaseLabel.Foreground = new SolidColorBrush(Colors.Red);
            }
        }

        public void ShowDataText(ComparisionData data, String PhaseName, int phaseIndex, bool isSuccess)
        {
            LoadPhaseData(PhaseName, phaseIndex, isSuccess);
            LoadBaseLabels();
            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                LoadDataText(jointType.ToString(), 0, (int)jointType + 1);
                LoadDataText(data[jointType].Data.ToString(), 1, (int)jointType + 1, data[jointType].IsCorrect);
            }
        }

        private void LoadBaseLabels()
        {
            mainGrid.Children.Clear();
            LoadDataText("Joint Type", 0, 0);
            LoadDataText("Error Data", 1, 0);
        }

        private void LoadDataText(String data, int col, int row, bool isCorrect = true)
        {
            double parseData = 0;
            Label label = new Label();
            label.Visibility = System.Windows.Visibility.Visible;
            label.Content = data;
            SolidColorBrush dataColor;
            label.FontSize = 10;
            if (double.TryParse(data, out parseData))
            {
                if (isCorrect)
                {
                    dataColor = new SolidColorBrush(Colors.Green);
                }
                else
                {
                    dataColor = new SolidColorBrush(Colors.Red);
                }
            }
            else
            {
                dataColor = new SolidColorBrush(Colors.White);
            }
            label.Foreground = dataColor;
            mainGrid.Children.Add(label);
            Grid.SetColumn(label, col);
            Grid.SetRow(label, row);
        }

        private void LoadDataBar(double data, int col, int row)
        {
            int cell = row * 2 + col;
            ((CorrectnessBar)mainGrid.Children[cell]).Data = data;
        }
    }
}
