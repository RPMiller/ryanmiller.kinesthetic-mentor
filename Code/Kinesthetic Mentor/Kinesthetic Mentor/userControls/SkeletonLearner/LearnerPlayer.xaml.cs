﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;
using System.Threading;
using System.Windows.Threading;

namespace Kinesthetic_Mentor.userControls.SkeletonLearner
{
    /// <summary>
    /// Interaction logic for LearnerPlayer.xaml
    /// </summary>
    public partial class LearnerPlayer : UserControl, ICountDownFinishedReceiver, ISkeletonReceiver,IMovementFinishedReceiver
    {
        private ILearnerMovementFinishedReceiver receiver;
        private MovementPlayerManager manager;
        private MovementRecorder recorder;
        private int currentFrame = 0;
        private bool isRunning = false;

        public LearnerPlayer()
        {
            InitializeComponent();
            skeletonViewer.RegisterCountDownFinishedReceiver(this);
            skeletonViewer.Unregister();
            skeletonViewer.SetColorToMentor();
        }

        public void SetUp(Movement movement, ILearnerMovementFinishedReceiver receiver,int countDown = 3)
        {
            this.receiver = receiver;
            manager = new MovementPlayerManager(movement);
            manager.registerSkeletonReceiver(skeletonViewer);
            skeletonViewer.StartCountDown(countDown);
            learnerSkeletonViewer.StartCountDown(countDown);
            recorder = new MovementRecorder();
            manager.SeekFrame(0);
            manager.registerMovementFinishedReceiver(this);
            framesNeeededLabel.Content = movement.PercentOfFramesCorrectNeeded;
        }

        public void Replay()
        {
            currentFrame = 0;
            isRunning = true;
            recorder.StartRecording();
        }

        public void updateCountDownFinishedReceiver()
        {
            Replay();
            KinectSensorManager.registerSkeletonReceiver(this);
        }

        private void LearnerPlayer_Loaded(object sender, RoutedEventArgs e)
        {
            KinectSensorManager.unregisterAllSkeletonReceivers();
            learnerSkeletonViewer.Register();
        }

        public void UpdateSkeleton(MySkeleton skeleton)
        {
            if (isRunning)
            {
                manager.SeekFrame(currentFrame);
                currentFrame++;
            }
        }

        public void updateMovementFinishedReceiver()
        {
            isRunning = false;
            Movement learnerMovement = new Movement();
            learnerMovement.AddMovementPhase(recorder.StopRecording());
            receiver.updateMovementFinishedReceiver();
            receiver.UpdateLearnerMovementFinishedReceiver(learnerMovement, manager.GetMovement());
        }
    }
}
