﻿using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.SkeletonLearner
{
    /// <summary>
    /// Interaction logic for SingleCommonMistakeDisplayer.xaml
    /// </summary>
    public partial class SingleCommonMistakeDisplayer : UserControl
    {
        public SingleCommonMistakeDisplayer(CommonMistake mistake)
        {
            InitializeComponent();
            messageLabel.Text = mistake.Message;
            nameLabel.Content = mistake.MistakePhase.Name;
            if (mistake.MistakeVideoLocation != null)
            {
                mainScreen.Children.Add(new VideoViewer(mistake.MistakeVideoLocation) { Width = 250 });
            }
        }
    }
}
