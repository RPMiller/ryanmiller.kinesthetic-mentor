﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls.Help
{
    /// <summary>
    /// Interaction logic for HelpHome.xaml
    /// </summary>
    public partial class HelpHome : UserControl
    {
        public HelpHome()
        {
            InitializeComponent();
            helpNavigation.glossaryButton.Click += glossaryButton_Click;
            helpNavigation.tutorialButton.Click += tutorialButton_Click;
        }

        void tutorialButton_Click(object sender, RoutedEventArgs e)
        {
            helpNavigation.NavigationProperty = null;
        }

        void glossaryButton_Click(object sender, RoutedEventArgs e)
        {
            helpNavigation.NavigationProperty = new Glossary();
        }
    }
}
