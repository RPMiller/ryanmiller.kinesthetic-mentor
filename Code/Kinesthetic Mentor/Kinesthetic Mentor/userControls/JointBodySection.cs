﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace Kinesthetic_Mentor.userControls
{
    class JointBodySection
    {
        private IList<JointConnection> connections = new List<JointConnection>();
        private JointType initailJointType;
        private JointType endingJointType;


        public JointBodySection(IList<JointConnection> connections, JointType initailJointType, JointType endingJointType)
        {
            this.connections = connections;
            this.initailJointType = initailJointType;
            this.endingJointType = endingJointType;
        }

        public IList<JointConnection> GetConnections()
        {
            return connections;
        }

        public JointConnection GetOuterJointConnection(JointType jointType)
        {
            JointConnection connection = null;
            if (jointType != initailJointType || jointType != endingJointType)
            {
                bool isSearchingForJoint = true;
                JointType start = JointType.Head;
                JointType end = JointType.Head;

                for (int i = 0; i < connections.Count() && isSearchingForJoint; i++)
                {
                    isSearchingForJoint = !connections[i].GetEndJoint().Equals(jointType);
                    if (!isSearchingForJoint)
                    {
                        start = connections[i].GetStartJoint();
                        end = connections[i].GetEndJoint();
                    }
                }
                if (!isSearchingForJoint)
                {
                    connection = new JointConnection(start, end);
                }
            }

            return connection;
        }
    }
}
