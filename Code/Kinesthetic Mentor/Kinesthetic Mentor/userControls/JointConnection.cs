﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace Kinesthetic_Mentor.userControls
{
    class JointConnection
    {
        private JointType startJoint;
        private JointType endJoint;
        public float JointConnectionLength { get; set; }

        public JointConnection(JointType start, JointType end)
        {
            startJoint = start;
            endJoint = end;
        }

        public JointType GetStartJoint()
        {
            return startJoint;
        }

        public JointType GetEndJoint()
        {
            return endJoint;
        }
        
        public override bool Equals(object obj)
        {
            bool isEqual = false;

            if (obj.GetType().Equals(this.GetType()))
            {
                JointConnection connection = (JointConnection)obj;
                isEqual = connection.GetStartJoint().Equals(startJoint) && connection.GetEndJoint().Equals(endJoint);
            }

            return isEqual;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
