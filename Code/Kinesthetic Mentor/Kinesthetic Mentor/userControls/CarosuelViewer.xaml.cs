﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;

namespace Kinesthetic_Mentor.userControls
{
    /// <summary>
    /// Interaction logic for CarosuelViewer.xaml
    /// </summary>
    public partial class CarouselViewer : UserControl
    {
        private double currentElementNum = 0;
        private Dictionary<UIElement, KeyValuePair<MeshGeometry3D, int>> elementDictionary = new Dictionary<UIElement, KeyValuePair<MeshGeometry3D, int>>();
        private bool isRotating = false;
        private Viewport3D viewport = new Viewport3D();
        private Material material;
        private IElementFocusChangedReceiver receiver;


        public CarouselViewer(List<UIElement> elements,IElementFocusChangedReceiver receiver = null)
        {
            InitializeComponent();
            viewport.Camera = new PerspectiveCamera(new Point3D(0, 0, 2.2), new Vector3D(0, 0, -1), new Vector3D(0, 1, 0), 90);
            viewport.Children.Add(new ModelVisual3D {Content = new DirectionalLight {Color = Colors.White,  Direction = new Vector3D(0, 0, -1)}}) ;
            material = new DiffuseMaterial { Brush = Brushes.White };
            Viewport2DVisual3D.SetIsVisualHostMaterial(material, true);
            this.receiver = receiver;
            BuildCarouselViewer(elements);
            screen.Children.Add(viewport);
        }

        public void SetSizeBasedOnElements()
        {
            double maxHeight = 0;
            double maxWidth = 0;
            foreach (UIElement element in elementDictionary.Keys)
            {
                if (element.DesiredSize.Height > maxHeight)
                {
                    maxHeight = element.DesiredSize.Height;
                }
                if (element.DesiredSize.Width > maxWidth)
                {
                    maxWidth = element.DesiredSize.Width;
                }
            }
            this.Height = maxHeight + 50;
            this.Width = maxWidth + 100;
        }

        public bool IsRotating()
        {
            return isRotating;
        }

        private void BuildCarouselViewer(List<UIElement> elements)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                double rads = ((360 - ((360 / elements.Count) * i)) * (Math.PI / 180));
                UIElement element = elements[i];
                Viewport2DVisual3D visual = new Viewport2DVisual3D();
                MeshGeometry3D mesh = new MeshGeometry3D();
                Generate3DPoints(mesh, rads);
                mesh.TriangleIndices = new Int32Collection(new int[] { 0, 1, 2, 0, 2, 3 });
                mesh.TextureCoordinates = new PointCollection(new Point[] { new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 0) });
                visual.Geometry = mesh;
                visual.Visual = element;
                visual.Material = material;
                viewport.Children.Add(visual);
                elementDictionary.Add(element, new KeyValuePair<MeshGeometry3D, int>(mesh, i));
                element.PreviewMouseDown += element_PreviewMouseButtonDown;
            }
        }

        void element_PreviewMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            RotateToElement(sender as UIElement);
        }

        public void RotateToElement(UIElement element)
        {
            int nextValue = elementDictionary[element].Value;
            if (nextValue != currentElementNum && !isRotating)
            {
                isRotating = true;
                int numberOfElementsOverToRotate = (int)Math.Abs(nextValue - currentElementNum);
                int direction = -1;
                if (ShouldRotateLeft(numberOfElementsOverToRotate))
                {
                    direction = 1;
                }
                Thread t = new Thread(new ThreadStart(new Action(delegate() { Rotate(numberOfElementsOverToRotate, direction); })));
                t.Start();
            }
            if (receiver != null)
            {
                receiver.UpdateElementFocusChangedReceiver(element);
            }
        }

        private void Rotate(int numberOfElementsOverToRotate, int direction)
        {
            double radsToRotate = ConvertToRads(numberOfElementsOverToRotate);
            for (double i = 0; i <= radsToRotate; i+=.02)
            {
                foreach (KeyValuePair<UIElement, KeyValuePair<MeshGeometry3D, int>> keyValue in elementDictionary)
                {
                    MeshGeometry3D mesh = keyValue.Value.Key;
                    Generate3DPoints(mesh, ConvertToRads(keyValue.Value.Value) + i * direction);
                }
                Thread.Sleep(1);
            }
            RotateIndicies(numberOfElementsOverToRotate, direction);
            isRotating = false;
        }

        private void RotateIndicies(int numberOfElementsOverToRotate, int direction)
        {
            Dictionary<UIElement, KeyValuePair<MeshGeometry3D, int>> temp = new Dictionary<UIElement, KeyValuePair<MeshGeometry3D, int>>();
            foreach (KeyValuePair<UIElement, KeyValuePair<MeshGeometry3D, int>> keyValue in elementDictionary)
            {
                int nextNumber = keyValue.Value.Value + numberOfElementsOverToRotate * direction;
                if (nextNumber >= elementDictionary.Count)
                {
                    nextNumber -= elementDictionary.Count;
                }
                if (nextNumber < 0)
                {
                    nextNumber += elementDictionary.Count;
                }
                temp[keyValue.Key] = new KeyValuePair<MeshGeometry3D, int>(keyValue.Value.Key, nextNumber);
            }
            elementDictionary = temp;
        }

        private double ConvertToRads(double posistion)
        {
            if (posistion < 0)
            {
                posistion -= 1;
            }
            return ((360 - ((360 / elementDictionary.Count) * (posistion))) * (Math.PI / 180));
        }

        private bool ShouldRotateLeft(int numberOfElementsOverToRotate)
        {
            return numberOfElementsOverToRotate < elementDictionary.Count / 2;
        }

        private void Generate3DPoints(MeshGeometry3D mesh, double rads)
        {
            double RIGHT = 1 - Math.Sin(rads);
            double LEFT = RIGHT - 2;
            double TOP = 2.8 - (Math.Cos(rads) * 1.5);
            double BOTTOM = 0 - (Math.Cos(rads) * 1.5);
            double Z = Math.Cos(rads);
            Point3D topLeft = new Point3D(LEFT, TOP, Z);
            Point3D topRight = new Point3D(RIGHT, TOP, Z);
            Point3D bottomLeft = new Point3D(LEFT, BOTTOM, Z);
            Point3D bottomRight = new Point3D(RIGHT, BOTTOM, Z);
            Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new Action(delegate() { mesh.Positions = new Point3DCollection { topLeft, bottomLeft, bottomRight, topRight }; }));
        }
    }
}
