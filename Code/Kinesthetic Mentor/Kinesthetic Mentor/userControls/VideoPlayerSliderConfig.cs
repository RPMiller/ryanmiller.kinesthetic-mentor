﻿using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.userControls
{
    public class VideoPlayerSliderConfig : NormalSliderConfig
    {
        public void SetTotalFrames(int totalFrames)
        {
            slider.TotalFrames = totalFrames;
        }
    }
}
