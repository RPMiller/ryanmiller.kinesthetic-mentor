﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using HelixToolkit.Wpf;
using System.Windows.Media.Media3D;
using Kinesthetic_Mentor.Utility;

namespace Kinesthetic_Mentor.userControls
{
    /// <summary>
    /// Interaction logic for SkeletonViewer.xaml
    /// </summary>
    public partial class KinectSkeletonViewer : UserControl , ISkeletonReceiver
    {
        private JointMapping jointMap = new JointMapping();
        //private ShaderProgram shaderProgram;
        //private VBO<Vector3> triangle, square;
        //private VBO<int> triangleElements, squareElements;
        private DiffuseMaterial frontMaterial = new DiffuseMaterial(Brushes.White);
        private DiffuseMaterial backMaterial = new DiffuseMaterial(Brushes.Red);
        private const float radiusSize = .1f;
        private HelixViewport3D viewport = new HelixViewport3D();
        private const int maxPos3D = 5;
        private bool isTriangleMan;

        public KinectSkeletonViewer()
        {
            InitializeComponent();
            Build3DHelix();
            isTriangleMan = IsTriangleManConfigIO.ReadIsTriangleMan();
        }

        public void SetColorToMentor()
        {
            frontMaterial = new DiffuseMaterial(Brushes.Yellow);
        }

        public void UpdateSkeleton(Kinesthetic_Mentor.Models.MySkeleton skeleton)
        {
            skeletonViewer.Children.Clear();
            viewport.Children.Clear();
            viewport.Children.Add(new DefaultLights());
            MeshBuilder builder = new MeshBuilder();
            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                Draw3DEllipsiods(skeleton.Joints[jointType],builder);
            }
            foreach (JointConnection connection in JointMapping.GetConnections())
            {
                MyJoint start = skeleton.Joints[connection.GetStartJoint()];
                MyJoint end = skeleton.Joints[connection.GetEndJoint()];
                //DrawJointEllipse(start);
                //DrawJointEllipse(end);
                //DrawConnection(start, end);
                if (start.TrackingState == JointTrackingState.Tracked && end.TrackingState == JointTrackingState.Tracked)
                {
                    Draw3DBones(start, end, builder);
                }
            }


            if (isTriangleMan)
            {
                DrawSkeletonTrianglesForCos(builder, skeleton);
            }

            GeometryModel3D geomod = new GeometryModel3D();
            geomod.Material = frontMaterial;
            geomod.BackMaterial = backMaterial;
            geomod.Geometry = builder.ToMesh();
            ModelVisual3D sphere = new ModelVisual3D();
            sphere.Content = geomod;
            viewport.Children.Add(sphere);
        }

        private void DrawSkeletonTrianglesForCos(MeshBuilder builder,MySkeleton skeleton)
        {
            foreach(JointBodySection section in JointMapping.GetBodySections())
            {
                IList<JointConnection> connections = section.GetConnections();
                JointConnection previousConnection = connections[0];
                for (int i = 1; i < connections.Count; i++)
                {
                    JointConnection next = connections[i];
                    Draw3DBones(skeleton.Joints[previousConnection.GetStartJoint()], skeleton.Joints[previousConnection.GetEndJoint()], builder);
                    Draw3DBones(skeleton.Joints[previousConnection.GetEndJoint()], skeleton.Joints[next.GetEndJoint()], builder);
                    Draw3DBones(skeleton.Joints[next.GetEndJoint()], skeleton.Joints[previousConnection.GetStartJoint()], builder);
                    previousConnection = next;
                }
            }
        }

        private void Draw3DEllipsiods(MyJoint joint,MeshBuilder builder)
        {
            builder.AddEllipsoid(new Point3D(TranslateTo3DX(joint.Position.X), TranslateTo3DY(joint.Position.Y), TranslateTo3DZ(joint.Position.Z)), radiusSize, radiusSize, radiusSize);
        }

        private void Draw3DBones(MyJoint start,MyJoint end, MeshBuilder builder)
        {
            builder.AddCylinder(new Point3D(TranslateTo3DX(start.Position.X), TranslateTo3DY(start.Position.Y), TranslateTo3DZ(start.Position.Z)),
                new Point3D(TranslateTo3DX(end.Position.X), TranslateTo3DY(end.Position.Y), TranslateTo3DZ(end.Position.Z)), .1, 10);
        }

        private double TranslateTo3DX(double input)
        {
            double output = input * maxPos3D;
            if (output > 5)
            {
                output = 5;
            }
            else if (output < -5)
            {
                output = -5;
            }
            //mirrors the output
            return output * -1;
        }

        private double TranslateTo3DY(double input)
        {
            double output = input * maxPos3D;
            if (output > 5)
            {
                output = 5;
            }
            else if (output < -5)
            {
                output = -5;
            }
            return output;
        }

        private double TranslateTo3DZ(double input)
        {
            double output = (input / 1.3) * maxPos3D;
            return output - 7;
        }

        //private void OnDisplay() { }

        //private void OnRenderFrame()
        //{
        //    Gl.Viewport(0, 0, (int)skeletonViewer.Width, (int)skeletonViewer.Height);
        //    Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        //    Glut.glutMainLoop();

        //    shaderProgram.Use();
        //    shaderProgram["model_matrix"].SetValue(OpenGL.Matrix4.CreateTranslation(new Vector3(-1.5f,0,0)));
        //    uint vertexPositionIndex = (uint)Gl.GetAttribLocation(shaderProgram.ProgramID, "vertexPositions");
        //    Gl.EnableVertexAttribArray(vertexPositionIndex);
        //    Gl.BindBuffer(triangle);
        //    Gl.VertexAttribPointer(vertexPositionIndex,triangle.Size,triangle.PointerType,true,12,IntPtr.Zero);
        //    Gl.BindBuffer(triangleElements);
        //    Gl.DrawElements(BeginMode.Triangles, triangleElements.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);

        //    shaderProgram["model_matrix"].SetValue(OpenGL.Matrix4.CreateTranslation(new Vector3(1.5f, 0, 0)));
        //    Gl.BindBufferToShaderAttribute(square, shaderProgram, "vertexPosition");
        //    Gl.BindBuffer(squareElements);
        //    Gl.DrawElements(BeginMode.Quads, triangleElements.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);
            
        //    Glut.glutSwapBuffers();
        //}

//        public static String VertexShader = @"
//in vec3 vertexPosition;
//uniform mat4 projection_matrix;
//uniform mat4 view_matrix;
//uniform mat4 model_matrix;
//
//void main(void)
//{
//    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition,1);
//}
//";

//        public static String FragmentShader = @"
//void main(void)
//{
//    gl_FragColor = vec4(1,0,0,1);
//}
//";

//        private void Build3DGL()
//        {
//            Glut.glutInit();
//            Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
//            Glut.glutInitWindowSize((int)skeletonViewer.Width, (int)skeletonViewer.Height);
//            Glut.glutCreateWindow("Skeleton Viewer");
//            Glut.glutIdleFunc(OnRenderFrame);
//            Glut.glutDisplayFunc(OnDisplay);
//            shaderProgram = new ShaderProgram(VertexShader, FragmentShader);
//            shaderProgram.Use();
//            shaderProgram["projection_matrix"].SetValue(OpenGL.Matrix4.CreatePerspectiveFieldOfView(0.45f,(float)((float)skeletonViewer.Width / skeletonViewer.Height),0.1f,1000f));
//            shaderProgram["view_matrix"].SetValue(OpenGL.Matrix4.LookAt(new Vector3(0,0,10),Vector3.Zero,Vector3.Up));
//            triangle = new VBO<Vector3>(new Vector3[] { new Vector3(1, 0, 1), new Vector3(-1, -1, 0), new Vector3(1, -1, 0) });
//            square = new VBO<Vector3>(new Vector3[]{new Vector3(-1,1,0),new Vector3(1,1,0),new Vector3(1,-1,0),new Vector3(-1,-1,0)});
//            triangleElements = new VBO<int>(new int[] { 0, 1, 2 }, BufferTarget.ElementArrayBuffer);
//            squareElements = new VBO<int>(new int[] { 0, 1, 2, 3 }, BufferTarget.ElementArrayBuffer);
//        }

        private void Build3DHelix()
        {
            viewport.ClipToBounds = false;
            viewport.IsHitTestVisible = false;
            viewport.DefaultCamera = new PerspectiveCamera(new Point3D(0,0,-5),new Vector3D(0,0,5),new Vector3D (0,1,0),90);
            mainGrid.Children.Add(viewport);
            viewport.IsHeadLightEnabled = true;
        }

        private Geometry3D LoadFromFile(string objPath)
        {
            ModelImporter importer = new ModelImporter();
            Model3DGroup group = importer.Load(objPath);
            MeshBuilder TestMesh = new MeshBuilder(false, false);


            foreach (var m in group.Children)
            {
                var mGeo = m as GeometryModel3D;
                var mesh = (MeshGeometry3D)((Geometry3D)mGeo.Geometry);
                if (mesh != null) TestMesh.Append(mesh);
            }
            return TestMesh.ToMesh();

        }

        private void DrawJointEllipse(MyJoint joint)
        {
            int ellipseDiameter = (int)skeletonViewer.Height / 60;
            Shape ellipse = new Ellipse();
            ellipse.Height = ellipseDiameter;
            ellipse.Width = ellipseDiameter;
            Canvas.SetTop(ellipse, TranslatePointToPixelTop(joint.Position.Y));
            Canvas.SetLeft(ellipse, TranslatePointToPixelLeft(joint.Position.X));
            ellipse.Fill = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            skeletonViewer.Children.Add(ellipse);
        }

        private void DrawConnection(MyJoint start, MyJoint end)
        {
            Line line = new Line();
            line.StrokeThickness = 3;
            line.X1 = TranslatePointToPixelLeft(start.Position.X);
            line.X2 = TranslatePointToPixelLeft(end.Position.X);
            line.Y1 = TranslatePointToPixelTop(start.Position.Y);
            line.Y2 = TranslatePointToPixelTop(end.Position.Y);
            line.Stroke = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            skeletonViewer.Children.Add(line);
        }

        private double TranslatePointToPixelLeft(float point)
        {
            double translation = (((point + 1) / 2) * skeletonViewer.ActualWidth);
            if (translation > skeletonViewer.ActualWidth)
            {
                translation = skeletonViewer.ActualWidth;
            }
            else if (translation < 0)
            {
                translation = 0;
            }
            return translation;
        }

        private double TranslatePointToPixelTop(float point)
        {
            double translation = skeletonViewer.Height -(((point + 1) / 2) * skeletonViewer.Height);
            if (translation > skeletonViewer.ActualHeight)
            {
                translation = skeletonViewer.ActualHeight;
            }
            else if (translation < 0)
            {
                translation = 0;
            }
            return translation;
        }


        public void Unregister()
        {
            KinectSensorManager.unregisterSkeletonReceiver(this);
        }

        public void Register()
        {
            KinectSensorManager.registerSkeletonReceiver(this);
        }

        private void Viewer_Initialized(object sender, EventArgs e)
        {
            KinectSensorManager.registerSkeletonReceiver(this);
        }

        public void StartCountDown(int seconds)
        {
            countDown.StartCountDown(seconds);
        }

        public void RegisterCountDownFinishedReceiver(ICountDownFinishedReceiver receiver)
        {
            countDown.RegisterCountDownFinishedReceiver(receiver);
        }
    }
}
