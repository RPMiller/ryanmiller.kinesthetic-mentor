﻿using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kinesthetic_Mentor.userControls
{
    /// <summary>
    /// Interaction logic for PhaseSelector.xaml
    /// </summary>
    public partial class PhaseSelector : UserControl
    {
        IPhaseSelectedReceiver receiver;

        public PhaseSelector()
        {
            InitializeComponent();
        }

        public void SetUp(Movement movement, IPhaseSelectedReceiver receiver)
        {
            this.Visibility = System.Windows.Visibility.Visible;
            phaseSelectorPanel.Children.Clear();
            this.receiver = receiver;
            int phaseNumber = 1;
            foreach (MovementPhase phase in movement.GetMovementPhases())
            {
                CreateFileButton(phase.Name, phaseNumber);
                phaseNumber++;
            }
        }

        private void CreateFileButton(String phaseName,int phaseNumber)
        {
            if (String.IsNullOrEmpty(phaseName))
            {
                phaseName = "Nameless Phase";
            }
            phaseName = phaseNumber + ": " + phaseName;
            Button button = new Button();
            button.Content = phaseName;
            button.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
            button.Click += FileButton_Click;
            phaseSelectorPanel.Children.Add(button);
        }

        void FileButton_Click(object sender, RoutedEventArgs e)
        {
            int phaseNumber = 0;
            int.TryParse(((Button)sender).Content.ToString().Substring(0, 1), out phaseNumber);
            receiver.UpdatePhaseSelectedReceiver(phaseNumber - 1);
        }

        private void ExitButtonClick(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
