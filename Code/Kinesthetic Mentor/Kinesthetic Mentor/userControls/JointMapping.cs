﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace Kinesthetic_Mentor.userControls
{
    class JointMapping
    {
        private static IList<JointBodySection> sections = new List<JointBodySection>();
        private static IList<JointConnection> jointConnections = new List<JointConnection>();

        public static void SetUp()
        {
            MapCenterCore();
            MapLeftUpperCore();
            MapLeftLowerCore();
            MapRightUpperCore();
            MapRightLowerCore();
            MapLeftArm();
            MapRightArm();
            MapLeftLeg();
            MapRightLeg();
            MapBodySections();
        }

        private static void MapBodySections()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.Head, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.ShoulderRight));
            sections.Add(new JointBodySection(connections, JointType.Head, JointType.ShoulderRight));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.Head, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.ShoulderLeft));
            sections.Add(new JointBodySection(connections, JointType.Head, JointType.ShoulderLeft));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.WristRight, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.WristRight, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HandRight, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.HandRight, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.WristLeft, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.WristLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HandLeft, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.HandLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.AnkleLeft, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.AnkleLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.FootLeft, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.FootLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.AnkleRight, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.AnkleRight, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.FootRight, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.FootRight, JointType.Spine));



            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.WristRight, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.WristRight, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HandRight, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.HandRight, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.WristLeft, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.WristLeft, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HandLeft, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.HandLeft, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.AnkleLeft, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.AnkleLeft, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.FootLeft, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.FootLeft, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.AnkleRight, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.AnkleRight, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.FootRight, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.FootRight, JointType.Head));




            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ElbowLeft, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.ElbowLeft, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ElbowRight, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.ElbowRight, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ElbowLeft, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.ElbowLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ElbowRight, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.ElbowRight, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ShoulderLeft, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.ShoulderLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ShoulderRight, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.ShoulderRight, JointType.Spine));




            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.KneeLeft, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.KneeLeft, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.KneeRight, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Head));
            sections.Add(new JointBodySection(connections, JointType.KneeRight, JointType.Head));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.KneeLeft, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.KneeLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.KneeRight, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.KneeRight, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HipLeft, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.HipLeft, JointType.Spine));

            connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HipRight, JointType.HipCenter));
            connections.Add(new JointConnection(JointType.HipCenter, JointType.Spine));
            sections.Add(new JointBodySection(connections, JointType.HipRight, JointType.Spine));
        }

        public static IList<JointBodySection> GetBodySections()
        {
            return sections;
        }

        private static void MapCenterCore()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.Head, JointType.ShoulderCenter));
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.Spine));
            connections.Add(new JointConnection(JointType.Spine, JointType.HipCenter));
            sections.Add(new JointBodySection(connections, JointType.Head, JointType.HipCenter));
            jointConnections = jointConnections.Concat(connections).ToList();
        }

        private static void MapLeftUpperCore()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.ShoulderLeft));
            connections.Add(new JointConnection(JointType.ShoulderLeft, JointType.ElbowLeft));
            sections.Add(new JointBodySection(connections, JointType.ShoulderCenter, JointType.ElbowLeft));
            jointConnections = jointConnections.Concat(connections).ToList();
        }

        private static void MapRightUpperCore()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ShoulderCenter, JointType.ShoulderRight));
            connections.Add(new JointConnection(JointType.ShoulderRight, JointType.ElbowRight));
            sections.Add(new JointBodySection(connections, JointType.ShoulderCenter, JointType.ElbowRight));
            jointConnections = jointConnections.Concat(connections).ToList();
        }
        private static void MapLeftLowerCore()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HipCenter, JointType.HipLeft));
            connections.Add(new JointConnection(JointType.HipLeft, JointType.KneeLeft));
            sections.Add(new JointBodySection(connections, JointType.HipCenter, JointType.KneeLeft));
            jointConnections = jointConnections.Concat(connections).ToList();
        }

        private static void MapRightLowerCore()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HipCenter, JointType.HipRight));
            connections.Add(new JointConnection(JointType.HipRight, JointType.KneeRight));
            sections.Add(new JointBodySection(connections, JointType.HipCenter, JointType.KneeRight));
            jointConnections = jointConnections.Concat(connections).ToList();
        }


        private static void MapLeftArm()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ShoulderLeft,JointType.ElbowLeft));
            connections.Add(new JointConnection(JointType.ElbowLeft, JointType.WristLeft));
            connections.Add(new JointConnection(JointType.WristLeft, JointType.HandLeft));
            //sections.Add(new JointBodySection(connections, JointType.ShoulderLeft, JointType.HandLeft));
            jointConnections = jointConnections.Concat(connections).ToList();
        }

        private static void MapRightArm()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.ShoulderRight, JointType.ElbowRight));
            connections.Add(new JointConnection(JointType.ElbowRight, JointType.WristRight));
            connections.Add(new JointConnection(JointType.WristRight, JointType.HandRight));
            //sections.Add(new JointBodySection(connections, JointType.ShoulderRight, JointType.HandRight));
            jointConnections = jointConnections.Concat(connections).ToList();
        }

        private static void MapLeftLeg()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HipLeft, JointType.KneeLeft));
            connections.Add(new JointConnection(JointType.KneeLeft, JointType.AnkleLeft));
            connections.Add(new JointConnection(JointType.AnkleLeft, JointType.FootLeft));
            //sections.Add(new JointBodySection(connections, JointType.HipLeft, JointType.FootLeft));
            jointConnections = jointConnections.Concat(connections).ToList();
        }

        private static void MapRightLeg()
        {
            IList<JointConnection> connections = new List<JointConnection>();
            connections.Add(new JointConnection(JointType.HipRight, JointType.KneeRight));
            connections.Add(new JointConnection(JointType.KneeRight, JointType.AnkleRight));
            connections.Add(new JointConnection(JointType.AnkleRight, JointType.FootRight));
            //sections.Add(new JointBodySection(connections, JointType.HipRight, JointType.FootRight));
            jointConnections = jointConnections.Concat(connections).ToList();
        }

        public static IList<JointConnection> GetConnections()
        {
            return jointConnections;
        }
    }
}
