﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;

namespace Kinesthetic_Mentor.userControls
{
    /// <summary>
    /// Interaction logic for PlayerPanel.xaml
    /// </summary>
    public partial class PlayerPanel : UserControl
    {
        private IPlayerActionReceiver receiver;

        public PlayerPanel()
        {
            InitializeComponent();
        }

        public void SetPlayerReceiver(IPlayerActionReceiver receiver)
        {
            this.receiver = receiver;
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            playButton.Click += Play_Click;
            playButton.Click -= Pause_Click;
            playButton.Click -= Replay_Click;
            playButton.ToolTip = "Pauses the movement at the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/play.png", UriKind.Relative)) };
            receiver.StopReceived();
            slider.SeekFrame(0);
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            playButton.Click -= Play_Click;
            playButton.Click += Pause_Click;
            playButton.ToolTip = "Pauses the movement at the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/pause.png", UriKind.Relative)) };
            receiver.PlayReceived();
        }

        private void Pause_Click(object sender, RoutedEventArgs e)
        {
            playButton.Click += Play_Click;
            playButton.Click -= Pause_Click;
            playButton.ToolTip = "Plays the movement from the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/play.png", UriKind.Relative)) };
            receiver.PauseReceived();
        }

        private void Replay_Click(object sender, RoutedEventArgs e)
        {
            Replay();
            slider.SeekFrame(0);
            receiver.ReplayReceived();
        }

        public void Replay()
        {
            playButton.Click += Pause_Click;
            playButton.Click -= Replay_Click;
            playButton.ToolTip = "Pauses the movement at the current frame";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/pause.png", UriKind.Relative)) };
        }

        public void ChangeToReplay()
        {
            playButton.Click -= Pause_Click;
            playButton.Click -= Play_Click;
            playButton.Click += Replay_Click;
            playButton.ToolTip = "Will play the movement from the start.";
            playButton.Content = new Image() { Source = new BitmapImage(new Uri("../../Images/replay.png", UriKind.Relative)) };
        }
    }
}
