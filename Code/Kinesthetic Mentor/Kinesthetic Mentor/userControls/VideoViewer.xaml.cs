﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Kinesthetic_Mentor.Interfaces;
using System.Timers;
using System.Windows.Threading;

namespace Kinesthetic_Mentor.userControls
{
    /// <summary>
    /// Interaction logic for VideoViewer.xaml
    /// </summary>
    public partial class VideoViewer : UserControl, IPlayerActionReceiver, ISliderReceiver
    {
        public VideoViewer(String fileLoaction)
        {
            InitializeComponent();
            if (fileLoaction == null)
            {
                MessageBox.Show("The movement you wish to see the intro video for does not have one.");
            }
            else
            {
                videoPlayer.Source = new Uri(fileLoaction);
                videoPlayer.Play();
                videoPlayer.Pause();
            }
        }

        public void PlayReceived()
        {
            videoPlayer.Play();
        }

        public void PauseReceived()
        {
            videoPlayer.Pause();
        }

        public void StopReceived()
        {
            videoPlayer.Stop();
        }

        public void ReplayReceived()
        {
            videoPlayer.Stop();
            videoPlayer.Play();
        }

        public void UpdateFrameSeeked(int index)
        {
            videoPlayer.Position = new TimeSpan(0,0,0,0,index);
        }

        public void UpdateMovementEdited(Models.Movement movement)
        {
            throw new NotImplementedException();
        }

        private void Element_MediaOpened(object sender, RoutedEventArgs e)
        {
            VideoPlayerSliderConfig config = new VideoPlayerSliderConfig();
            config.SetUpSlider(playerPanel.slider);
            config.SetTotalFrames((int)videoPlayer.NaturalDuration.TimeSpan.TotalMilliseconds);
            playerPanel.slider.SetUp(this, new VideoPlayerSliderConfig());
            playerPanel.SetPlayerReceiver(this);
            DispatcherTimer t = new DispatcherTimer();
            t.Tick += t_Tick;
            t.Interval = new TimeSpan(0, 0, 0, 0, 1);
            t.Start();
        }

        void t_Tick(object sender, EventArgs e)
        {
            playerPanel.slider.SeekFrame((int)videoPlayer.Position.TotalMilliseconds);
        }
    }
}
