﻿#pragma checksum "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F65F6D83BE5149BC0F0B0B7977D6FCED"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Kinesthetic_Mentor.userControls;
using Kinesthetic_Mentor.userControls.SkeletonLearner;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Kinesthetic_Mentor.userControls.SkeletonLearner {
    
    
    /// <summary>
    /// LearnerMovementPlayer
    /// </summary>
    public partial class LearnerMovementPlayer : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel screen;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Kinesthetic_Mentor.userControls.MovementPlayer learnerPlayer;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Kinesthetic_Mentor.userControls.MovementPlayer mentorPlayer;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Kinesthetic_Mentor.userControls.PlayerPanel playerPanel;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label framesNeeededLabel;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Kinesthetic_Mentor.userControls.SkeletonLearner.ComparisonDataViewer comparisonViewer;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Kinesthetic_Mentor.userControls.SkeletonLearner.CommonMistakeDisplayer commonMistakeDisplayer;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Kinesthetic Mentor;component/usercontrols/skeletonlearner/learnermovementplayer." +
                    "xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
            ((Kinesthetic_Mentor.userControls.SkeletonLearner.LearnerMovementPlayer)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Learner_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.screen = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 3:
            this.learnerPlayer = ((Kinesthetic_Mentor.userControls.MovementPlayer)(target));
            return;
            case 4:
            this.mentorPlayer = ((Kinesthetic_Mentor.userControls.MovementPlayer)(target));
            return;
            case 5:
            this.playerPanel = ((Kinesthetic_Mentor.userControls.PlayerPanel)(target));
            return;
            case 6:
            this.framesNeeededLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            
            #line 31 "..\..\..\..\userControls\SkeletonLearner\LearnerMovementPlayer.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.TryAgain_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.comparisonViewer = ((Kinesthetic_Mentor.userControls.SkeletonLearner.ComparisonDataViewer)(target));
            return;
            case 9:
            this.commonMistakeDisplayer = ((Kinesthetic_Mentor.userControls.SkeletonLearner.CommonMistakeDisplayer)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

