﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;
using Kinesthetic_Mentor.userControls.SkeletonLearner;
using Kinesthetic_Mentor.userControls;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Utility;
using Kinesthetic_Mentor.userControls.PracticeSessions;
using Kinesthetic_Mentor.userControls.Help;

namespace Kinesthetic_Mentor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window , ISkeletonCalibratedReceiver
    {
        private KinectSensorManager sensorManager = new KinectSensorManager();

        public MainWindow()
        {
            InitializeComponent();
        }

        public void Window_Loaded(object sender,RoutedEventArgs args)
        {
            JointMapping.SetUp();
            //KinectSkeletonCalibrator calibrator = new KinectSkeletonCalibrator(this);
            sensorChooser.KinectSensorChooser = new Microsoft.Kinect.Toolkit.KinectSensorChooser();
            sensorChooser.KinectSensorChooser.KinectChanged += sensorManager.KinectSensorChooser_KinectChanged;
            sensorChooser.KinectSensorChooser.Start();
            foreach (UIElement element in navigationGrid.Children)
            {
                ((Button)element).Click += HighlightButton;
            }
        }

        public void Window_Closed(object sender, EventArgs args)
        {
            KinectSensorManager.unregisterAllSkeletonReceivers();
            sensorManager.stopKinect();
        }

        private void Record_Click(object sender, RoutedEventArgs e)
        {
            KinectSensorManager.unregisterAllSkeletonReceivers();
            SwapScreen(new Recorder());
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            KinectSensorManager.unregisterAllSkeletonReceivers();
            SwapScreen(new HomeScreen());
        }

        private void SwapScreen(UserControl newScreen)
        {
            screen.Children.Clear();
            newScreen.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            newScreen.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
            Border border = new Border();
            border.BorderBrush = new SolidColorBrush(Colors.White);
            border.BorderThickness = new Thickness(3);
            border.Child = newScreen;
            screen.Children.Add(border);
        }

        private void HighlightButton(object sender, RoutedEventArgs e)
        {
            foreach (UIElement element in navigationGrid.Children)
            {
                ((Button)element).Background = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            }
            ((Button)sender).Background = new SolidColorBrush(Colors.Indigo);
        }

        private void Learn_Click(object sender, RoutedEventArgs e)
        {
            Learner learner = new Learner();
            learner.Height = screen.ActualHeight;
            learner.Width = screen.ActualWidth;
            learner.SetUpNavigation();
            SwapScreen(learner);
        }

        public void updateSkeletonCalibratedReceiver()
        {
            navigationGrid.IsEnabled = true;
        }

        private void Practice_Click(object sender, RoutedEventArgs e)
        {
            PracticeSessionHome practice = new PracticeSessionHome();
            practice.Height = screen.ActualHeight;
            practice.Width = screen.ActualWidth;
            practice.SetUpNavigation();
            SwapScreen(practice);
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            Glossary help = new Glossary();
            help.Height = screen.ActualHeight;
            help.Width = screen.ActualWidth;
            SwapScreen(help);
        }
    }
}