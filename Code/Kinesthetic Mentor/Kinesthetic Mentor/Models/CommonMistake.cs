﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models
{
    [Serializable]
    public class CommonMistake
    {
        public String Message { get; set; }
        public MovementPhase MistakePhase { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public String MistakeVideoLocation { get; set; }

        public CommonMistake(String message, MovementPhase mistake,int startIndex, int endIndex)
        {
            Message = message;
            MistakePhase = mistake;
            StartIndex = startIndex;
            EndIndex = endIndex;
        }
    }
}
