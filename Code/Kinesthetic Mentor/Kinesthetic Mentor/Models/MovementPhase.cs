﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models
{
    [Serializable]
    public class MovementPhase
    {
        private IList<MovementFrame> frames = new List<MovementFrame>();
        private IList<CommonMistake> commonMistakes = new List<CommonMistake>();
        public String Name { get; set; }

        public MovementPhase()
        {

        }

        public void RemoveFrames(int startIndex,int endIndex)
        {
            int i = startIndex;
            if (i >= 0 && endIndex > 1)
            {
                while (startIndex < frames.Count && i <= endIndex)
                {
                    frames.RemoveAt(startIndex);
                    i++;
                }
            }
        }

        public MovementPhase(IList<MovementFrame> frames)
        {
            this.frames = frames;
        }

        public IList<MovementFrame> GetMovementFrames()
        {
            return frames;
        }

        public void AddMovementFrame(MovementFrame frame)
        {
            frames.Add(frame);
        }

        public void AddCommonMistake(CommonMistake mistake)
        {
            commonMistakes.Add(mistake);
        }

        public void RemoveCommonMistake(CommonMistake mistake)
        {
            commonMistakes.Remove(mistake);
        }

        public IList<CommonMistake> GetCommonMistakes()
        {
            return commonMistakes;
        }

        public void MergePhaseFrames(IList<MovementFrame> frames)
        {
            this.frames = frames.Concat(this.frames).ToList();
        }

        public void MergePhaseFrames(IList<MovementFrame> frames,int startIndex,int endIndex)
        {
            for (int i = startIndex; i <= endIndex && i < frames.Count; i++)
            {
                this.frames.Add(frames[i]);
            }
        }
    }
}
