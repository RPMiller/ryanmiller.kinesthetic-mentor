﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace Kinesthetic_Mentor.Models
{
    [Serializable]
    public class MovementFrame
    {
        private MySkeleton skeleton;

        public MovementFrame(MySkeleton skeleton)
        {
            this.skeleton = skeleton;
        }

        public MySkeleton GetSkeleton()
        {
            return skeleton;
        }
    }
}
