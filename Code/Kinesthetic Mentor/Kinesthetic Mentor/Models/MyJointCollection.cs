﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using System.Collections;

namespace Kinesthetic_Mentor.Models
{
    [Serializable]
    public class MyJointCollection : IEnumerable<MyJoint>, IEnumerable
    {
        public int Count { get { return 20; } protected set { ;} }
        private MyJoint[] joints = new MyJoint[20];
        public MyJoint this[JointType jointType] { 
            get
            {
                return joints[(int)jointType];
            } 
            set
            {
                joints[(int)jointType] = value;
            } }

        public IEnumerator<MyJoint> GetEnumerator()
        {
            foreach (JointType item in Enum.GetValues(typeof(JointType)))
            {
                yield return this[item];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
