﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using System.Diagnostics;

namespace Kinesthetic_Mentor.Models
{
    [Serializable]
    [DebuggerDisplay("Position:{Position} JointType:{JointType} TrackingState:{TrackingState}")]
    public struct MyJoint
    {
        public static bool operator !=(MyJoint joint1, MyJoint joint2)
        {
            return !joint1.Equals(joint2);
        }
        public static bool operator ==(MyJoint joint1, MyJoint joint2)
        {
            return joint1.Equals(joint2);
        }

        public JointType JointType { get; set; }
        public SkeletonPoint Position { get; set; }
        public JointTrackingState TrackingState { get; set; }

        public bool Equals(MyJoint joint)
        {
            return joint.Position == Position && joint.JointType == JointType && joint.TrackingState == TrackingState;
        }
        public override bool Equals(object obj)
        {
            bool isEqual = false;
            if (obj.GetType().Equals(typeof(Joint)))
            {
                isEqual = Equals((Joint)obj);
            }
            return isEqual;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
