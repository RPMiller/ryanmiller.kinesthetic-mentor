﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace Kinesthetic_Mentor.Models
{
    [Serializable]
    public class MySkeleton
    {
        public SkeletonTrackingState TrackingState { get; set; }
        public MyJointCollection Joints { get; set; }

        public MySkeleton(SkeletonTrackingState state, JointCollection collection)
        {
            TrackingState = state;
            MyJointCollection joints = new MyJointCollection();
            foreach (Joint joint in collection)
            {
                joints[joint.JointType] = new MyJoint {JointType = joint.JointType,Position = joint.Position, TrackingState = joint.TrackingState };
            }
            Joints = joints;
        }

        public MySkeleton()
        {

        }
    }
}
