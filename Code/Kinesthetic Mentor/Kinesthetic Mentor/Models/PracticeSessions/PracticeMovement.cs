﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.Utility;
using System.Runtime.Serialization;

namespace Kinesthetic_Mentor.Models.PracticeSessions
{
    [Serializable]
    public class PracticeMovement : ISerializable 
    {
        private Movement movement;
        public String CopyMovementFileLocation = null;
        public int NumberOfReps { get; set; }

        public PracticeMovement(String copyMovementFileLocation)
        {
            FlatFileIO io = new FlatFileIO();
            this.CopyMovementFileLocation = copyMovementFileLocation;
            movement = io.ReadMovement(copyMovementFileLocation);
        }

        public PracticeMovement(Movement movement)
        {
            this.movement = movement;
        }

        protected PracticeMovement(SerializationInfo info, StreamingContext context)
        {
            CopyMovementFileLocation = info.GetString("loc");
            NumberOfReps = info.GetInt32("reps");
            if (CopyMovementFileLocation == null)
            {
                movement = (Movement)info.GetValue("mov", typeof(Movement));
            }
            else
            {
                FlatFileIO io = new FlatFileIO();
                movement = io.ReadMovement(CopyMovementFileLocation);
            }
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (NumberOfReps < 1)
            {
                NumberOfReps = 1;
            }
            info.AddValue("loc",CopyMovementFileLocation);
            info.AddValue("reps", NumberOfReps);
            if (CopyMovementFileLocation == null)
            {
                info.AddValue("mov", movement);
            }
        }

        public void SetMovement(Movement movement)
        {
            CopyMovementFileLocation = null;
            this.movement = movement;
        }

        public Movement GetMovement()
        {
            return movement;
        }
    }
}
