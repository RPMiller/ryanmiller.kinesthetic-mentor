﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models.PracticeSessions
{
    [Serializable]
    public class PracticeSession
    {
        private IList<PracticeMovement> practiceMovements = new List<PracticeMovement>();
        public String Name { get; set; }
        public String Description { get; set; }

        public IList<PracticeMovement> GetPracticeMovements()
        {
            return practiceMovements;
        }

        public void AddPracticeMovement(PracticeMovement movement)
        {
            practiceMovements.Add(movement);
        }

        public int GetNumberOfMovements()
        {
            return practiceMovements.Count();
        }
    }
}
