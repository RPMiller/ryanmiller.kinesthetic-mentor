﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models
{
    [Serializable]
    public class Movement
    {
        private IList<MovementPhase> phases = new List<MovementPhase>();
        public String IntroVideoLocation { get; set; }
        public String Name { get; set; }
        public double PercentOfFramesCorrectNeeded;

        public Movement(double percentOfFramesCorrectNeeded = 50)
        {
            this.PercentOfFramesCorrectNeeded = percentOfFramesCorrectNeeded;
        }

        public Movement(IList<MovementPhase> phases)
        {
            this.phases = phases;
        }

        public IList<MovementPhase> GetMovementPhases()
        {
            return phases;
        }

        public void AddMovementPhase(MovementPhase phase)
        {
            phases.Add(phase);
        }

        public MovementFrame GetFrameAtIndex(int index)
        {
            MovementFrame frame = null;
            int phaseStart = 0;
            for (int i = 0; i < phases.Count && frame == null; i++)
            {
                MovementPhase currentPhase = phases[i];
                int phaseLength = currentPhase.GetMovementFrames().Count;
                if (index < phaseStart + phaseLength && index >= phaseStart)
                {
                    frame = currentPhase.GetMovementFrames()[index - phaseStart];
                }
                else
                {
                    phaseStart += phaseLength;
                }
            }
            if (frame == null)
            {
                throw new IndexOutOfRangeException("Frame at the index " + index + " does not exist");
            }
            return frame;
        }

        public int GetPhaseIndexAtFrameIndex(int frameIndex)
        {
            int phaseStart = 0;
            int index = -1;
            for (int i = 0; i < phases.Count && index == -1; i++)
            {
                MovementPhase currentPhase = phases[i];
                int phaseLength = currentPhase.GetMovementFrames().Count;
                if (frameIndex < phaseStart + phaseLength && frameIndex >= phaseStart)
                {
                    index = i;
                }
                else
                {
                    phaseStart += phaseLength;
                }
            }
            if (phaseStart == 0)
            {
                index = 0;
            }
            if (index == -1)
            {
                throw new IndexOutOfRangeException("Phase at frame index " + frameIndex + " does not exist");
            }
            return index;
        }

        public void RemoveFrames(int startIndex,int endIndex, Kinesthetic_Mentor.Interfaces.IPhaseDeletedReceiver receiver)
        {
            int phaseStart = 0;
            bool isDeleting = true;
            for (int i = 0; i < phases.Count && isDeleting;i++ )
            {
                MovementPhase phase = phases[i];
                int phaseLength = phase.GetMovementFrames().Count;
                if (DoesTheAreaGoFromStartToEndOfPhase(startIndex, phaseStart, endIndex, phaseLength))
                {
                    phases.RemoveAt(i);
                    receiver.UpdatePhaseDeletedReceiver(i);
                    i--;
                }
                else if (DoesTheAreaGoFromMiddleToMiddleOfPhase(startIndex, endIndex, phaseStart, phaseLength))
                {
                    phase.RemoveFrames(startIndex - phaseStart, endIndex - phaseStart);
                }
                else if (DoesTheAreaGoFromMiddleToEndOfPhase(startIndex, endIndex, phaseStart, phaseLength))
                {
                    phase.RemoveFrames(startIndex - phaseStart, phaseLength + phaseStart);
                }
                else if (DoesTheAreaGoFromStartToMiddleOfPhase(endIndex, phaseStart, phaseLength))
                {
                    phase.RemoveFrames(0, endIndex - phaseStart);
                    isDeleting = false;
                }
                phaseStart += phaseLength;
            }
        }

        public void MergeFramesIntoPhase(int startIndex, int endIndex, Kinesthetic_Mentor.Interfaces.IPhaseDeletedReceiver receiver)
        {
            int phaseStart = 0;
            bool isSearching = true;
            MovementPhase newPhase = new MovementPhase();
            int phasesAddedBefore = 0;

            for (int i = 0; i < phases.Count && isSearching; i++)
            {
                MovementPhase phase = phases[i];
                int phaseLength = phase.GetMovementFrames().Count;
                if (DoesTheAreaGoFromStartToEndOfPhase(startIndex, phaseStart, endIndex, phaseLength))
                {
                    MergeIntoPhaseAreaGreaterThanPhase(receiver, newPhase, phasesAddedBefore, i, phase);
                    i--;
                }
                else if (DoesTheAreaGoFromMiddleToMiddleOfPhase(startIndex, endIndex, phaseStart, phaseLength))
                {
                    MergeIntoPhaseAreaInMiddleOfPhase(startIndex, endIndex, phaseStart, newPhase, phase, phaseLength);
                    i++;
                }
                else if (DoesTheAreaGoFromMiddleToEndOfPhase(startIndex, endIndex, phaseStart, phaseLength))
                {
                    newPhase.MergePhaseFrames(phase.GetMovementFrames(), startIndex - phaseStart, phaseLength + phaseStart);
                    phase.RemoveFrames(startIndex - phaseStart, phaseLength + phaseStart);
                }
                else if (DoesTheAreaGoFromStartToMiddleOfPhase(endIndex, phaseStart, phaseLength))
                {
                    newPhase.MergePhaseFrames(phase.GetMovementFrames(), 0, endIndex - (phaseStart - 1));
                    phase.RemoveFrames(0, endIndex - phaseStart);
                }

                phaseStart += phaseLength;
            }
            bool isStartAtZero = true;
            if (startIndex > 0)
            {
                isStartAtZero = false;
                startIndex--;
            }
            int insertIndex = GetPhaseIndexAtFrameIndex(startIndex);
            if (!isStartAtZero)
            {
                insertIndex++;
            }
            phases.Insert(insertIndex,newPhase);
            receiver.UpdatePhaseMovedReceiver(insertIndex);
        }

        private bool DoesTheAreaGoFromStartToMiddleOfPhase(int endIndex, int phaseStart, int phaseLength)
        {
            return endIndex > phaseStart && endIndex <= phaseStart + phaseLength;
        }

        private bool DoesTheAreaGoFromStartToEndOfPhase(int startIndex, int phaseStart, int endIndex, int phaseLength)
        {
            return startIndex <= phaseStart && endIndex >= phaseStart + phaseLength - 1;
        }

        private bool DoesTheAreaGoFromMiddleToMiddleOfPhase(int startIndex, int endIndex, int phaseStart, int phaseLength)
        {
            return (startIndex > phaseStart && endIndex < phaseStart + phaseLength);
        }

        private bool DoesTheAreaGoFromMiddleToEndOfPhase(int startIndex, int endIndex, int phaseStart, int phaseLength)
        {
            return startIndex > phaseStart && startIndex < phaseStart + phaseLength && endIndex - phaseStart >= phaseLength;
        }

        private void MergeIntoPhaseAreaInMiddleOfPhase(int startIndex, int endIndex, int phaseStart, MovementPhase newPhase, MovementPhase phase, int phaseLength)
        {
            MovementPhase after = new MovementPhase();
            newPhase.MergePhaseFrames(phase.GetMovementFrames(), startIndex - phaseStart, endIndex - phaseStart);
            after.MergePhaseFrames(phase.GetMovementFrames(), (endIndex - phaseStart) + 1, phaseLength);
            phase.RemoveFrames((endIndex - phaseStart) + 1, phaseLength);
            phase.RemoveFrames(startIndex - phaseStart, endIndex - phaseStart);
            phases.Insert(GetPhaseIndexAtFrameIndex(startIndex - 1) + 1, after);
        }

        private void MergeIntoPhaseAreaGreaterThanPhase(Kinesthetic_Mentor.Interfaces.IPhaseDeletedReceiver receiver, MovementPhase newPhase, int phasesAddedBefore, int i, MovementPhase phase)
        {
            newPhase.MergePhaseFrames(phase.GetMovementFrames());
            phases.RemoveAt(i - phasesAddedBefore);
            receiver.UpdatePhaseMovedReceiver(i);
        }

        public int GetTotalNumberOfFrames()
        {
            int frames = 0;

            foreach (MovementPhase phase in phases)
            {
                frames += phase.GetMovementFrames().Count;
            }

            return frames;
        }
    }
}
