﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models.Learner
{
    public class LearnerMovement
    {
        private IList<LearnerPhase> movement = new List<LearnerPhase>();
        private int lastPhaseIndexWhereFrameWasAccessed = 0;

        public LearnerMovement(Movement learner, Movement mentor)
        {
            IList<MovementPhase> learnerPhases = learner.GetMovementPhases();
            IList<MovementPhase> mentorPhases = mentor.GetMovementPhases();
            int phaseStart = 0;
            MovementPhase learnerPhase = learnerPhases[0];
            for (int i = 0; i < mentorPhases.Count; i++)
            {
                MovementPhase mentorPhase = mentorPhases[i];
                int framesInPhase = mentorPhase.GetMovementFrames().Count();
                MovementPhase currentLearnerPhase = CreatePhases(learnerPhase,phaseStart,phaseStart + framesInPhase);
                movement.Add(new LearnerPhase(currentLearnerPhase,mentorPhase,mentor.PercentOfFramesCorrectNeeded));
                phaseStart = phaseStart + framesInPhase;
            }
        }

        private MovementPhase CreatePhases(MovementPhase learnerPhase,int startIndex, int endIndex)
        {
            MovementPhase phase = new MovementPhase(); 

            for (int i = startIndex; i < endIndex && i < learnerPhase.GetMovementFrames().Count; i++)
            {
                phase.AddMovementFrame(learnerPhase.GetMovementFrames()[i]);
            }

            return phase;
        }

        public LearnerFrame GetFrameAtIndex(int index)
        {
            LearnerFrame frame = null;
            int phaseStart = 0;
            for (int i = 0; i < movement.Count && frame == null; i++)
            {
                LearnerPhase currentPhase = movement[i];
                int phaseLength = currentPhase.GetLearnerFrames().Count;
                if (index < phaseStart + phaseLength && index >= phaseStart)
                {
                    frame = currentPhase.GetLearnerFrames()[index - phaseStart];
                    lastPhaseIndexWhereFrameWasAccessed = i;
                }
                else
                {
                    phaseStart += phaseLength;
                }
            }
            if (frame == null)
            {
                throw new IndexOutOfRangeException("Frame at the index " + index + " does not exist");
            }
            return frame;
        }

        public bool IsCorrect()
        {
            bool isCorrect = true;

            for (int i = 0; i < movement.Count && isCorrect; i++)
            {
                isCorrect = movement[i].IsPhaseSuccessful();
            }

            return isCorrect;
        }

        public int GetPhaseIndexAtFrameIndex(int frameIndex)
        {
            int phaseStart = 0;
            int index = -1;
            for (int i = 0; i < movement.Count && index == -1; i++)
            {
                LearnerPhase currentPhase = movement[i];
                int phaseLength = currentPhase.GetLearnerFrames().Count;
                if (frameIndex < phaseStart + phaseLength && frameIndex >= phaseStart)
                {
                    index = i;
                }
                else
                {
                    phaseStart += phaseLength;
                }
            }
            if (index == -1)
            {
                throw new IndexOutOfRangeException("Phase at frame index " + frameIndex + " does not exist");
            }
            return index;
        }

        public IList<LearnerPhase> GetPhases()
        {
            return movement;
        }

        public int GetLastPhaseIndexWhereFrameWasAccessed()
        {
            return lastPhaseIndexWhereFrameWasAccessed;
        }
    }
}
