﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models.Learner
{
    public class ComparisonValue
    {
        public double Data { get; set; }
        public bool IsCorrect { get; set; }

        public ComparisonValue(double data, bool isCorrect)
        {
            this.Data = data;
            this.IsCorrect = isCorrect;
        }
    }
}
