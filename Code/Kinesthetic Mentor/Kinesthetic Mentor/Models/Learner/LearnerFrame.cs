﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.userControls;
using Kinesthetic_Mentor.Utility;

namespace Kinesthetic_Mentor.Models.Learner
{
    public class LearnerFrame
    {
        private MovementFrame learnerFrame;
        private MovementFrame mentorFrame;
        private ComparisionData mentorData;
        private ComparisionData comparison = new ComparisionData();
        private bool isCorrect = true;
        private const double JITTER_RADIUS = .05;

        public LearnerFrame(ComparisionData learnerData, ComparisionData mentorData,MovementFrame learnerFrame, MovementFrame mentorFrame)
        {
            this.learnerFrame = learnerFrame;
            this.mentorFrame = mentorFrame;
            this.mentorData = mentorData;
            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                comparison[jointType] = new ComparisonValue(0,true);
                if (learnerFrame.GetSkeleton().Joints[jointType].TrackingState == JointTrackingState.Tracked && mentorFrame.GetSkeleton().Joints[jointType].TrackingState == JointTrackingState.Tracked)
                {
                    double data = learnerData[jointType].Data - mentorData[jointType].Data;
                    data = DataBeyondTheMarginOfError(data, jointType);
                    comparison[jointType].Data += data;
                }
            }
            SetUpForCalculateErrorBasedOnPosition(learnerFrame.GetSkeleton(), mentorFrame.GetSkeleton());
            CheckIfIsCorrect(CalculateTolorence(mentorData));
        }

        private double CalculateTolorence(ComparisionData mentorData)
        {
            double tolorence = 0;

            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                if (learnerFrame.GetSkeleton().Joints[jointType].TrackingState == JointTrackingState.Tracked && mentorFrame.GetSkeleton().Joints[jointType].TrackingState == JointTrackingState.Tracked)
                {
                    int numberOfTimesCalculated = JointMapping.GetBodySections().Where(x => x.GetConnections().Where(z => z.GetStartJoint() == jointType || z.GetEndJoint() == jointType).Count() > 0).Count();
                    if (jointType == JointType.ShoulderLeft || jointType == JointType.ShoulderRight || jointType == JointType.ShoulderCenter)
                    {
                        numberOfTimesCalculated += 2;
                    }
                    comparison[jointType].Data /= numberOfTimesCalculated;
                    tolorence += Math.Abs(mentorData[jointType].Data);
                }
            }
            return Math.Log(tolorence,4);
        }

        private void CheckIfIsCorrect(double tolorence)
        {
            const float MINIMUM_TOLERENCE = .12f;
            double SCALE = 2.6;
            tolorence *= SCALE;
            double CurrentTolerance = tolorence > MINIMUM_TOLERENCE ? tolorence : MINIMUM_TOLERENCE;
            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                double data = comparison[jointType].Data;
                if (data >= CurrentTolerance)
                {
                    isCorrect = false;
                    comparison[jointType].IsCorrect = false;
                }
                comparison[jointType].Data = Math.Round(comparison[jointType].Data / (CurrentTolerance * 10), 8);
            }
        }

        private void SetUpForCalculateErrorBasedOnPosition(MySkeleton learnerSkeleton,MySkeleton mentorSkeleton)
        {
            foreach (JointBodySection bodySection in JointMapping.GetBodySections())
            {
                JointConnection firstJointConnection = null;
                foreach (JointConnection nextJointConnection in bodySection.GetConnections())
                {
                    if (firstJointConnection != null)
                    {
                        if (AreTheJointsTracked(learnerSkeleton, mentorSkeleton, firstJointConnection.GetStartJoint(),firstJointConnection.GetEndJoint(),nextJointConnection.GetEndJoint()))
                        {
                            CalculateErrorBasedOnPosition(learnerSkeleton.Joints[firstJointConnection.GetStartJoint()],
                                learnerSkeleton.Joints[firstJointConnection.GetEndJoint()], learnerSkeleton.Joints[nextJointConnection.GetEndJoint()],
                                mentorSkeleton.Joints[firstJointConnection.GetStartJoint()],
                                mentorSkeleton.Joints[firstJointConnection.GetEndJoint()], mentorSkeleton.Joints[nextJointConnection.GetEndJoint()]);
                        }
                    }
                    firstJointConnection = nextJointConnection;
                }
            }
        }

        private static bool AreTheJointsTracked(MySkeleton learnerSkeleton, MySkeleton mentorSkeleton, JointType firstJoint, JointType secondJoint, JointType thirdJoint)
        {
            return learnerSkeleton.Joints[firstJoint].TrackingState == JointTrackingState.Tracked && learnerSkeleton.Joints[secondJoint].TrackingState == JointTrackingState.Tracked && learnerSkeleton.Joints[thirdJoint].TrackingState == JointTrackingState.Tracked && 
                mentorSkeleton.Joints[firstJoint].TrackingState == JointTrackingState.Tracked && mentorSkeleton.Joints[secondJoint].TrackingState == JointTrackingState.Tracked && mentorSkeleton.Joints[thirdJoint].TrackingState == JointTrackingState.Tracked;
        }

        private void CalculateErrorBasedOnPosition(MyJoint learnerJoint1, MyJoint learnerJoint2, MyJoint learnerJoint3,
            MyJoint mentorJoint1, MyJoint mentorJoint2, MyJoint mentorJoint3)
        {
            const double TRIANGLE_ANGLE = Math.PI;

            double learnerDistanceA = MyMath.GetDistance(learnerJoint1.Position.X, learnerJoint1.Position.Y, learnerJoint1.Position.Z,
                learnerJoint2.Position.X, learnerJoint2.Position.Y, learnerJoint2.Position.Z);
            double learnerDistanceB = MyMath.GetDistance(learnerJoint2.Position.X, learnerJoint2.Position.Y, learnerJoint2.Position.Z,
                learnerJoint3.Position.X, learnerJoint3.Position.Y, learnerJoint3.Position.Z);
            double learnerDistanceC = MyMath.GetDistance(learnerJoint3.Position.X, learnerJoint3.Position.Y, learnerJoint3.Position.Z,
                learnerJoint1.Position.X, learnerJoint1.Position.Y, learnerJoint1.Position.Z);
            double mentorDistanceA = MyMath.GetDistance(mentorJoint1.Position.X, mentorJoint1.Position.Y, mentorJoint1.Position.Z,
                mentorJoint2.Position.X, mentorJoint2.Position.Y, mentorJoint2.Position.Z);
            double mentorDistanceB = MyMath.GetDistance(mentorJoint2.Position.X, mentorJoint2.Position.Y, mentorJoint2.Position.Z,
                mentorJoint3.Position.X, mentorJoint3.Position.Y, mentorJoint3.Position.Z);
            double mentorDistanceC = MyMath.GetDistance(mentorJoint3.Position.X, mentorJoint3.Position.Y, mentorJoint3.Position.Z,
                mentorJoint1.Position.X, mentorJoint1.Position.Y, mentorJoint1.Position.Z);

            double learnerAngleA = MyMath.GetAngleLawOfCosinesForA(learnerDistanceA, learnerDistanceB, learnerDistanceC);
            double learnerAngleB = MyMath.GetAngleLawOfCosinesForA(learnerDistanceB, learnerDistanceA, learnerDistanceC);
            double learnerAngleC = TRIANGLE_ANGLE - (learnerAngleA + learnerAngleB);

            double mentorAngleA = MyMath.GetAngleLawOfCosinesForA(mentorDistanceA, mentorDistanceB, mentorDistanceC);
            double mentorAngleB = MyMath.GetAngleLawOfCosinesForA(mentorDistanceB, mentorDistanceA, mentorDistanceC);
            double mentorAngleC = TRIANGLE_ANGLE - (mentorAngleA + mentorAngleB);

            CalculateErrorUsingAngles(mentorAngleA - learnerAngleA, learnerJoint3.JointType);
            CalculateErrorUsingAngles(mentorAngleB - learnerAngleB, learnerJoint1.JointType);
            CalculateErrorUsingAngles(mentorAngleC - learnerAngleC, learnerJoint2.JointType);
        }

        private void CalculateErrorUsingAngles(double angleDifference, JointType jointType)
        {
            if (comparison[jointType] != null || comparison[jointType].Data > JITTER_RADIUS)
            {
                double error = Math.Abs(angleDifference);
                const double SCALE = .36;
                comparison[jointType].Data += error * SCALE;
            }
        }

        private double DataBeyondTheMarginOfError(double data,JointType jointType)
        {
            const double MARGIN_OF_ERROR = .20;
            double offSet = Math.Abs(mentorData[jointType].Data * MARGIN_OF_ERROR);
            double maxMargin = mentorData[jointType].Data + offSet + JITTER_RADIUS;
            double minMargin = mentorData[jointType].Data - Math.Abs(offSet) - JITTER_RADIUS;
            const double SCALE = 11.5;
            if ((data >= minMargin && data <= maxMargin))
            {
                data = 0;
            }
            else
            {
                data = Math.Abs(offSet);
            }
            return data * SCALE;
        }

        public ComparisionData GetComparison()
        {
            return comparison;
        }

        public bool GetIsCorrect()
        {
            return isCorrect;
        }
    }
}
