﻿using Microsoft.Kinect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models.Learner
{
    public class ComparisionData : IEnumerable<ComparisonValue>, IEnumerable
    {
        public int Count { get { return 20; } protected set { ;} }
        private ComparisonValue[] differnece = new ComparisonValue[20];

        public ComparisonValue this[JointType jointType]
        {
            get
            {
                if (differnece[(int)jointType] == null)
                {
                    differnece[(int)jointType] = new ComparisonValue(0,true);
                }
                return differnece[(int)jointType];
            }
            set
            {
                differnece[(int)jointType] = value;
            }
        }

        public IEnumerator<ComparisonValue> GetEnumerator()
        {
            foreach (JointType item in Enum.GetValues(typeof(JointType)))
            {
                yield return this[item];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    //public enum Position
    //{
    //    X = 0,
    //    Y = 1,
    //    Z = 2
    //}
}
