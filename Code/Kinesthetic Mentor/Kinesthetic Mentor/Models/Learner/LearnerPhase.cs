﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Models.Learner
{
    public class LearnerPhase
    {
        private IList<LearnerFrame> learnerPhase;
        private IList<IList<LearnerFrame>> commonMistakePhases = new List<IList<LearnerFrame>>();
        private double framePercentForSuccess;
        public String Name { get; set; }
        public double NumberOfFramesCorrect { get; private set; }

        public LearnerPhase(MovementPhase learner,MovementPhase mentor, double framePercentForSuccess = 50)
        {
            this.Name = mentor.Name;
            this.framePercentForSuccess = framePercentForSuccess;
            IList<MovementFrame> learnerFrames = learner.GetMovementFrames();
            IList<MovementFrame> mentorFrames = mentor.GetMovementFrames();
            learnerPhase = GenerateMovmentFramesFromPhases(learnerFrames, mentorFrames);
            GenerateCommonMistakeFrames(learner, mentor);
        }

        private void GenerateCommonMistakeFrames(MovementPhase learner, MovementPhase mentor)
        {
            IList<MovementFrame> learnerFrames = new List<MovementFrame>();
            IList<MovementFrame> mentorFrames;

            foreach (CommonMistake mistake in mentor.GetCommonMistakes())
            {
                mentorFrames = mistake.MistakePhase.GetMovementFrames();
                IList<MovementFrame> recordedLearnerFrames  = learner.GetMovementFrames();
                for (int i = mistake.StartIndex; i < mistake.EndIndex; i++)
                {
                    learnerFrames.Add(recordedLearnerFrames[i]);
                }
                commonMistakePhases.Add(GenerateMovmentFramesFromPhases(learnerFrames,mentorFrames));
            }
        }

        private IList<LearnerFrame> GenerateMovmentFramesFromPhases(IList<MovementFrame> learnerFrames, IList<MovementFrame> mentorFrames)
        {
            IList<LearnerFrame> frames = new List<LearnerFrame>();
            MovementFrame previousLearnerFrame = learnerFrames[0];
            MovementFrame previousMentorFrame = learnerFrames[0];

            for (int i = 0; i < learnerFrames.Count && i < mentorFrames.Count; i++)
            {
                MovementFrame nextLearnerFrame = learnerFrames[i];
                MovementFrame nextMentorFrame = mentorFrames[i];
                ComparisionData learnerData = GetComparedFrames(previousLearnerFrame.GetSkeleton(), nextLearnerFrame.GetSkeleton());
                ComparisionData mentorData = GetComparedFrames(previousMentorFrame.GetSkeleton(), nextMentorFrame.GetSkeleton());
                LearnerFrame learnerFrame = new LearnerFrame(learnerData, mentorData, nextLearnerFrame, nextMentorFrame);
                frames.Add(learnerFrame);
                previousLearnerFrame = nextLearnerFrame;
                previousMentorFrame = nextMentorFrame;
            }

            return frames;
        }

        private ComparisionData GetComparedFrames(MySkeleton previous, MySkeleton next)
        {
            ComparisionData data = new ComparisionData();

            foreach (JointType jointType in Enum.GetValues(typeof(JointType)))
            {
                double distance = Math.Abs(previous.Joints[jointType].Position.X - next.Joints[jointType].Position.X);
                distance += Math.Abs(previous.Joints[jointType].Position.Y - next.Joints[jointType].Position.Y);
                distance += Math.Abs(previous.Joints[jointType].Position.Z - next.Joints[jointType].Position.Z);
                data[jointType] = new ComparisonValue(distance, true);
            }

            return data;
        }

        public bool IsPhaseSuccessful()
        {
            NumberOfFramesCorrect = 0;

            foreach (LearnerFrame frame in learnerPhase)
            {
                if (frame.GetIsCorrect())
                {
                    NumberOfFramesCorrect++;
                }
            }
            return NumberOfFramesCorrect * 100 >= framePercentForSuccess * learnerPhase.Count();
        }

        public bool IsCommonMistakePhaseSuccessful(int index)
        {
            int numberOfCorrectFrames = 0;

            foreach (LearnerFrame frame in commonMistakePhases[index])
            {
                if (frame.GetIsCorrect())
                {
                    numberOfCorrectFrames++;
                }
            }

            return numberOfCorrectFrames > (framePercentForSuccess / 100 * commonMistakePhases[index].Count());
        }

        public IList<LearnerFrame> GetLearnerFrames()
        {
            return learnerPhase;
        }

        public IList<IList<LearnerFrame>> GetCommonMistakes()
        {
            return commonMistakePhases;
        }
    }
}