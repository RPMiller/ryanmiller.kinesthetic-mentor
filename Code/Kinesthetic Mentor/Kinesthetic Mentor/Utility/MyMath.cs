﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Utility
{
    public class MyMath
    {
        public static double GetAngleLawOfCosinesForA(double a, double b, double c)
        {
            double angle = 0;
            float squares = (float)(Math.Pow(c, 2) + Math.Pow(b, 2) - Math.Pow(a, 2));
            angle = (Math.Acos((squares) / (2 * c * b)));
            return angle;
        }

        public static double GetDistance(double x1, double y1, double z1, double x2, double y2, double z2)
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2) + Math.Pow((z2 - z1), 2));
        }
    }
}
