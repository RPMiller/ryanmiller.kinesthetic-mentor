﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.userControls;
using Kinesthetic_Mentor.Interfaces;
using Microsoft.Kinect;
using Kinesthetic_Mentor.Models;

namespace Kinesthetic_Mentor.Utility
{
    class KinectSkeletonCalibrator : ISkeletonReceiver
    {
        private ISkeletonCalibratedReceiver receiver;
        private static bool isCalibrated = false;

        public KinectSkeletonCalibrator(ISkeletonCalibratedReceiver receiver)
        {
            this.receiver = receiver;
            KinectSensorManager.registerSkeletonReceiver(this);
        }

        private float GetDistanceOfJoints(MyJoint start, MyJoint end)
        {
            return (float)Math.Sqrt(Math.Pow(start.Position.X, 2) + Math.Pow(start.Position.Y, 2) + Math.Pow(start.Position.Z, 2));
        }

        public void UpdateSkeleton(MySkeleton skeleton)
        {
            if (!isCalibrated)
            {
                if (skeleton.Joints.Where(x => !x.TrackingState.Equals(Microsoft.Kinect.JointTrackingState.NotTracked)).Count() == skeleton.Joints.Count())
                {
                    foreach (JointConnection connection in JointMapping.GetConnections())
                    {
                        connection.JointConnectionLength = GetDistanceOfJoints(skeleton.Joints[connection.GetStartJoint()], skeleton.Joints[connection.GetEndJoint()]);
                    }
                    receiver.updateSkeletonCalibratedReceiver();
                    isCalibrated = true;
                }
            }
        }

        private static MySkeleton previousSkeleton;

        public static void AlterSkeletonData(MySkeleton skeleton)
        {
            if (isCalibrated)
            {
                AlterInferredJoints(skeleton.Joints.Where(x => x.TrackingState == JointTrackingState.Inferred).ToList(),skeleton);
                if (previousSkeleton == null)
                {
                    if (skeleton.Joints.Where(x => x.TrackingState.Equals(Microsoft.Kinect.JointTrackingState.Tracked)).Count() == skeleton.Joints.Count())
                    {
                        previousSkeleton = skeleton;
                    }
                }
                else
                {
                    List<MyJoint> untrackedJoints = skeleton.Joints.Where(x => x.TrackingState == JointTrackingState.NotTracked).ToList();
                    for (int i = 0; i < untrackedJoints.Count; i++)
                    {
                        MyJoint joint = untrackedJoints[i];
                        joint = previousSkeleton.Joints[joint.JointType];
                    }
                    previousSkeleton = skeleton;
                }
            }
        }

        private static void AlterInferredJoints(IList<MyJoint> inferredJoints, MySkeleton skeleton)
        {
            int numberOfJointsInferred = inferredJoints.Count();
            IList<JointBodySection> sections = JointMapping.GetBodySections();
            do
            {
                IDictionary<MyJoint, SkeletonPoint> jointsThatWereFixed = new Dictionary<MyJoint, SkeletonPoint>();
                foreach (MyJoint joint in inferredJoints)
                {
                    bool isSearching = true;
                    JointConnection connectionInQuestion = null;
                    for (int i = 0; i < sections.Count() && isSearching; i++)
                    {
                        JointBodySection sectionInQuestion = sections[i];
                        connectionInQuestion = sectionInQuestion.GetOuterJointConnection(joint.JointType);
                        if (connectionInQuestion != null)
                        {
                            isSearching = false;
                        }
                    }
                    if (!isSearching && AreTheOuterJointsTracked(connectionInQuestion,skeleton))
                    {
                        SkeletonPoint point = CalculateJointPosition(joint, skeleton.Joints[connectionInQuestion.GetStartJoint()], skeleton.Joints[connectionInQuestion.GetEndJoint()]);
                        jointsThatWereFixed.Add(joint,point);
                    }
                }
                updateJoints(skeleton, jointsThatWereFixed, inferredJoints);
            }while(numberOfJointsInferred != inferredJoints.Count);
        }

        private static void updateJoints(MySkeleton skeleton, IDictionary<MyJoint, SkeletonPoint> fixedJoints, IList<MyJoint> inferedJoints)
        {
            MyJointCollection collection = new MyJointCollection();
            foreach (MyJoint joint in skeleton.Joints)
            {
                collection[joint.JointType] = joint;
            }
            foreach(KeyValuePair<MyJoint,SkeletonPoint> pairs in fixedJoints)
            {
                collection[pairs.Key.JointType] = new MyJoint { JointType = pairs.Key.JointType, Position = pairs.Value, TrackingState = JointTrackingState.Tracked };
                inferedJoints.Remove(pairs.Key);
            }
        }

        private static bool AreTheOuterJointsTracked(JointConnection connection, MySkeleton skeleton)
        {
            return skeleton.Joints[connection.GetStartJoint()].TrackingState == JointTrackingState.Tracked && skeleton.Joints[connection.GetEndJoint()].TrackingState == JointTrackingState.Tracked;
        }

        private static SkeletonPoint CalculateJointPosition(MyJoint unkownJoint, MyJoint firstKnownJoint, MyJoint secondKnownJoint)
        {
            float unkownToFirstLength = GetLengthOfAppendageFromJointMap(new JointConnection(unkownJoint.JointType,firstKnownJoint.JointType));
            float unkownToSecondLength = GetLengthOfAppendageFromJointMap(new JointConnection(unkownJoint.JointType, secondKnownJoint.JointType));
            float firstToSecondLength = GetDistance(firstKnownJoint.Position.X, secondKnownJoint.Position.X, firstKnownJoint.Position.Y,
                secondKnownJoint.Position.Y, firstKnownJoint.Position.Z, secondKnownJoint.Position.Z);
            if (unkownToSecondLength > unkownToFirstLength)
            {

            }
            return CalculateJointPositionHelper(unkownJoint, firstKnownJoint,secondKnownJoint,unkownToFirstLength,firstToSecondLength,unkownToSecondLength);
        }

        private static float GetLastTriangleAngle(MyJoint secondKnownJoint, float unkownToFirstLength, float unkownToSecondLength, float firstToSecondLength)
        {
            float complementaryAngle = GetAngleLawOfCosinesForB(secondKnownJoint.Position.Y, secondKnownJoint.Position.X, firstToSecondLength) +
                GetAngleLawOfCosinesForB(unkownToFirstLength, unkownToSecondLength, firstToSecondLength);
            return 90 - complementaryAngle;
        }

        private static SkeletonPoint CalculateJointPositionHelper(MyJoint unkownJoint, MyJoint firstKnownJoint, MyJoint secondKnownJoint, float unkownToFirstLength, float unkownToSecondLength, float firstToSecondLength)
        {
            float lastTriangleAngle = GetLastTriangleAngle(secondKnownJoint, unkownToFirstLength, unkownToSecondLength, firstToSecondLength);
            int xDirection = 1;
            int yDirection = 1;
            float lengthBetweenX1AndX2 = (float)Math.Sin(lastTriangleAngle) * firstToSecondLength;
            float lengthBetweenY1AndY2 = (float)Math.Cos(lastTriangleAngle) * firstToSecondLength;
            float x2 = firstKnownJoint.Position.X + (xDirection*lengthBetweenX1AndX2);
            float y2 = firstKnownJoint.Position.Y + (yDirection * lengthBetweenY1AndY2);
            float z2 = GetUnkownZ(firstKnownJoint.Position.X,x2,firstKnownJoint.Position.Y,y2,firstKnownJoint.Position.Z);
            return new SkeletonPoint { X = lengthBetweenX1AndX2, Y = lengthBetweenY1AndY2, Z = z2 };
        }

        private static float GetLengthOfAppendageFromJointMap(JointConnection connection)
        {
            float value = 0;
            IList<JointConnection> connections = JointMapping.GetConnections();
            for(int i = 0; i < connections.Count() && value == 0;i++)
            {
                JointConnection connectionInQuestion = connections[i];
                if (connection.Equals(connectionInQuestion))
                {
                    value = connectionInQuestion.JointConnectionLength;
                }
            }
            return value;
        }

        public static float GetUnkownZ(float x1, float x2, float y1, float y2, float z1)
        {
            float z2 = 0;

            return z2;
        }

        private static float GetAngleLawOfCosinesForB(float a, float b, float c)
        {
            float angle = 0;
            if (c > a)
            {
                if (b > c)
                {
                    float squares = (float)(Math.Pow(a, 2) + Math.Pow(b, 2) - Math.Pow(c, 2));
                    angle = (float)(Math.Acos((squares) / (2 * a * b)));
                }
                else
                {
                    float squares = (float)(Math.Pow(a, 2) + Math.Pow(c, 2) - Math.Pow(b, 2));
                    angle = (float)(Math.Acos((squares) / (2 * a * c)));
                }
            }
            else
            {
                angle = GetAngleLawOfCosinesForB(c, b, a);
            }
            return angle;
        }

        private static float GetDistance(float x1, float x2, float y1, float y2, float z1, float z2)
        {
            return (float)Math.Sqrt(SquareDifference(x1, x2) + SquareDifference(y1, y2) + SquareDifference(z1, z2));
        }

        private static float SquareDifference(float x1, float x2)
        {
            return (float)Math.Pow(x2 - x1, 2);
        }
    }
}
