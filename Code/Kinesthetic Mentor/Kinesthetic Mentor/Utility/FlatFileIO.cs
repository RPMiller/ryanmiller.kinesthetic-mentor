﻿using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Models.PracticeSessions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Kinesthetic_Mentor.Utility
{
    class FlatFileIO : Kinesthetic_Mentor.Interfaces.IMovementDAL
    {
        private const String MOVEMENT_FILE_EXTENSION = ".kmo";
        private const String PRACTICE_SESSION_FILE_EXTENSION = ".pra";
        private const String FOLDER_LOCATION = @"../../RecordedMovements/";

        public void SaveMovement(Models.Movement movement, string fileLocation)
        {
            using(Stream stream = new FileStream(fileLocation + MOVEMENT_FILE_EXTENSION, FileMode.Create, FileAccess.Write))
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, movement);
            }
        }

        public Models.Movement ReadMovement(string fileLocation)
        {
            Movement movement;
            using (Stream stream = new FileStream(fileLocation + MOVEMENT_FILE_EXTENSION, FileMode.Open, FileAccess.Read))
            {
                IFormatter formatter = new BinaryFormatter();
                movement =  (Movement)formatter.Deserialize(stream);
            }
            return movement;
        }

        public static String GetMovementFileExtension()
        {
            return MOVEMENT_FILE_EXTENSION;
        }

        public static String GetPracticeSessionFileExtension()
        {
            return PRACTICE_SESSION_FILE_EXTENSION;
        }

        public static bool FileExists(String fileNameWithExtension)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(FlatFileIO.GetFolderLocation());
            FileInfo[] files = directoryInfo.GetFiles();
            bool exists = false;

            for(int i = 0; i < files.Count() && !exists; i++)
            {
                exists = files[i].Name == fileNameWithExtension;
            }

            return exists;
        }

        public void DeleteMovement(string fileLocation)
        {
            int startIndex = fileLocation.LastIndexOf('/') + 1;
            String fileName = fileLocation.Substring(startIndex, fileLocation.Count() - startIndex).ToString();
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete " + fileName + "?","Delete", MessageBoxButton.YesNoCancel);
            if (result == MessageBoxResult.Yes)
            {
                SaveDeletedMovmentInPracticeSessions(fileLocation);
                
                File.Delete(fileLocation + MOVEMENT_FILE_EXTENSION);
            }
        }

        private void SaveDeletedMovmentInPracticeSessions(string fileLocation)
        {
            String fileFilter = "*" + Kinesthetic_Mentor.Utility.FlatFileIO.GetPracticeSessionFileExtension();
            DirectoryInfo directoryInfo = new DirectoryInfo(FlatFileIO.GetFolderLocation());
            FileInfo[] files = directoryInfo.GetFiles(fileFilter);
            Movement movement = ReadMovement(fileLocation);
            foreach (FileInfo info in files)
            {
                PracticeSession session = ReadPracticeSession(info.FullName.Substring(0,info.FullName.Length - 4));
                bool sessionChanged = false;
                foreach (PracticeMovement practice in session.GetPracticeMovements())
                {
                    if (practice.CopyMovementFileLocation == fileLocation)
                    {
                        practice.SetMovement(movement);
                        sessionChanged = true;
                    }
                }
                if (sessionChanged)
                {
                    SavePracticeSession(session, info.FullName.Substring(0, info.FullName.Length - 4));
                }
            }
        }

        public void DeletePracticeSession(String fileLocation)
        {
            int startIndex = fileLocation.LastIndexOf('/') + 1;
            String fileName = fileLocation.Substring(startIndex, fileLocation.Count() - startIndex).ToString();
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete " + fileName + "?", "Delete", MessageBoxButton.YesNoCancel);
            if (result == MessageBoxResult.Yes)
            {
                File.Delete(fileLocation + PRACTICE_SESSION_FILE_EXTENSION);
            }
        }

        public static String GetFolderLocation()
        {
            return FOLDER_LOCATION;
        }


        public void SavePracticeSession(PracticeSession session, string fileLocation)
        {
            using (Stream stream = new FileStream(fileLocation + PRACTICE_SESSION_FILE_EXTENSION, FileMode.Create, FileAccess.Write))
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, session);
            }
        }

        public PracticeSession ReadPracticeSession(string fileLocation)
        {
            PracticeSession session;
            using (Stream stream = new FileStream(fileLocation + PRACTICE_SESSION_FILE_EXTENSION, FileMode.Open, FileAccess.Read))
            {
                IFormatter formatter = new BinaryFormatter();
                session = (PracticeSession)formatter.Deserialize(stream);
            }
            return session;
        }
    }
}
