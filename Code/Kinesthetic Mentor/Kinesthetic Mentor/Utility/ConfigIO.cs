﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Utility
{
    public class IsTriangleManConfigIO
    {
        private static bool isTriangleMan;
        public static bool ReadIsTriangleMan()
        {
            bool.TryParse(System.IO.File.ReadAllText("IsTriangle.txt"), out isTriangleMan);
            return (bool)isTriangleMan;
        }

        public static void WriteIsTriangleMan(bool triangle)
        {
            System.IO.File.WriteAllText("IsTriangle.txt",triangle.ToString());
        }
    }
}
