﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using System.Windows.Threading;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;

namespace Kinesthetic_Mentor
{
    public class MovementPlayerManager
    {
        private IList<ISkeletonReceiver> skeletonReceivers = new List<ISkeletonReceiver>();
        private IList<IMovementFinishedReceiver> movementFinishedReceivers = new List<IMovementFinishedReceiver>();
        private DispatcherTimer timer = new DispatcherTimer();
        private const int SECONDS_IN_100NANOSECOND = 10000000;
        private const int FRAMES_PER_SECOND = 30;
        private const int TIME_PER_TICK = SECONDS_IN_100NANOSECOND / FRAMES_PER_SECOND;
        private Movement currentMovement;
        private int frameIndex = 0;
        private delegate void UpdateSkeletonDelegate();
        private UpdateSkeletonDelegate skeletonDelegate;
        private MovementSlider slider;

        public MovementPlayerManager(Movement movement)
        {
            currentMovement = movement;
            timer.Interval = new TimeSpan(TIME_PER_TICK);
            timer.Tick += timer_Tick;
            skeletonDelegate = new UpdateSkeletonDelegate(updateSkeletonReceivers);
        }

        public void SetSlider(MovementSlider slider)
        {
            this.slider = slider;
            skeletonDelegate = new UpdateSkeletonDelegate(GetSkeletonFromSlider);
        }

        public Movement GetMovement()
        {
            return currentMovement;
        }

        private void GetSkeletonFromSlider()
        {
            slider.SeekNextFrame();
            if (!slider.IsAtEnd())
            {
                slider.GetConfig().UpdateSkeletonReceiver();
            }
            else
            {
                throw new Exception();
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                skeletonDelegate.Invoke();
                frameIndex++;
            }
            catch(Exception)
            {
                PauseMovement();
                updateMovementFinishedReceivers();
            }
        }

        private void updateSkeletonReceivers()
        {
            try
            {
                MySkeleton skeleton = currentMovement.GetFrameAtIndex(frameIndex).GetSkeleton();
                foreach (ISkeletonReceiver receiver in skeletonReceivers)
                {
                    receiver.UpdateSkeleton(skeleton);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                PauseMovement();
                updateMovementFinishedReceivers();
            }
        }

        public void registerSkeletonReceiver(ISkeletonReceiver receiver)
        {
            skeletonReceivers.Add(receiver);
            updateSkeletonReceivers();
        }

        public void unregisterAllSkeletonReceivers()
        {
            skeletonReceivers.Clear();
        }

        public void unregisterSkeletonReceiver(ISkeletonReceiver receiver)
        {
            skeletonReceivers.Remove(receiver);
        }

        private void updateMovementFinishedReceivers()
        {
            foreach (IMovementFinishedReceiver receiver in movementFinishedReceivers)
            {
                receiver.updateMovementFinishedReceiver();
            }
        }

        public void registerMovementFinishedReceiver(IMovementFinishedReceiver receiver)
        {
            movementFinishedReceivers.Add(receiver);
        }

        public void unregisterAllMovementFinishedReceivers()
        {
            movementFinishedReceivers.Clear();
        }

        public void unregisterMovementFinishedReceiver(IMovementFinishedReceiver receiver)
        {
            movementFinishedReceivers.Remove(receiver);
        }

        public void PlayMovement()
        {
            timer.Start();
        }

        public void PauseMovement()
        {
            timer.Stop();
        }

        public void Replay()
        {
            frameIndex = 0;
            timer.Start();
        }

        public void Restart()
        {
            timer.Stop();
            frameIndex = 0;
        }

        public void SeekFrame(int index)
        {
            frameIndex = index;
            updateSkeletonReceivers();
        }
    }
}
