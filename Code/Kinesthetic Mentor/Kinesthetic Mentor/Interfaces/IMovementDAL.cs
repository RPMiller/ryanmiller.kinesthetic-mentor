﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.Models;
using Kinesthetic_Mentor.Models.PracticeSessions;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface IMovementDAL
    {
        void SaveMovement(Movement movement, String fileLocation);
        Movement ReadMovement(String fileLocation);
        void SavePracticeSession(PracticeSession session, String fileLocation);
        PracticeSession ReadPracticeSession(String fileLocation);
    }
}
