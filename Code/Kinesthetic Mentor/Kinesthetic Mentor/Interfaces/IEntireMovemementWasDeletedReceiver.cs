﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.userControls.SkeletonRecorderControls;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface IEntireMovemementWasDeletedReceiver
    {
        void UpdateEntireMovemementWasDeletedReceiver(Editor editorWithNoPhases);
    }
}
