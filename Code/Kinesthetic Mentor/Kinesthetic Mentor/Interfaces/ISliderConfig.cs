﻿using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface ISliderConfig
    {
        void SetUpSlider(Kinesthetic_Mentor.userControls.SkeletonRecorderControls.MovementSlider slider);
        void Mouse_LeftDown(object sender, MouseButtonEventArgs e);
        void UpdateSkeletonReceiver();
        bool GetIsSliding();
    }
}
