﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface ISkeletonReceiver
    {
        void UpdateSkeleton(Kinesthetic_Mentor.Models.MySkeleton skeleton);
    }
}
