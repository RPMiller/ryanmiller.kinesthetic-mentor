﻿using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface ISliderReceiver
    {
        void UpdateFrameSeeked(int index);
        void UpdateMovementEdited(Movement movement);
    }
}
