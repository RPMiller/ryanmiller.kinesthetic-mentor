﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinesthetic_Mentor.Models;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface ILearnerMovementFinishedReceiver : IMovementFinishedReceiver
    {
        void UpdateLearnerMovementFinishedReceiver(Movement learner, Movement mentor);
    }
}
