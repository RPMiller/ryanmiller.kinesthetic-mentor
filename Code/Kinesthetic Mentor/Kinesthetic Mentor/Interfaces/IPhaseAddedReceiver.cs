﻿using Kinesthetic_Mentor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface IPhaseAddedReceiver
    {
        void UpdatePhaseAddedReceiver(MovementPhase phase);
    }
}
