﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface IPlayerActionReceiver
    {
        void PlayReceived();
        void PauseReceived();
        void StopReceived();
        void ReplayReceived();
    }
}
