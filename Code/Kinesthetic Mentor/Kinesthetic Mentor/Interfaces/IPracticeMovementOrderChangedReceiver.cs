﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface IPracticeMovementOrderChangedReceiver
    {
        void UpdatePracticeMovementOrderChangedReceiver(int oldPosition, int newPosition);
    }
}
