﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Kinesthetic_Mentor.Interfaces
{
    public interface IElementFocusChangedReceiver
    {
        void UpdateElementFocusChangedReceiver(UIElement elementInFocus);
    }
}
