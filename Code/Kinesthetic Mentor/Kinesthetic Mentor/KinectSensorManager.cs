﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect;
using Kinesthetic_Mentor.Interfaces;
using Kinesthetic_Mentor.Models;
using System.Windows.Threading;

namespace Kinesthetic_Mentor
{
    class KinectSensorManager
    {
        private KinectSensor sensor;
        private static IList<ISkeletonReceiver> skeletonReceivers = new List<ISkeletonReceiver>();

        void KinectSensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    Skeleton[] skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                    Skeleton skeleton = skeletons.FirstOrDefault(IsSkeletonFound);
                    if (skeleton != null)
                    {
                        MySkeleton mySkeleton = new MySkeleton (skeleton.TrackingState, skeleton.Joints);
                        //Kinesthetic_Mentor.Utility.KinectSkeletonCalibrator.AlterSkeletonData(mySkeleton);
                        updateSkeletonReceivers(mySkeleton);
                    }
                }
            }
        }

        private bool IsSkeletonFound(Skeleton x)
        {
            return x.Joints[JointType.Head].TrackingState == JointTrackingState.Tracked && x.Joints[JointType.ShoulderCenter].TrackingState != JointTrackingState.NotTracked && x.Joints[JointType.HipCenter].TrackingState != JointTrackingState.NotTracked;
        }

        public void KinectSensorChooser_KinectChanged(object sender, Microsoft.Kinect.Toolkit.KinectChangedEventArgs e)
        {
            if (e.NewSensor != null)
            {
                TransformSmoothParameters smoothingParameters = new TransformSmoothParameters();
                {
                    smoothingParameters.Smoothing = 0.5f;
                    smoothingParameters.Correction = 0.5f;
                    smoothingParameters.Prediction = 0.5f;
                    smoothingParameters.JitterRadius = 0.05f;
                    smoothingParameters.MaxDeviationRadius = 0.04f;
                };

                stopKinect(e.OldSensor);
                sensor = e.NewSensor;
                sensor.DepthStream.Enable();
                sensor.SkeletonStream.Enable(smoothingParameters);
                sensor.ColorStream.Enable();
                sensor.Start();
                sensor.SkeletonFrameReady += KinectSensor_SkeletonFrameReady;
            }
        }

        public void stopKinect()
        {
            stopKinect(sensor);
        }

        private void stopKinect(KinectSensor stopSensor)
        {
            if (stopSensor != null)
            {
                stopSensor.Stop();
                if(stopSensor.AudioSource != null)
                {
                    stopSensor.AudioSource.Stop();
                }
            }
        }

        public static void registerSkeletonReceiver(ISkeletonReceiver receiver)
        {
            if (!skeletonReceivers.Contains(receiver))
            {
                skeletonReceivers.Add(receiver);
            }
        }

        public static void unregisterAllSkeletonReceivers()
        {
            skeletonReceivers.Clear();
        }

        public static void unregisterSkeletonReceiver(ISkeletonReceiver receiver)
        {
            skeletonReceivers.Remove(receiver);
        }

        private void updateSkeletonReceivers(MySkeleton skeleton)
        {
            try
            {
                foreach (ISkeletonReceiver receiver in skeletonReceivers)
                {
                    receiver.UpdateSkeleton(skeleton);
                }
            }
            catch (InvalidOperationException e)
            {
                String s = e.Message;
            }
        }
    }
}
